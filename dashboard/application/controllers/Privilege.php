<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilege extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if($this->access_control->check('Privilege Management') == FALSE) {
			show_404();
		}

		$this->load->model('privilege_model');
	}

	public function group()
	{
		if($this->input->is_ajax_request()){
			if($this->input->post()){
				if($this->privilege_model->edit()){
					$output = array(
						'status'	=> 1,
						'response'	=> "Edit Privilege Success"
					);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> "Edit Privilege Failed"
					);
				}

				$this->output->set_content_type('application/json')->set_output(json_encode($output));
			}
		} else {
			$data['group_list'] = $this->privilege_model->get_group_list();
			$data['permission_list'] = $this->privilege_model->get_permission_list();


			$page['title']        = 'Group Privilege';
			$page['subtitle']     = 'Manage access list';
			$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
			$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
			$page['body_content'] = $this->load->view('pages/privilege', $data, TRUE);
			$page['page_js']		= array(
				base_url('assets/custom/js/manage_privileges.js'),
			);
			$page['use_datatable'] = TRUE;

			$this->load->view('common/skeleton', $page);
		}
		
	}
	public function add($type){
		if($this->input->is_ajax_request()){
			if($this->input->post())
			{
				if($type == 'group') {
					if($this->privilege_model->add('group')){
						$output = array(
							'status'	=> 1,
							'response'	=> 'Success add new group'
						);
					} else {
						$output = array(
							'status'	=> 0,
							'response'	=> 'Error add new group'
						);
					}
				} else if($type == 'permission') {
					if($this->privilege_model->add('permission')){
						$output = array(
							'status'	=> 1,
							'response'	=> 'Success add new permission'
						);
					} else {
						$output = array(
							'status'	=> 0,
							'response'	=> 'Error add new permission'
						);
					}
				}

				$this->output->set_content_type('application/json')->set_output(json_encode($output));
			}
		}
		
	}

	public function get_privilege_list()
	{
		$privilege_list = $this->privilege_model->get_privilege_list();

		$this->output->set_content_type('application/json')->set_output(json_encode($privilege_list));
	}

	public function get_group_privilege()
	{
		$result = $this->privilege_model->get_group_permission();

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

}

/* End of file Privilege.php */
/* Location: ./application/controllers/Privilege.php */