<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('category_model');
	}
	public function add_new()
	{
		if($this->input->post()){
			$result = $this->category_model->add();
			if($result['status']){
				$output = array(
					'status'	=> 1,
					'response'	=> $result
				);
			} else {
				$output = array(
					'status'	=> 0,
					'message'	=> "ERROR"
				);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */