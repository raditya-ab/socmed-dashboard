<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if($this->access_control->check('User Approval') == FALSE) {
			show_404();
		}

		$this->load->model('user_model');
		$this->load->library('access_control');
	}

	public function index()
	{
		$this->load->model('privilege_model');

		$data['group_list']		= $this->privilege_model->get_group_list();
		
		$page['page_js']      = array(
			base_url('assets/custom/js/manage_users.js')
		);
		$page['title']        = 'User Management ';
		$page['subtitle']     = 'Manage users';
		$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
		$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
		$page['body_content'] = $this->load->view('pages/user', $data, TRUE);
		$page['use_datatable'] = TRUE;

		$this->load->view('common/skeleton', $page);
	}

	public function get_user_list()
	{
		$user_list = $this->user_model->get_user_list();

		$this->output->set_content_type('application/json')->set_output(json_encode($user_list));
	}

	public function approve() {
		$userid = $this->input->post('userid');

		if($this->user_model->approve_user($userid)) {
			$output = array(
				'status'	=> 1,
				'response'	=> 'User approved'
			);
		} else {
			$output = array(
				'status'	=> 0,
				'response'	=> 'User approval Failed'
			);
		}
		echo json_encode($output);
		// $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function block() {
		$userid = $this->input->post('userid');

		if($this->user_model->block_user($userid)) {
			$output = array(
				'status'	=> 1,
				'response'	=> 'User blocked'
			);
		} else {
			$output = array(
				'status'	=> 0,
				'response'	=> 'User blocking Failed'
			);
		}
		echo json_encode($output);
		// $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function get_user_data()
	{
		if($this->input->is_ajax_request()){
			if($this->input->post()){
				$data = $this->user_model->get_user_detail();
				if( ! empty($data)) {
					$output = array(
						'status'	=> 1,
						'response'	=> $data
					);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> array()
					);
				}
			}
		} else {
			$output = array(
				'status'	=> 0,
				'response'	=> "Error"
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function edit($params){
		if($this->user_model->edit_user($params)){
			$output = array(
				'status'	=> 1,
				'response'	=> "Success Edit User"
			);
		} else {
			$output = array(
				'status'	=> 0,
				'response'	=> "Failed Edit User"
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */