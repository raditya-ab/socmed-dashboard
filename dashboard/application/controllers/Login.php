<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	    $this->load->library('hybridauth');
	    $this->load->model('login_model');
	}

	public function index()
	{
		if( !empty($this->session->userdata('HA::STORE')) || !empty($this->session->userdata('loggedIn'))){
	    	redirect('pages/dashboard', 'refresh');
	    }

		$data = array();
		foreach ($this->hybridauth->HA->getProviders() as $provider_id => $params)
		{
		  $data['provider'][] = array(
		  	'providers' => strtolower($provider_id),
		  	'url'	=> site_url("login/verify_login/{$provider_id}")
		  );
		}

		$this->load->view('pages/login',$data);
	}

	public function verify_login($provider = NULL){

		$params = array(
		  'hauth_return_to' => site_url("login/verify_login/{$provider}"),
		);

		if( ! is_null($provider) ) {
			if (isset($_REQUEST['openid_identifier']))
			{
			  $params['openid_identifier'] = $_REQUEST['openid_identifier'];
			}
			try
			{
				$adapter = $this->hybridauth->HA->authenticate($provider, $params);
				
				$profile = $adapter->getUserProfile();
			  
				$profile->provider_name = $provider; //append provider name into returned data

				if($this->login_model->user_exist($profile) == FALSE){
					$this->login_model->register_user($profile);
				}
				$data = $this->login_model->get_user_data($profile);

				$session_data = array(
					'loggedIn'		=> array(
						'loggedIP'        => $this->input->ip_address(),
						'login_timestamp' => date('Y-m-d H:i:s',time()),
						'id'              => $data->id,
						'group_id'        => $data->group_id,
						'email'           => $data->email,
						'first_name'      => $data->first_name,
						'last_name'       => $data->last_name,
						'screen_name'     => $data->screen_name,
						'profile_img'     => @$data->profile_image_url,
						'privilege'       => $data->permission_name
					),
					'status'		=> $data->status
				);
				
				if($data->status == 1){
					$this->session->set_userdata( $session_data );
					
					$this->login_model->update_last_login($profile->email); // update login ip and timestamp
					
					redirect('Pages/dashboard','refresh');
				}

				redirect('Pages/confirm','refresh');
			}
			catch (Exception $e)
			{
			  show_error($e->getMessage());
			}
		} else {
			$profile = new StdClass();
			$profile->email = $this->input->post('email', TRUE);
			
			if($this->login_model->user_exist($profile)){
				if($this->login_model->verify()){
					$data = $this->login_model->get_user_data($profile);
					
					$session_data = array(
						'loggedIn'		=> array(
							'loggedIP'        => $this->input->ip_address(),
							'login_timestamp' => date('Y-m-d H:i:s',time()),
							'id'				=> $data->id,
							'group_id'		=> $data->group_id,
							'email'           => $data->email,
							'first_name'      => $data->first_name,
							'last_name'       => $data->last_name,
							'screen_name'		=> $data->screen_name,
							'profile_img'		=> $data->profile_image_url,
							'privilege'       => $data->permission_name
						),
						'status'		=> $data->status
					);

					$output = array(
						'status'	=> 1,
						'response'	=> 'Login Success'
					);

					if($data->status == 1){
						$output['redirectURL'] = site_url('pages/dashboard');
					} else {
						$output['redirectURL'] = site_url('pages/confirm');
					}

					$this->session->set_userdata( $session_data );
					$this->login_model->update_last_login($profile->email);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> 'Login Failed'
					);
				}
			} else {
		 		$output = array(
		 			'status'	=> 0,
		 			'response'	=> 'Email is not registed yet'
		 		);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		}
	}

	public function logout()
	{
		session_unset();
		session_destroy();
		redirect('Login','refresh');
	}

	public function test()
	{
		$data = $this->searchTweets('#asiangames');
		echo "<pre>";
		print_r ($data);
		echo "</pre>";
	}

	public function test_lagi()
	{
		$data = $this->lookupUser('raditya_ab');

		echo "<pre>";
		print_r ($data);
		echo "</pre>";
	}

	/**
	 * Handle the OpenID and OAuth endpoint
	 */
	public function endpoint()
	{
	  $this->hybridauth->process();
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */