<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->library('form_validation');
		$this->load->model('login_model');

		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_is_unique');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('conf_password', 'Confirmation Password', 'trim|required|matches[password]');
			$this->form_validation->set_rules('gender', 'Gender', 'trim|required|alpha');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status'	=> 0,
					'response'	=> validation_errors()
				);
				$this->output->set_content_type('application/json')->set_output(json_encode($output));
			} else {
				if($this->login_model->register_user()){
					$output = array(
						'status'	=> 1,
						'response'	=> 'Register Success'
					);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> 'Register Failed'
					);
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->load->view('pages/register');
		}
	}

	public function email_is_unique($str) {
		$this->db->select('email');
		$this->db->from('users');
		$this->db->where('email', $str);
		$result = $this->db->get()->num_rows();
		if($result > 0) {
			$this->form_validation->set_message('email_is_unique', 'Email already exist');
			return FALSE;
		}

		return TRUE;
	}

}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */