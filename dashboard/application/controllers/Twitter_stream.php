<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter_stream extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if($this->access_control->check('View') == FALSE){
			show_404();
		}

		$this->load->model('twitter_model');
		$this->load->library('form_validation');
		$this->load->library('pagination');
	}

	public function index()
	{
		if($this->access_control->check('Create') == FALSE && $this->access_control->check('Edit') == FALSE) {
			show_404();
		}

		$page['title']        = 'Twitter';
		$page['subtitle']     = 'Search about trending topics and stuff';
		$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
		$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
		$page['body_content'] = $this->load->view('pages/twitter/search_tweet', NULL, TRUE);
		
		$page['page_js']      = array(
			base_url('assets/custom/js/twitter/twitter_module.js'),
			base_url('assets/plugins/linkifyjs/linkify.js'),
			base_url('assets/plugins/linkifyjs/linkify-jquery.js'),
			base_url('assets/plugins/bootstrap-notify/bootstrap-notify.min.js')
		);
		$page['use_datatable'] = FALSE;

		$this->load->view('common/skeleton', $page);
	}

	public function grab_data() {
		if($this->access_control->check('Create') == FALSE && $this->access_control->check('Edit') == FALSE) {
			show_404();
		}
		
		$parameter = array();

		$this->form_validation->set_rules('search_string', 'Search Field', 'trim|required|min_length[1]');
		$this->form_validation->set_rules('count', 'Number of result', 'trim|numeric|greater_than[0]');
		$this->form_validation->set_rules('search_type', 'Search', 'trim|required|in_list[hashtag,screen_name]|alpha_dash');
		$this->form_validation->set_rules('param', 'Search Param', 'trim|required|in_list[hashtag,screen_name]|alpha_dash|matches[search_type]');
		$this->form_validation->set_rules('max_id', 'Max ID', 'trim|numeric');
		$this->form_validation->set_rules('until', 'Date Limit', 'trim|regex_match[/(\d){4}-(\d){2}-(\d){2}/]');

		if ($this->form_validation->run() == FALSE) {
			$output = array(
				'status'	=> 0,
				'response'  => array(
					'message'	=> validation_errors()
				)
			);
		} else {
			$save = ($this->input->post('save_result'));
			$param = $this->input->post('param');
			$search_string = trim($this->input->post('search_string'));

			if($this->input->post('count')){
				$parameter['count'] = $this->input->post('count');
			}

			if($param == 'hashtag'){

				if($this->input->post('result_type')){
					$parameter['result_type'] = $this->input->post('result_type');
				}
				if($this->input->post('until')){
					$parameter['until'] = $this->input->post('until');
				}

				if($this->input->post('max_id') && ! is_null($this->input->post('max_id'))){
					$parameter['max_id'] = $this->input->post('max_id');
				}

				$data = $this->twitter_model->searchTweets($search_string, $parameter);

				$output = array(
					'status'	=> $data['status'],
					'response'  => $data['response']
				);

				if($save){
					if($this->twitter_model->saveTweets($data)){
						$output['status'] = 2;
					}
				}

			} else if($param == 'screen_name') {
				$data = $this->twitter_model->searchUser($search_string, $parameter);
				
				$output = array(
					'status'	=> $data['status'],
					'response'  => $data['response']
				);

				if($save){
					if($this->twitter_model->saveUser($data)){
						$output['status'] = 2;
					}
				}
				
			}
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	} 

	public function hashtag()
	{
		$data['hashtag_list'] = $this->grab_hashtag_list();

		$page['title']        = '#Hashtags';
		$page['subtitle']     = 'List of hashtags stored in database';
		$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
		$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
		$page['body_content'] = $this->load->view('pages/twitter/hashtag_list', $data, TRUE);
		
		$page['page_js']      = array(
			base_url('assets/custom/js/twitter/twitter_hashtag_list.js'),
			base_url('assets/plugins/bootstrap-notify/bootstrap-notify.min.js')
		);
		$page['use_datatable'] = FALSE;

		$this->load->view('common/skeleton', $page);
	}

	public function grab_hashtag_list()
	{
		if($this->input->is_ajax_request()){
			$this->output->set_content_type('application/json')->set_output(json_encode($this->twitter_model->get_hashtag_list()));
		} else {
			return $this->twitter_model->get_hashtag_list();
		}
	}

	public function hashtag_detail($hashtag, $type = 'tweet')
	{
		$pagination_config['base_url'] = site_url("twitter_stream/hashtag_detail/$hashtag/$type");
		$pagination_config['uri_segment'] = 5;
		$pagination_config['full_tag_open'] = "<ul class='pagination'>";
		$pagination_config['full_tag_close'] ="</ul>";
		$pagination_config['num_tag_open'] = '<li>';
		$pagination_config['num_tag_close'] = '</li>';
		$pagination_config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$pagination_config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$pagination_config['next_tag_open'] = "<li>";
		$pagination_config['next_tagl_close'] = "</li>";
		$pagination_config['prev_tag_open'] = "<li>";
		$pagination_config['prev_tagl_close'] = "</li>";
		$pagination_config['first_tag_open'] = "<li>";
		$pagination_config['first_tagl_close'] = "</li>";
		$pagination_config['last_tag_open'] = "<li>";
		$pagination_config['last_tagl_close'] = "</li>";

		if($type == 'tweet'){
			$pagination_config['per_page'] = 12;
		} else if($type == 'media'){
			$pagination_config['per_page'] = 6;
		} else if($type == 'contributor'){
			$pagination_config['per_page'] = 12;
		}
		
		$page_num = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		
		if($this->input->is_ajax_request()){
			if($type == 'tweet') {
				
			} else if($type == 'media') {
				$output = array('media_list' => $this->twitter_model->get_tweet_media_list($hashtag)['user_tweets']);
			} else if($type == 'contributor') {
				$output = array('contributor_list' => $this->twitter_model->get_users_by_hashtag($hashtag));
			}

			// $this->output->set_content_type('application/json')->set_output(json_encode($output));

		} else {
			$data = array();

			if($type == 'tweet') {
				$data['tweet_list'] = $this->twitter_model->get_tweets_by_hashtag($hashtag, $pagination_config['per_page'], $page_num );
			} else if($type == 'media') {
				$data['media_list'] = $this->twitter_model->get_tweet_media_list($hashtag, 'hashtag', $pagination_config['per_page'], $page_num)['user_tweets'];

			} else if($type == 'contributor') {
				$data['contributor_list'] = $this->twitter_model->get_users_by_hashtag($hashtag, $pagination_config['per_page'], $page_num);
			}

			$data['oldest_tweet']	   = $this->twitter_model->get_oldest_hashtag_tweet($hashtag)['created_at'];
			$data['tweet_count']       = $this->twitter_model->count_hashtag($hashtag);
			$data['media_count']       = $this->twitter_model->count_all_tweet_media($hashtag);
			$data['contributor_count'] = $this->twitter_model->count_users_by_hashtag($hashtag);

			$data['hashtag'] = $hashtag;

			if($type == 'tweet') {
				$pagination_config['total_rows'] = $data['tweet_count'];
			} else if($type == 'media') {
				$pagination_config['total_rows'] = $data['media_count'];
			} else if($type == 'contributor') {
				$pagination_config['total_rows'] = $data['contributor_count'];
			}

			$this->pagination->initialize($pagination_config);
			$data['links'] = $this->pagination->create_links();

			$page['title']        = '#'.$hashtag;
			$page['subtitle']     = 'All tweets mentioned this hashtag';
			$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
			$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);

			if($type == 'tweet') {
				$page['body_content'] = $this->load->view('pages/twitter/hashtag_detail_tweet', $data, TRUE);
			} else if($type == 'media') {
				$page['body_content'] = $this->load->view('pages/twitter/hashtag_detail_media', $data, TRUE);
			} else if($type == 'contributor') {
				$page['body_content'] = $this->load->view('pages/twitter/hashtag_detail_contributor', $data, TRUE);
			}

			$page['page_css']		= array(
				base_url('assets/plugins/froala_editor/css/third_party/embedly.min.css'),
				base_url('assets/plugins/froala_editor/css/froala_editor.pkgd.min.css')
			);

			$page['page_js']      = array(
				base_url('assets/custom/js/twitter/twitter_hashtag_detail.js'),
				base_url('assets/plugins/bootstrap-notify/bootstrap-notify.min.js'),
				base_url('assets/plugins/linkifyjs/linkify.js'),
				base_url('assets/plugins/linkifyjs/linkify-jquery.js'),
				base_url('assets/plugins/momentjs/moment.js'),
				base_url('assets/plugins/froala_editor/js/froala_editor.pkgd.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/platform.js'),
				base_url('assets/plugins/froala_editor/js/third_party/embedly.min.js'),
			);

			if($type == 'tweet'){
				array_push($page['page_js'], base_url('assets/custom/js/twitter/twitter_hashtag_detail_tweet.js'));
			} else if($type == 'media') {
				array_push($page['page_js'], base_url('assets/custom/js/twitter/twitter_hashtag_detail_media.js'));
			} else {
				array_push($page['page_js'], base_url('assets/custom/js/twitter/twitter_hashtag_detail_contributor.js'));
			}
			$page['use_datatable'] = FALSE;
			
			$this->load->view('common/skeleton', $page);
		}
	}

	public function download_csv($hashtag, $type){

		$select_param = array();
		if($this->input->get('tweet_uid')){
			array_push($select_param, 't.tweet_uid');
		}
		if($this->input->get('name')){
			array_push($select_param, 'u.name');
		}
		if($this->input->get('screen_name')){
			array_push($select_param, 'u.screen_name');
		}
		if($this->input->get('text')){
			array_push($select_param, 't.text');
		}
		if($this->input->get('created_at')){
			array_push($select_param, 't.created_at');
		}

		if($this->input->get('grab_all_tweet')){
			$filename = $hashtag."_all.csv";
			$tweet_list = $this->twitter_model->grab_tweet_for_download($hashtag, NULL,$select_param);
		} else {
			$daterange = $this->input->get('tweet_daterange');

			$filename = $hashtag."_".str_replace(" ","",str_replace(":","",str_replace(" - ", "", $daterange))).".csv";
			$tweet_list = $this->twitter_model->grab_tweet_for_download($hashtag, $daterange,$select_param);
		}

		$this->array_to_csv_download($tweet_list['result']['tweet_list'], $filename);
	}

	public function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
	    // open raw memory as file so no temp files needed, you might run out of memory though
	        $f = fopen('php://memory', 'w'); 
	        // loop over the input array
	        foreach ($array as $line) { 
	            // generate csv lines from the inner arrays
	            fputcsv($f, $line, $delimiter); 
	        }
	        // reset the file pointer to the start of the file
	        fseek($f, 0);
	        // tell the browser it's going to be a csv file
	        header('Content-Type: application/csv');
	        // tell the browser we want to save it instead of displaying it
	        header('Content-Disposition: attachment; filename="'.$filename.'";');
	        // make php send the generated csv lines to the browser
	        fpassthru($f);
	        fclose($f);
	}  

	public function load_hashtag_detail_view($retrieved_data)
	{
		$data['user_detail'] = $retrieved_data['response']['user'];
		$data['user_timeline'] = $retrieved_data['response']['pure_result'];
		$data['tweet_datestamp'] = $retrieved_data['response']['tweet_date'];

		$view = $this->load->view('page_element/twitter/user_tweet_timeline', $data, TRUE);

		$this->output->set_content_type('application/json')->set_output(json_encode($view));
	}

	public function load_hashtag_media_view($retrieved_data)
	{
		$data['user_detail'] = $retrieved_data['response']['user'];
		$data['user_timeline'] = $retrieved_data['response']['pure_result'];
		$data['tweet_datestamp'] = $retrieved_data['response']['tweet_date'];

		$view = $this->load->view('page_element/twitter/user_tweet_timeline', $data, TRUE);

		$this->output->set_content_type('application/json')->set_output(json_encode($view));
	}

	public function user_detail($screen_name)
	{
		if(!$this->input->is_ajax_request())
		{
			$retrieved_data = $this->twitter_model->retrieve_user_timeline($screen_name);
			$data['user_detail'] = $retrieved_data['response']['user'];
			$data['user_timeline'] = $retrieved_data['response']['pure_result'];
			$data['tweet_datestamp'] = $retrieved_data['response']['tweet_date'];
			
			$page['title']        = 'Hi! @'.$screen_name;
			$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
			$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
			$page['body_content'] = $this->load->view('pages/twitter/profile', $data, TRUE);
			$page['page_css']		= array(
				base_url('assets/plugins/froala_editor/css/third_party/embedly.min.css'),
				base_url('assets/plugins/froala_editor/css/froala_editor.pkgd.min.css'),
			);
			$page['page_js']      = array(
				base_url('assets/custom/js/twitter/twitter_user_profile.js'),
				base_url('assets/plugins/linkifyjs/linkify.js'),
				base_url('assets/plugins/linkifyjs/linkify-jquery.js'),
				base_url('assets/plugins/bootstrap-notify/bootstrap-notify.min.js'),
				base_url('assets/plugins/froala_editor/js/froala_editor.pkgd.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/platform.js'),
				base_url('assets/plugins/froala_editor/js/third_party/embedly.min.js'),
				base_url('assets/plugins/momentjs/moment.js')
			);
			$page['use_datatable'] = FALSE;

			// echo "<pre>";
			// print_r ($data);
			// echo "</pre>";
			$this->load->view('common/skeleton', $page);
		}
		else
		{
			$data['retrieved_data'] = $this->twitter_model->retrieve_user_timeline($screen_name);
			
			$view = array(
				'status'	=> $data['retrieved_data']['status'],
				'user_detail'	=> $data['retrieved_data']['response']['user'],
				'user_timeline'	=> $data['retrieved_data']['response']['pure_result'],
				'response'	=> $this->load->view('page_element/twitter/user_tweet_timeline', $data, TRUE)
			);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($view));
		}
	}

	
	public function testing_curl()
	{
		$data = $this->twitter_model->testing_curl();

		echo "<pre>";
		print_r ($data);
		echo "</pre>";
	}
	public function load_user_timeline_view($retrieved_data)
	{
		$data['user_detail'] = $retrieved_data['response']['user'];
		$data['user_timeline'] = $retrieved_data['response']['pure_result'];
		$data['tweet_datestamp'] = $retrieved_data['response']['tweet_date'];

		$view = $this->load->view('page_element/twitter/user_tweet_timeline', $data, TRUE);

		$this->output->set_content_type('application/json')->set_output(json_encode($view));
	}

	public function retrieve_user_timeline($screen_name)
	{
		$result = $this->twitter_model->retrieve_user_timeline('354389070', NULL);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save_search_param()
	{
		$search_string = trim(strtolower($this->input->post('search_string', TRUE)));

		if(substr($search_string, 0, 1) == '#'){
			$search_string = ltrim($search_string, '#');
		}

		$output = $this->twitter_model->save_search_param($search_string);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function get_search_param()
	{
		$output = $this->twitter_model->get_search_param();

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function search($id)
	{
		$data = $this->twitter_model->lookupTweet($id);
		echo "<pre>";
		print_r ($data);
		echo "</pre>";
	}

	public function lookup()
	{
		include_once APPPATH.'vendor/autoload.php';

		$conn = new Abraham\TwitterOAuth\TwitterOAuth('17PZ7QyiM3ZCfsu3nX4lMb6hn','A55tmO8U1K0sadhXy9breVh8GLBXTKeRNKwCIJqw2furOaDh4R','100623131-4xfkLIPsPFRtsis1otCeWZzXG58cM14wJZM0bzKV','p6AAnZR2AjpCxiRSzDpB41uW5eD8eebqYCWY7cdLNmeBu');
		$content = $conn->get("account/verify_credentials");

		echo "<pre>";
		print_r ($content);
		echo "</pre>";
	}

	public function media_hashtag()
	{
		$data['data'] = $this->twitter_model->get_tweet_media_list('thorragnarok');
	}
	
	public function super_unique($array,$key)
	{
	   $temp_array = array();

	   foreach ($array as &$v) {

	       if (!isset($temp_array[$v[$key]]))

	       $temp_array[$v[$key]] =& $v;

	   }

	   $array = array_values($temp_array);

	   return $array;
	}

}

/* End of file Twitter_stream.php */
/* Location: ./application/controllers/Twitter_stream.php */