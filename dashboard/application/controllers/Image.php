<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Image extends MY_Controller {
		
		public function __construct()
		{
			parent::__construct();
			if($this->access_control->check('Create') == FALSE && $this->access_control->check('Edit') == FALSE) {
				show_404();
			}

			$this->load->model('image_model');
			$this->load->model('common_model');
		}
		public function index()
		{
			// $data = NULL;
			$data['image_list'] = $this->image_model->load_gallery();

			$page['page_js']      = array(
				base_url('assets/plugins/masonry/masonry.pkgd.min.js'),
				base_url('assets/custom/js/manage_media.js')
			);

			$page['title']         = 'Media';
			$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
			$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
			$page['body_content'] = $this->load->view('pages/manage_media', $data, TRUE);

			$this->load->view('common/skeleton', $page);
		}

		public function get_gallery()
		{
			$result = $this->image_model->load_gallery();
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}

		public function delete()
		{
			if($this->input->is_ajax_request()) {
				$image_id = $this->input->post('id', TRUE);

				if($this->image_model->delete($image_id)){
					$output = array(
						'status'	=> 1
					);
				} else {
					$output = array(
						'status'	=> 0
					);
				}
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		}

		//Handling photo uploads
		public function upload()
		{
			$this->load->library('upload');
			$this->load->library('image_lib');
			
			$mime_type = getMimeType($_FILES['uploaded_file']['tmp_name']);
			
			// echo "<pre>";
			// print_r ($_FILES);
			// echo "</pre>";
			// echo "<pre>";
			// print_r ($mime_type);
			// echo "</pre>";
			if($mime_type != 'application/octet-stream')
			{
				if(stripos($mime_type,'image') !== FALSE){
					$config['upload_path'] = getcwd().'/uploads/img/';
					$dir = "/uploads/img/";
					$config['max_size']  = (5 * 1024);
				} else if(stripos($mime_type,'application/pdf') !== FALSE){
					$config['upload_path'] = getcwd().'/uploads/file/';
					$dir = "/uploads/file/";
					$config['max_size']  = (20 * 1024);
				} else if(stripos($mime_type,'video') !== FALSE){
					$config['upload_path'] = getcwd().'/uploads/video/';
					$dir = "/uploads/video/";
					$config['max_size']  = (100 * 1024);
				}
			} else
			{
				$file_type = $_FILES['uploaded_file']['type'];

				if(stripos($file_type,'image') !== FALSE){
					$config['upload_path'] = getcwd().'/uploads/img/';
					$dir = "/uploads/img/";
					$config['max_size']  = (5 * 1024);
				} else if(stripos($file_type,'application/pdf') !== FALSE){
					$config['upload_path'] = getcwd().'/uploads/file/';
					$dir = "/uploads/file/";
					$config['max_size']  = (20 * 1024);
				} else if(stripos($file_type,'video') !== FALSE){
					$config['upload_path'] = getcwd().'/uploads/video/';
					$dir = "/uploads/video/";
					$config['max_size']  = (100 * 1024);
				}
			}
			
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|pdf|mp4|mpeg|flv|3gp';
			
			
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('uploaded_file')){
				$output = array('error' => $this->upload->display_errors());
			}
			else{
				$upload_data = $this->upload->data();

				$img_conf['source_image']   = $_FILES['uploaded_file']['tmp_name'];
				$img_conf['new_image']      = getcwd().'/uploads/img/thumb/thumb_'.$upload_data['file_name'];
				$img_conf['image_library']  = 'gd2';
				$img_conf['maintain_ratio'] = TRUE;
				$img_conf['height']         = 200;
				$img_conf['width']          = 200;

				$this->image_lib->initialize($img_conf);

	            if( ! $this->image_lib->resize())
	            {
	            	unlink($upload_data['full_path']);
	            }
	            else
	            {
	            	$upload_data['thumb_url'] = '/uploads/img/thumb/thumb_'.$upload_data['file_name'];
	            }
	            
				if($this->common_model->store_media($upload_data)){
					$output = array(
						'status'	=> 1,
						'link'	=> base_url($dir."$upload_data[raw_name]".$upload_data['file_ext']),
						'thumb'	=> base_url($upload_data['thumb_url'])
					);	
				} else {
					unlink($upload_data['full_path']);
					$output = array(
						'status'	=> 0
					);
				}
			}

			echo json_encode($output);
		}
	}
	
	/* End of file Image.php */
	/* Location: ./application/controllers/Image.php */	