<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if($this->access_control->check('Article Management') == FALSE) {
			show_404();
		}
		
		$this->load->model('common_model');
		$this->load->library('form_validation');
		$this->load->model('article_model');
	}
	public function index()
	{
		$data['article_date'] = $this->article_model->get_article_date_dropdown();
		$data['article_category'] = $this->article_model->get_article_category_dropdown();
		$data['article_author'] = $this->article_model->get_article_author_dropdown();

		$page['title']        = 'Articles';
		$page['subtitle']     = 'Manage articles';
		$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
		$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
		$page['body_content'] = $this->load->view('pages/articles/article_list', $data, TRUE);
		$page['page_js']      = array(
			base_url('assets/custom/js/article_list.js')
		);
		$page['use_datatable'] = TRUE;

		$this->load->view('common/skeleton', $page);
	}

	public function add()
	{
		if($this->access_control->check('Edit') == FALSE && $this->access_control->check('Create') == FALSE) {
			show_404();
		}

		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[100]');
			$this->form_validation->set_rules('subtitle', 'Subtitle', 'trim');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[150]');
			$this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('featured_article', 'Featured', 'trim|numeric');
			$this->form_validation->set_rules('status', 'Post Status', 'trim|numeric');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status'	=> 0,
					'response'	=> validation_errors()
				);
			} else {
				$status = $this->input->post('status', TRUE);

				if($status == 0){
					$response = 'in Draft';
				} else {
					$response = 'and Published';
				}
				if($this->article_model->post_article()){
					$output = array(
						'status'	=> 1,
						'response'	=> 'Article has been saved '.$response
					);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> 'Error saving article to DB'
					);
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$page['page_css']		= array(
				base_url('assets/plugins/froala_editor/css/froala_editor.pkgd.min.css'),
				base_url('assets/plugins/froala_editor/css/themes/gray.min.css'),
				base_url('assets/plugins/froala_editor/css/third_party/embedly.min.css'),
				base_url('assets/plugins/froala_editor/css/third_party/codemirror.min.css'),
			);

			$page['page_js']      = array(
				base_url('assets/plugins/froala_editor/js/froala_editor.pkgd.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/codemirror.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/xml.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/platform.js'),
				base_url('assets/plugins/froala_editor/js/third_party/embedly.min.js'),
				base_url('assets/custom/js/new_article.js')
			);
			$data['category_list']	= $this->common_model->get_category();
			$page['title']        = 'Add New Articles';

			$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
			$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
			$page['body_content'] = $this->load->view('pages/articles/new_article', $data, TRUE);

			$this->load->view('common/skeleton', $page);
		}
	}

	public function edit($article_id)
	{
		if($this->access_control->check('Edit') == FALSE) {
			show_404();
		}

		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[100]');
			$this->form_validation->set_rules('subtitle', 'Subtitle', 'trim');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[150]');
			$this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('featured_article', 'Featured', 'trim|numeric');
			$this->form_validation->set_rules('status', 'Post Status', 'trim|numeric');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status'	=> 0,
					'response'	=> validation_errors()
				);
			} else {
				$status = $this->input->post('status', TRUE);

				if($status == 0){
					$response = 'and saved in Draft';
				} else {
					$response = 'and Published';
				}
				if($this->article_model->post_article('edit')){
					$output = array(
						'status'	=> 1,
						'response'	=> 'Article has been updated '.$response
					);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> 'Error when trying to update article'
					);
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$page['page_css']		= array(
				base_url('assets/plugins/froala_editor/css/froala_editor.pkgd.min.css'),
				base_url('assets/plugins/froala_editor/css/themes/gray.min.css'),
				base_url('assets/plugins/froala_editor/css/third_party/embedly.min.css'),
				base_url('assets/plugins/froala_editor/css/third_party/codemirror.min.css'),
			);

			$page['page_js']      = array(
				base_url('assets/plugins/froala_editor/js/froala_editor.pkgd.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/codemirror.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/xml.min.js'),
				base_url('assets/plugins/froala_editor/js/third_party/platform.js'),
				base_url('assets/plugins/froala_editor/js/third_party/embedly.min.js'),
				base_url('assets/custom/js/edit_article.js')
			);
			$data['category_list'] = $this->common_model->get_category();
			$data['post_detail'] = $this->article_model->get_detail($article_id);
			
			$page['title']         = 'Edit Articles';
			$page['navbar']       = $this->load->view('common/navbar', NULL, TRUE);
			$page['sidebar']      = $this->load->view('common/sidebar', NULL, TRUE);
			$page['body_content'] = $this->load->view('pages/articles/edit_article', $data, TRUE);

			$this->load->view('common/skeleton', $page);
		}
	}

	public function delete($id){
		if($this->access_control->check('Delete') == FALSE) {
			$output = array(
				'status'	=> 0,
				'response'	=> "Failed delete article. Doesn't have permission"
			);
		}
		else
		{
			if($this->input->is_ajax_request()){
				if($this->article_model->delete($id)){
					$output = array(
						'status'	=> 1,
						'response'	=> "Success delete article"
					);
				} else {
					$output = array(
						'status'	=> 0,
						'response'	=> "Failed delete article"
					);
				}
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	//Handling photo uploads
	public function upload_article_image()
	{
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$mime_type = getMimeType($_FILES['uploaded_file']['tmp_name']);
		// echo "<pre>";
		// print_r ($_FILES);
		// echo "</pre>";
		// echo "<pre>";
		// print_r ($mime_type);
		// echo "</pre>";
		if($mime_type != 'application/octet-stream')
		{
			if(stripos($mime_type,'image') !== FALSE){
				$config['upload_path'] = getcwd().'/uploads/img/';
				$dir = "/uploads/img/";
				$config['max_size']  = (5 * 1024);
			} else if(stripos($mime_type,'application/pdf') !== FALSE){
				$config['upload_path'] = getcwd().'/uploads/file/';
				$dir = "/uploads/file/";
				$config['max_size']  = (20 * 1024);
			} else if(stripos($mime_type,'video') !== FALSE){
				$config['upload_path'] = getcwd().'/uploads/video/';
				$dir = "/uploads/video/";
				$config['max_size']  = (100 * 1024);
			}
		} else
		{
			$file_type = $_FILES['uploaded_file']['type'];

			if(stripos($file_type,'image') !== FALSE){
				$config['upload_path'] = getcwd().'/uploads/img/';
				$dir = "/uploads/img/";
				$config['max_size']  = (5 * 1024);
			} else if(stripos($file_type,'application/pdf') !== FALSE){
				$config['upload_path'] = getcwd().'/uploads/file/';
				$dir = "/uploads/file/";
				$config['max_size']  = (20 * 1024);
			} else if(stripos($file_type,'video') !== FALSE){
				$config['upload_path'] = getcwd().'/uploads/video/';
				$dir = "/uploads/video/";
				$config['max_size']  = (100 * 1024);
			}
		}
		
		$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|pdf|mp4|mpeg|flv|3gp';
		
		
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('uploaded_file')){
			$output = array('error' => $this->upload->display_errors());
		}
		else{
			$upload_data = $this->upload->data();

			$img_conf['source_image']   = $_FILES['uploaded_file']['tmp_name'];
			$img_conf['new_image']      = getcwd().'/uploads/img/thumb/thumb_'.$upload_data['file_name'];
			$img_conf['image_library']  = 'gd2';
			$img_conf['maintain_ratio'] = TRUE;
			$img_conf['height']         = 200;
			$img_conf['width']          = 200;

			$this->image_lib->initialize($img_conf);

            if( ! $this->image_lib->resize())
            {
            	unlink($upload_data['full_path']);
            }
            else
            {
            	$upload_data['thumb_url'] = '/uploads/img/thumb/thumb_'.$upload_data['file_name'];
            }
            
			if($this->common_model->store_media($upload_data)){
				$output = array(
					'link'	=> base_url($dir."$upload_data[raw_name]".$upload_data['file_ext'])
				);	
			} else {
				unlink($upload_data['full_path']);
			}
		}
		echo json_encode($output);
	}

	public function get_article_list()
	{
		$article_list = $this->article_model->get_article_list();

		$this->output->set_content_type('application/json')->set_output(json_encode($article_list));
	}

	public function bulk_edit(){
		if($this->article_model->bulk_edit()){
			$output = array(
				'status'	=> 1
			);
		} else {
			$output = array(
				'status'	=> 0
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Article.php */
/* Location: ./application/controllers/Article.php */