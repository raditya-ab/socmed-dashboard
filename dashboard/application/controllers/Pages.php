<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->model('common_model');
	}

	public function dashboard()
	{
		$this->load->model('article_model');
		$data['google_user'] = $this->common_model->get_total_user('Google');
		$data['facebook_user'] = $this->common_model->get_total_user('Facebook');
		$data['twitter_user'] = $this->common_model->get_total_user('Twitter');
		$data['email_user'] = $this->common_model->get_total_user();
		$data['article_list']	= $this->article_model->get_article_post();
		
		$page['title'] = 'Recent Updates';
		$page['subtitle'] = 'Latest status of our company';
		$page['page_js'] = array(
			base_url('assets/plugins/froala_editor/js/third_party/platform.js'),
			base_url('assets/plugins/masonry/masonry.pkgd.min.js'),
			base_url('assets/custom/js/dashboard/dashboard.js')
		);
		$page['navbar'] = $this->load->view('common/navbar', NULL, TRUE);
		$page['sidebar'] = $this->load->view('common/sidebar', NULL, TRUE);
		$page['body_content'] = $this->load->view('pages/dashboard', $data, TRUE);

		$this->load->view('common/skeleton', $page);
	}

	public function confirm()
	{
		$this->load->view('pages/lockscreen');
	}
}

/* End of file Pages.php */
/* Location: ./application/controllers/Pages.php */