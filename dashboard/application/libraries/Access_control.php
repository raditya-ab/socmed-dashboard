<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_control
{
	protected $ci;
	private $user_empty = FALSE;
	private $user_id, $group_id;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->user_id = $this->ci->session->userdata('loggedIn')['id'];
        $this->group_id = $this->ci->session->userdata('loggedIn')['group_id'];
	}

	public function check($permission) {
		// if(! $this->user_permissions($permission, $user_id)) {
		// 	echo "suni";
		// 	return FALSE;
		// }

		if(! $this->group_permissions($permission, $this->group_id)) {
			
			return FALSE;
		}
		
		return TRUE;
	}
	
	public function user_permissions($permission, $user_id) {
		$this->ci->db->where('permission_name', $permission);
		$this->ci->db->where('userid', $this->user_id);
		$this->ci->db->from('user_permissions');
		$this->db->join('permission_l', 'table.column = table.column', 'left');
		$result = $this->ci->db->get();

		if($result->num_rows() > 0) {
			$permission = $result->row();

			if($permission->permission_type == 0) {
				return FALSE;
			}

			return TRUE;
		}

		$this->setUserEmpty('true');

		return TRUE;
	}

	public function group_permissions($permission, $group_id) {
		$this->ci->db->where('pl.permission_name', $permission);
		$this->ci->db->where('gp.group_id', $this->group_id);
		$this->ci->db->from('group_permissions gp');
		$this->ci->db->join('permission_list pl', 'pl.id = gp.permission_id', 'left');
		
		$result = $this->ci->db->get();
		
		if($result->num_rows() > 0) {
			$permission = $result->row();

			if($permission->permission_type == 0) {
				return FALSE;
			}

			return TRUE;
		}

		return FALSE;
	}

	public function setUserEmpty($val) {
		$this->user_empty = $val;
	}

	public function isUserEmpty() {
		return $this->user_empty;
	}

}

/* End of file Access_control.php */
/* Location: ./application/libraries/Access_control.php */
