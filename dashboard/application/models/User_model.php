<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function get_user_list()
	{
		$this->db->select('id, email, first_name, last_name, IFNULL(provider_name, "-") as provider_name, ug.group_name, CASE u.status WHEN 0 THEN "Not Approved" WHEN 1 THEN "Approved" END as status, u.status as real_status', FALSE);
		$this->db->from('users u');
		$this->db->join('user_provider up', 'u.id = up.user_id', 'left');
		$this->db->join('usergroups ug', 'u.group_id = ug.group_id', 'left');
		$result = $this->db->get()->result();
		// echo $this->db->last_query();

		if(count($result) > 0){
			$list_data = array();
			foreach($result as $i => $user) {
				$row = array();

				$row['checkbox'] = "<input type='checkbox' class='chbUser' value='{$user->id}'> ";
				$row['no'] = ++$i;
				$row['first_name'] = $user->first_name;
				$row['last_name'] = $user->last_name;
				$row['email'] = $user->email;
				$row['provider_name'] = $user->provider_name;
				$row['group'] = ucwords($user->group_name);
				$row['status']		= $user->status;
				if($user->real_status == 0)
					$row['action'] = "<button class='btn btn-xs btn-info btn-approve' data-id='{$user->id}'>Approve</button>";
				else
					$row['action'] = "<button class='btn btn-xs btn-danger btn-blocks' data-id='{$user->id}'>Block</button>";
				$row['action'] .= "&nbsp";
				$row['action'] .= "<button class='btn btn-xs btn-warning btn-edit' data-id='{$user->id}'>Edit</button>";

				$list_data['data'][] = $row;
			}

			return $list_data;
		}

		return array();
	}
	
	public function approve_user($user_id) {
		$this->db->where('id', $user_id);
		if($this->db->update('users', array('status' => 1, 'updated_at' => date('Y-m-d H:i:s')))){
			
			return TRUE;
		}

		return FALSE;
	}

	public function block_user($user_id) {
		$this->db->where('id', $user_id);
		if($this->db->update('users', array('status' => 0, 'updated_at' => date('Y-m-d H:i:s')))){
			
			return TRUE;
		}

		return FALSE;
	}

	public function get_user_detail()
	{
		$userid = $this->input->post('id');

		$this->db->select('id, group_id, email, first_name, last_name, gender, screen_name, profile_image_url, location');
		$this->db->from('users u');
		$this->db->where('id', $userid);

		$result = $this->db->get()->row();

		if(empty($result)){
			return FALSE;
		} else {
			return $result;
		}
	}

	public function edit_user($params){
		if($params == "form") {
			$update_data = array(
				'email'	=> $this->input->post('email', TRUE),
				'first_name'	=> $this->input->post('first_name', TRUE),
				'last_name'	=> $this->input->post('last_name', TRUE),
				'screen_name'	=> $this->input->post('screen_name', TRUE),
				'group_id'	=> $this->input->post('group_id', TRUE),
				'updated_at'	=> date('Y-m-d H:i:s')
			);

			$this->db->where('id', $this->input->post('id'));

			$this->db->update('users', $update_data);

			if($this->db->affected_rows() > 0) {
				return TRUE;
			}
		} else if($params == "bulk_action") {
			$type = $this->input->post('type', TRUE);
			$users = $this->input->post('users', TRUE);

			switch($type){
				case 'approve':
					$this->db->where_in('id', $users);
					$this->db->set('status', 1);
					$this->db->set('updated_at', date('Y-m-d :H:i:s'));
					$this->db->update('users');
					break;
				case 'block':
					$this->db->where_in('id', $users);
					$this->db->set('status', 0);
					$this->db->set('updated_at', date('Y-m-d :H:i:s'));
					$this->db->update('users');
					break;
				case 'delete':
					foreach($users as $uid){
						if($this->is_using_social($uid)){
							$this->db->where('user_id', $uid);
							$this->db->delete('user_provider');
						}
					}
					$this->db->set('status', -1);
					$this->db->set('updated_at', date('Y-m-d :H:i:s'));
					$this->db->where_in('id', $users);
					$this->db->update('users');
					break;
			}

			if($this->db->affected_rows() > 0) {
				return TRUE;
			}
		}

		return FALSE;
	}

	public function is_using_social($id){
		$this->db->select('provider_uid');
		$this->db->from('user_provider');
		$this->db->where('user_id', $id);

		$result = $this->db->get()->num_rows();

		if($result > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */