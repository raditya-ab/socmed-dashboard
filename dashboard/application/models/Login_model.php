<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function user_exist($params) {
		if(@$params->identifier) {
			$this->db->where('provider_uid', $params->identifier);
			$num_row = $this->db->get('user_provider')->num_rows();
		} elseif(@$params->email) {
			$this->db->where('email', $params->email);
			$num_row = $this->db->get('users')->num_rows();
		}

		if($num_row > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function register_user($profile_data = NULL) {
		$this->db->trans_start();

		if(isset($profile_data->provider_name)) {
			$profile = array(
				'email'             => $profile_data->email,
				'first_name'        => $profile_data->firstName,
				'last_name'         => (! is_null($profile_data->lastName)) ? $profile_data->lastName : '',
				'gender'            => $profile_data->gender,
				'location'          => $profile_data->city,
				'screen_name'       => $profile_data->displayName,
				'profile_image_url' => $profile_data->photoURL,
				'password'          => password_hash($this->__random_password(), PASSWORD_BCRYPT)
			);

			if($profile_data->gender == 'male') {
				$profile['profile_image_url'] = 'assets/dist/img/male_placeholder.jpg';
			} else if($profile_data->gender == 'female') {
				$profile['profile_image_url'] = 'assets/dist/img/female_placeholder.jpg';
			} else {
				$profile['profile_image_url'] = 'assets/dist/img/neutral_placeholder.png';
			}

			$this->db->insert('users', $profile);
			$inserted_id = $this->db->insert_id();

			$provider_data = array(
				'user_id'       => $inserted_id,
				'provider_uid'  => $profile_data->identifier,
				'provider_name' => $profile_data->provider_name
			);

			$this->db->insert('user_provider', $provider_data);
		} else {
			$profile = array(
				'email'             => $this->input->post('email', TRUE),
				'password'			=> md5($this->input->post('password', TRUE).'429b5351e7dbff69e4d2e8a05d266324'),
				'first_name'        => $this->input->post('first_name', TRUE),
				'last_name'         => $this->input->post('last_name', TRUE),
				'screen_name'       => $this->input->post('first_name', TRUE).' '.$this->input->post('last_name', TRUE),
				'gender'			=> $this->input->post('gender', TRUE)
			);

			if($this->input->post('gender', TRUE) == 'male') {
				$profile['profile_image_url'] = 'assets/dist/img/male_placeholder.jpg';
			} else {
				$profile['profile_image_url'] = 'assets/dist/img/female_placeholder.jpg';
			}

			$this->db->insert('users', $profile);
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE){
			$this->update_last_login($profile['email']);

			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function verify(){
		$email         = $this->input->post('email', TRUE);
		$password      = $this->input->post('password', TRUE);
		$password_hash = md5($this->input->post('password', TRUE).'429b5351e7dbff69e4d2e8a05d266324');

		$this->db->select('email, password');
		$this->db->from('users');
		$this->db->like('email', $email);
		$this->db->like('password', $password_hash);
		$this->db->where('status != ', -1);

		$result = $this->db->get();

		$num_row = $result->num_rows();

		if($num_row == 1){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update_last_login($email){
		$data = array(
			'last_login'	=> date('Y-m-d H:i:s'),
			'last_login_ip' => $this->input->ip_address()
		);

		$this->db->where('email', $email);
		if($this->db->update('users', $data)){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_user_data($params){
		if(@$params->identifier) {
			$this->db->select('u.id, u.group_id, email, first_name, last_name, gender, screen_name, profile_image_url, u.status, GROUP_CONCAT(pl.permission_name) as permission_name');
			$this->db->from('users u');
			$this->db->join('user_provider up', 'up.user_id = u.id', 'left');
			$this->db->join('group_permissions gp', 'u.group_id = gp.group_id', 'left');
			$this->db->join('permission_list pl', 'gp.permission_id = pl.id', 'left');
			$this->db->group_by('email');
			$this->db->where('provider_uid', $params->identifier);
			$this->db->where('gp.permission_type', 1);
			$result = $this->db->get()->row();
		} elseif(@$params->email) {
			$this->db->select('u.id, u.group_id, email, first_name, last_name, gender, screen_name, profile_image_url, status, GROUP_CONCAT(pl.permission_name) as permission_name');
			$this->db->from('users u');
			$this->db->join('group_permissions gp', 'u.group_id = gp.group_id', 'left');
			$this->db->join('permission_list pl', 'gp.permission_id = pl.id', 'left');
			$this->db->where('email', $params->email);
			$this->db->where('gp.permission_type', 1);
			$result = $this->db->get()->row();
		}
		
		return $result;
	}

	private function __random_password( $length = 8 ) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
	}
}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */