<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	public function get_article_list()
	{
		$this->db->select('a.id, a.title, a.subtitle, u.first_name as author, GROUP_CONCAT(c.name) as article_category, CASE COUNT(acm.article_id) WHEN 0 THEN "-" ELSE COUNT(acm.article_id) END as comment_count, a.date_created, CASE a.status WHEN 0 THEN "Draft" WHEN 1 THEN "Published" END as status', FALSE);
		$this->db->from('articles a');
		if($this->input->post('date_filter')){
			$this->db->like('a.date_created', $this->input->post('date_filter', TRUE), 'BOTH');
		}
		if($this->input->post('category_filter')){
			$this->db->where('ac.category_id', $this->input->post('category_filter', TRUE));
		}
		if($this->input->post('author_filter')) {
			$this->db->where('a.author', $this->input->post('author_filter'));
		}
		$this->db->where('a.status != ', -1);
		$this->db->join('article_category ac', 'ac.article_id = a.id', 'left');
		$this->db->join('category c', 'ac.category_id = c.id', 'left');
		$this->db->join('users u', 'a.author = u.id', 'left');
		$this->db->join('article_comment acm', 'a.id = acm.article_id', 'left');
		$this->db->group_by('a.id');
		
		$result = $this->db->get()->result();
		// echo $this->db->last_query();
		

		if(count($result) > 0){
			$list_data = array();
			foreach($result as $i => $article) {
				$row = array();

				$row['checkbox'] = "<input type='checkbox' class='chbArticle' value='{$article->id}'> ";
				$row['title'] = $article->title;
				$row['author'] = $article->author;
				$row['article_category'] = $article->article_category;
				$row['comment_count'] = $article->comment_count;
				$row['date_created'] =  $article->date_created;
				// $row['date_updated'] =  $article->date_updated;
				$row['status']		= $article->status;
				$row['action'] = "<a href='".site_url('article/edit')."/".$article->id."' class='btn btn-warning btn-edit btn-xs'>Edit</a>";

				if ( stripos($this->session->userdata("loggedIn")['privilege'],"Delete") !== FALSE){
					$row['action'] .= "&nbsp;";
					$row['action'] .= "<button type='button' class='btn btn-danger btn-xs btn-delete' data-id='{$article->id}'>Delete</button>";
				}

				$list_data['data'][] = $row;
			}

			return $list_data;
		}

		return array('data'=> array());
	}

	public function get_article_post()
	{
		$this->db->select('a.id, a.title, u.first_name as author, a.content, u.profile_image_url as author_photo, GROUP_CONCAT(c.name) as article_category, CASE COUNT(acm.article_id) WHEN 0 THEN "-" ELSE COUNT(acm.article_id) END as comment_count, a.date_created, a.date_updated, e.first_name as updater, CASE a.status WHEN 0 THEN "Draft" WHEN 1 THEN "Published" END as status', FALSE);
		$this->db->where('a.status != ', -1);
		$this->db->where('a.status != ', 0);
		$this->db->from('articles a');
		$this->db->join('article_category ac', 'ac.article_id = a.id', 'left');
		$this->db->join('category c', 'ac.category_id = c.id', 'left');
		$this->db->join('users u', 'a.author = u.id', 'left');
		$this->db->join('users e', 'a.last_editor = e.id', 'left');
		$this->db->join('article_comment acm', 'a.id = acm.article_id', 'left');
		$this->db->group_by('a.id');
		$this->db->order_by('a.date_created', 'desc');
	
		$result = $this->db->get()->result();
		
		// echo $this->db->last_query();

		return $result;
	}

	public function get_detail($article_id)
	{
		$this->db->select('a.id, a.author as author_id, u.first_name as author, a.title, a.subtitle, a.slug, a.content, a.featured_image, m.media_url as featured_image_url, a.date_created, a.date_updated, IFNULL(e.first_name,"-") as updater, IFNULL(a.date_updated,"-") as date_updated, IFNULL(a.date_published,"-") as date_published, a.featured_articles, CASE a.status WHEN 0 THEN "Draft" WHEN 1 THEN "Published" END as post_status, a.status', FALSE);
		$this->db->from('articles a');
		$this->db->join('media m', 'a.featured_image = m.id', 'left');
		$this->db->join('users u', 'a.author = u.id', 'left');
		$this->db->join('users e', 'a.last_editor = e.id', 'left');
		$this->db->where('a.id', $article_id);
		$post_detail = $this->db->get('articles')->row();

		$this->db->select('c.id, c.name');
		$this->db->from('category c');
		$this->db->join('article_category ac', 'ac.category_id = c.id', 'left');
		$this->db->where('article_id', $article_id);
		$post_category = $this->db->get()->result();

		$data = array(
			'post_detail'	=> $post_detail,
			'post_category'	=> $post_category
		);

		return $data;
	}

	public function post_article($type = 'add'){
		$this->db->trans_start();
		if($type == 'add'){
			$article_data = array(
				'author'	=> $this->session->userdata('loggedIn')['id'],
				'title'	=> $this->input->post('title', TRUE),
				'subtitle'	=> $this->input->post('subtitle', TRUE),
				'slug'	=> $this->input->post('slug', TRUE),
				'content'	=> $this->input->post('content'),
				'featured_image'	=> $this->input->post('featured_image'),
				'date_created'	=> date('Y-m-d H:i:s'),
				'status'	=> $this->input->post('status', TRUE),
				'featured_articles'	=> $this->input->post('featured_articles')
			);

			if($article_data['status'] === '1') 
				$article_data['date_published'] = date('Y-m-d H:i:s');

			if($this->db->insert('articles', $article_data)){
				$article_id = $this->db->insert_id();
				$article_category_data = array();

				$categories = $this->input->post('category_id', TRUE);

				if(count($categories) > 1){
					foreach($categories as $category_id){
						$article_category_data[] = array(
							'category_id'	=> $category_id,
							'article_id'	=> $article_id
						);
					}

					if(! $this->db->insert_batch('article_category', $article_category_data)){
						return FALSE;
					}
				} else{
					$article_category_data = array(
						'category_id'	=> $categories[0],
						'article_id'	=> $article_id
					);

					if(! $this->db->insert('article_category', $article_category_data)){
						return FALSE;
					}

					if(! $this->db->insert('author_article', array('article_id' => $article_id, 'author_id' => $this->session->userdata('loggedIn')['id']))){
						return FALSE;
					}
				}
			} else {
				return FALSE;
			}
		} else if($type = 'edit') {
			$article_id = $this->input->post('id', TRUE);
			$article_data = array(
				'title'	=> $this->input->post('title', TRUE),
				'subtitle'	=> $this->input->post('subtitle', TRUE),
				'slug'	=> $this->input->post('slug', TRUE),
				'content'	=> $this->input->post('content'),
				'featured_image'	=> $this->input->post('featured_image'),
				'date_updated'	=> date('Y-m-d H:i:s'),
				'last_editor'	=> $this->session->userdata('loggedIn')['id'],
				'status'	=> $this->input->post('status', TRUE),
				'featured_articles'	=> $this->input->post('featured_articles')
			);

			if($article_data['status'] === '1'){
				$article_data['date_published'] = date('Y-m-d H:i:s');
			}

			$this->db->where('id', $article_id);

			if($this->db->update('articles', $article_data)){
				$this->db->where('article_id', $article_id);
				$this->db->delete('article_category');
				if($this->db->affected_rows() > 0) {
					$article_category_data = array();

					$categories = $this->input->post('category_id', TRUE);

					if(count($categories) > 1){
						foreach($categories as $category_id){
							$article_category_data[] = array(
								'category_id'	=> $category_id,
								'article_id'	=> $article_id
							);
						}

						if(! $this->db->insert_batch('article_category', $article_category_data)){
							return FALSE;
						}
					} else{
						$article_category_data = array(
							'category_id'	=> $categories[0],
							'article_id'	=> $article_id
						);

						if(! $this->db->insert('article_category', $article_category_data)){
							return FALSE;
						}
					}
				}
			} else {
				return FALSE;
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE){
			return FALSE;
		}

		return TRUE;
	}

	public function delete($id){
		$this->db->trans_start();
		$this->db->set('status', -1);
		$this->db->where('article_id', $id);
		$this->db->update('article_category');
		if($this->db->affected_rows() > 0){
			$this->db->set('status', -1);
			$this->db->set('date_updated', date('Y-m-d H:i:s'));
			$this->db->set('last_editor', $this->session->userdata('loggedIn')['id']);
			$this->db->where('id', $id);
			$this->db->update('articles');
		}
		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE){
			return TRUE;
		}

		return FALSE;
	}

	public function bulk_edit()
	{
		$type = $this->input->post('type', TRUE);
		$articles = $this->input->post('articles', TRUE);

		$this->db->trans_start();
		switch($type){
			case 'publish':
				$this->db->where_in('id', $articles);
				$this->db->set('status', 1);
				$this->db->set('date_published', date('Y-m-d H:i:s'));
				$this->db->set('last_editor', $this->session->userdata('loggedIn'['id']));
				$this->db->update('articles');
				break;
			case 'unpublish':
				$this->db->where_in('id', $articles);
				$this->db->set('status', 0);
				$this->db->update('articles');
				break;
			case 'delete':
				foreach($articles as $aid){
					$this->db->set('status', -1);
					$this->db->where('article_id', $aid);
					$this->db->update('article_category');
				}
				$this->db->set('status', -1);
				$this->db->set('date_updated', date('Y-m-d H:i:s'));
				$this->db->where_in('id', $articles);
				$this->db->update('articles');
				break;
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE){
			return TRUE;
		}

		return FALSE;
	}

	public function get_article_date_dropdown()
	{
		$this->db->select('date_created');
		$this->db->group_by('DATE_FORMAT(date_created, "%Y-%m")');
		$result = $this->db->get('articles')->result();

		return $result;
	}

	public function get_article_category_dropdown()
	{
		$this->db->select('c.id, c.name');
		$this->db->from('category c');
		$this->db->join('article_category ac', 'ac.category_id = c.id', 'inner');
		$this->db->group_by('c.id');
		$result = $this->db->get()->result();

		return $result;
	}

	public function get_article_author_dropdown()
	{
		$this->db->select('a.author, u.first_name');
		$this->db->from('users u');
		$this->db->join('articles a', 'a.author = u.id', 'inner');
		$this->db->group_by('a.author');
		$result = $this->db->get()->result();

		return $result;
	}

}

/* End of file Article_model.php */
/* Location: ./application/models/Article_model.php */