<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image_model extends CI_Model {

	public function load_gallery(){
		$list = array();
		$this->db->like('media_type', 'image', 'RIGHT');
		$this->db->where('status != ', -1);
		$result = $this->db->get('media')->result();

		foreach($result as $media){
			$list[] = array(
				'id'	=> $media->id,
				'url'	=> base_url($media->media_url),
				'thumb'	=> base_url($media->thumb_url)
			);
		}
		return $list;
	}

	public function delete($id) {
		$this->db->where('id', $id);
		$media = $this->db->get('media')->row();

		if(is_writable(getcwd().'/'.$media->media_url)){
			unlink(getcwd().'/'.$media->media_url);
		}

		if(is_writable(getcwd().'/'.$media->thumb_url)){
			unlink(getcwd().'/'.$media->thumb_url);
		}

		$this->db->set('status', -1);
		$this->db->where('id', $id);
		$this->db->update('media');

		if($this->db->affected_rows() > 0){
			return TRUE;
		}

		return FALSE;
	}
}

/* End of file Image_model.php */
/* Location: ./application/models/Image_model.php */