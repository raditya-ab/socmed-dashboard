<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilege_model extends CI_Model {

	public function get_privilege_list()
	{
		$this->db->select('gp.permission_id, pl.permission_name, gp.permission_type, GROUP_CONCAT(ug.group_name) as group_name');
		$this->db->from('group_permissions gp');
		$this->db->join('permission_list pl', 'pl.id = gp.permission_id', 'left');
		$this->db->join('usergroups ug', 'ug.group_id = gp.group_id', 'left');
		$this->db->where('permission_type', 1);
		$this->db->group_by('permission_id');
		$result = $this->db->get()->result();
		// echo $this->db->last_query();

		if(count($result) > 0){
			$list_data = array();
			foreach($result as $i => $permission) {
				$row = array();

				$row['no'] = ++$i;
				$row['permission_name'] = $permission->permission_name;
				$row['group'] = ucwords($permission->group_name);
				$row['action'] = "<button class='btn btn-flat btn-xs btn-info btn-edit-priv' data-id='{$permission->permission_id}'>Edit</button>";

				$list_data['data'][] = $row;
			}

			return $list_data;
		}

		return array();
	}

	public function get_group_permission()
	{
		$permission_id = $this->input->post('permission_id', TRUE);

		$this->db->select('gp.permission_id, pl.permission_name, gp.permission_type, gp.group_id, ug.group_name');
		$this->db->from('group_permissions gp');
		$this->db->join('permission_list pl', 'pl.id = gp.permission_id', 'left');
		$this->db->join('usergroups ug', 'ug.group_id = gp.group_id', 'left');
		$this->db->where('gp.permission_id', $permission_id);
		$result = $this->db->get()->result();

		return $result;
	}

	public function get_permission_list()
	{
		$result = $this->db->get('permission_list')->result();

		return $result;
	}
	public function get_group_list()
	{
		$result = $this->db->get('usergroups')->result();

		return $result;
	}

	public function add($type){
		$list = array();

		$this->db->trans_start();
		if($type == 'group') {
			
			if($this->db->insert('usergroups', array('group_name' => $this->input->post('group_name', TRUE)))){
				$group_id = $this->db->insert_id();
				$permission_list = $this->get_permission_list();
				$given_permission = $this->input->post('permission_name');
				foreach($permission_list as $permission){
					$list[] = array(
						'permission_id'	=> $permission->id,
						'permission_type'	=> (array_search($permission->permission_name, $given_permission) !== FALSE) ? 1 : 0,
						'group_id'	=> $group_id
					);
				}

				$this->db->insert_batch('group_permissions', $list);
			}
		} else if($type == 'permission') {
			if($this->db->insert('permission_list', array('permission_name' => $this->input->post('permission_name', TRUE)))){
				$permission_id = $this->db->insert_id();
				$group_list = $this->get_group_list();
				$given_group = $this->input->post('group_name');
				foreach($group_list as $group){
					$list[] = array(
						'permission_id'	=> $permission_id,
						'permission_type'	=> (array_search($group->group_name, $given_group) !== FALSE) ? 1 : 0,
						'group_id'	=> $group->group_id
					);
				}

				$this->db->insert_batch('group_permissions', $list);
			}
		}
		$this->db->trans_complete();

		if($this->db->trans_status() == TRUE){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function edit()
	{
		$update_data = array();
		$priv_id = $this->input->post('permission_id');

		$user = $this->input->post('rbo_user');
		$editor = $this->input->post('rbo_editor');
		$admin = $this->input->post('rbo_admin');

		$group_list = $this->get_group_list();		

		$this->db->trans_start();
		foreach($group_list as $group) {
			$this->db->where('permission_id', $priv_id);
			$this->db->where('group_id', $group->group_id);
			$this->db->set('permission_type', $this->input->post('rbo_'.$group->group_name));
			$this->db->update('group_permissions');
		}
		$this->db->trans_complete();

		if($this->db->trans_status() === TRUE){
			return TRUE;
		}

		return FALSE;
	}
	private function fetch_default_permission()
	{

	}
	

}

/* End of file Privilege_model.php */
/* Location: ./application/models/Privilege_list.php */