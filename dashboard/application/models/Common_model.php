<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function get_total_user($provider = NULL) {
		if($provider == 'Google') {
			$this->db->select('COUNT(*) as num');
			$this->db->where('provider_name', 'Google');
			$result = $this->db->get('user_provider')->row();
		} else if($provider == 'Facebook') {
			$this->db->select('COUNT(*) as num');
			$this->db->where('provider_name', 'Facebook');
			$result = $this->db->get('user_provider')->row();
		} else if($provider == 'Twitter') {
			$this->db->select('COUNT(*) as num');
			$this->db->where('provider_name', 'Twitter');
			$result = $this->db->get('user_provider')->row();
		} else if($provider == NULL) {
			$query = $this->db->query("SELECT COUNT(*) as `num` FROM `users` WHERE `id` NOT IN (SELECT `user_id` FROM `user_provider`)");
			$result = $query->row();
		}
		
		return $result;
	}

	public function get_category()
	{
		$this->db->select('id, name');
		$this->db->where('status', 1);
		$this->db->order_by('name', 'asc');
		$result = $this->db->get('category')->result();

		return $result;
	}

	public function store_media($data) {
		$mime_type = getMimeType($_FILES['uploaded_file']['tmp_name']);
		
		if(stripos($mime_type,'image') !== FALSE){
			$dir = "/uploads/img/";
		} else if(stripos($mime_type,'application/pdf') !== FALSE){
			$dir = "/uploads/file/";
		} else if(stripos($mime_type,'video') !== FALSE){
			$dir = "/uploads/video/";
		}

		$media_data = array(
			'media_url'     => $dir."$data[raw_name]".$data['file_ext'],
			'thumb_url'		=> $data['thumb_url'],
			'media_name'    => $data['raw_name'],
			'media_ext'     => $data['file_ext'],
			'media_size'    => $data['file_size'],
			'media_type'    => $data['file_type'],
			'date_uploaded' => date('Y-m-d H:i:s'),
			'uploader'      => $this->session->userdata('loggedIn')['id']
		);

		if($this->db->insert('media', $media_data)){
			return TRUE;
		}

		return FALSE;
	}
	

}

/* End of file Common_model.php */
/* Location: ./application/models/Common_model.php */