<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {
	public function add()
	{
		$data = array(
			'name'	=> $this->input->post('category_name'),
			'date_created'	=> date('Y-m-d H:i:s')
		);

		if($this->db->insert('category', $data)){
			return array(
				'status'	=> 1,
				'id'	=> $this->db->insert_id(),
				'category_name'	=> $data['name']
			);
		} else {
			return array(
				'status'	=> 0,
				'message'	=> $this->db->_error_message()
			);
		}
	}
	

}

/* End of file Category_model.php */
/* Location: ./application/models/Category_model.php */