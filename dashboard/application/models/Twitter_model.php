<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Twitter_model extends CI_Model {

	private $conn;
	private $credentials;

	public function __construct()
	{
		$this->config->load('twitter_oauth');

		$this->credentials = $this->config->item('twitteroauth');

		parent::__construct();
		$this->load->library('hybridauth');
		$this->load->helper('arrayhelper_helper');

		
	}

	public function grab_tweet_for_download($hashtag, $daterange = NULL, $select_param)
	{
		$this->db->select($select_param);
		$this->db->from('tweets t');
		$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->where('h.hashtag', $hashtag);
		if(! is_null($daterange)){
			$start_date = explode(" - ", $daterange)[0];
			$end_date = explode(" - ", $daterange)[1];

			$this->db->where('t.created_at >=', $start_date);
			$this->db->where('t.created_at <=', $end_date);
		}
		$this->db->order_by('t.created_at', 'desc');

		$tweet_list = $this->db->get()->result_array();

		$oldest_tweet = $this->get_oldest_hashtag_tweet($hashtag);

		if(count($tweet_list) > 0 && count($oldest_tweet) > 0){
			return array(
				'status'	=> 1,
				'result'	=> array(
					'tweet_list'   => $tweet_list,
					'oldest_tweet' => $oldest_tweet
				)
			);
		} else {
			return array(
				'status'	=> 0,
				'result'	=> "No Result"
			);
		}
	}
	public function get_oldest_hashtag_tweet($hashtag)
	{
		$this->db->select('t.id, t.text, t.created_at, t.tweet_uid, u.name, u.screen_name');
		$this->db->from('tweets t');
		$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->where('h.hashtag', $hashtag);
		$this->db->where_in('t.created_at',"SELECT MIN(t2.created_at) FROM tweets t2", FALSE);

		$result = $this->db->get()->row_array();

		return $result;
	}
	public function lookupTweet($id, $params = NULL)
	{
		$parameter   = array();
		$tweets      = array();

		// A comma separated list of Tweet IDs, up to 100 are allowed in a single request.
		if(is_array($id)){
			$parameter['id'] = implode(",", $id);
		} else {
			$parameter['id'] = $id;
		}

		if( !is_null($params) || !empty($params)) {
			// The entities node that may appear within embedded statuses will not be included when set to false.
			if(array_key_exists('include_entities', $params)) { 
				$parameter['include_entities'] = empty($params['include_entities']) ? true : false;
			}

			// When set to either true , t or 1 , each Tweet returned in a timeline will include a user object including only the status authors numerical ID. Omit this parameter to receive the complete user object.
			if(array_key_exists('trim_user', $params)) {
				$parameter['trim_user'] = empty($params['trim_user']) ? false : true;
			}

			// When using the map parameter, Tweets that do not exist or cannot be viewed by the current user will still have their key represented but with an explicitly null value paired with it
			if(array_key_exists('map', $params)) {
				$parameter['map'] = empty($params['map']) ? true : false;
			}

			// If alt text has been added to any attached media entities, this parameter will return an ext_alt_text value in the top-level key for the media entity. If no value has been set, this will be returned as null
			if(array_key_exists('include_ext_alt_text', $params)) {
				$parameter['include_ext_alt_text'] = empty($params['include_ext_alt_text']) ? true : false;
			}
		}

		$this->conn = new Abraham\TwitterOAuth\TwitterOAuth($this->credentials['consumer_key'],$this->credentials['consumer_secret'],$this->credentials['token'],$this->credentials['token_secret']);
		$response = $this->conn->get('statuses/lookup', $parameter);

		echo "<pre>";
		print_r ($response);
		echo "</pre>";

	}

	public function testing_curl(){
		$key = $this->credentials['consumer_key'];
		$secret = $this->credentials['consumer_secret'];
		$api_endpoint = 'https://api.twitter.com/1.1/search/tweets.json?q=%23PahamiAturanTaxiOnline&result_type=recent&count=100&until=2017-10-26&include_entities=true'; // endpoint must support "Application-only authentication"

		// request token
		$basic_credentials = base64_encode($key.':'.$secret);
		$tk = curl_init('https://api.twitter.com/oauth2/token');
		curl_setopt($tk, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$basic_credentials, 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
		curl_setopt($tk, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
		curl_setopt($tk, CURLOPT_RETURNTRANSFER, true);
		$token = json_decode(curl_exec($tk));
		curl_close($tk);

		// use token
		if (isset($token->token_type) && $token->token_type == 'bearer') {
			$br = curl_init($api_endpoint);
			curl_setopt($br, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token->access_token));
			curl_setopt($br, CURLOPT_RETURNTRANSFER, true);
			$data = curl_exec($br);
			curl_close($br);
		  	
			// do_something_here_with($data);
		}
		return json_decode($data);
	}
	public function searchTweets($search_string, $params = NULL)
	{
		$parameter   = array();
		$tweets      = array();
		$hashtags    = array();
		$tweet_urls  = array();
		$users       = array();
		$pure_result = array();
		$tweet_media = array();

		if(substr($search_string, 0, 1) == '#'){
			$parameter['q'] = $search_string;
		} else {
			$parameter['q']	= '#'.$search_string;
		}

		if( !is_null($params) || !empty($params)){
			if(array_key_exists('count', $params)){
				$parameter['count'] = (empty($params['count'])) ? 21 : $params['count']; // How many data to be grabbed
			} else {
				$parameter['count'] = 21;
			}

			if(array_key_exists('result_type', $params)){
				$parameter['result_type'] = $params['result_type']; //mixed, recent, or popular
			}

			if(array_key_exists('until', $params)){
				$parameter['until'] = $params['until']; // Date threshold of the tweets
			}

			if(array_key_exists('since_id', $params)){
				$parameter['since_id'] = $params['since_id']; // Search tweet with this id...
			}

			if(array_key_exists('max_id', $params)){
				$parameter['max_id'] = $params['max_id']; // Until this id.
			}

			if(array_key_exists('include_entities', $params)){
				$parameter['include_entities'] = $params['include_entities']; // true/false. Depends on whether to include tweet entities or not
			}
		}

		$this->conn = new Abraham\TwitterOAuth\TwitterOAuth($this->credentials['consumer_key'],$this->credentials['consumer_secret'],$this->credentials['token'],$this->credentials['token_secret']);
		$response   = $this->conn->get('search/tweets', $parameter);

		// $adapter = $this->hybridauth->HA->authenticate('Twitter');
		// $response = $adapter->api()->get('search/tweets.json', $parameter);

		// check the last HTTP status code returned
		if (property_exists($response, 'errors')) {
			return array(
				'status'	=> 0,
				'response'	=> array(
					'code'	=> $response->errors[0]->code,
					'message'	=> $response->errors[0]->message,
				)
			);
			// throw new Exception("Search request failed! {$this->providerId} returned an error. " . $this->errorMessageByStatus($this->api->http_code));
		}

		if ($response && count($response)) {
			$sanitized_result = array();
			foreach ($response->statuses as $item) {
				$result_to_show = array();
				$tweet = array();
				$user  = array();

				$id = (property_exists($item, 'id_str')) ? $item->id_str : "";

				if($this->tweet_is_exist($id) == FALSE){
					$tweet['id']         = (property_exists($item, 'id_str')) ? $item->id_str : "";
					$tweet['text']       = (property_exists($item, 'text')) ? $item->text : "";
					$tweet['created_at'] = (property_exists($item, 'created_at')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";
					$tweet['tweet_uid']  = (property_exists($item, 'user')) ? $item->user->id : "";

					$result_to_show['id'] = $tweet['id'];
					$result_to_show['text'] = $tweet['text'];
					$result_to_show['created_at'] = $tweet['created_at'];
					$result_to_show['tweet_uid'] = $tweet['tweet_uid'];

					if(count($item->entities->hashtags) > 0){
						foreach($item->entities->hashtags as $hashtag_item){
							$hashtag = array();

							$hashtag['tweet_id'] = $tweet['id'];
							$hashtag['hashtag']  = (property_exists($hashtag_item, 'text')) ? $hashtag_item->text : "";

							$hashtags[] = $hashtag;
							$result_to_show['hashtag'][] = $hashtag['hashtag'];

						}
					}

					

					if(property_exists($item, 'retweeted_status'))
					{
						if(property_exists($item->retweeted_status, 'extended_entities')) {
							if(property_exists($item->retweeted_status->extended_entities, 'media')) {
								if(count($item->retweeted_status->extended_entities->media) > 0) {
									foreach($item->retweeted_status->extended_entities->media as $media_item) {
										$media = array();

										$media['media_id']	= (property_exists($media_item, 'id_str')) ? $media_item->id_str : "";
										$media['tweet_id']	= $tweet['id'];
										$media['media_type']	= (property_exists($media_item, 'type')) ? $media_item->type : "";
										
										if($media['media_type'] == 'photo') {
											$media['media_url'] = (property_exists($media_item, 'media_url')) ? $media_item->media_url : "";
										} else if($media['media_type'] == 'video') {
											$video_taken = array();
											$video_no_mp4 = array();
											foreach($media_item->video_info->variants as $var){
												if(property_exists($var, 'bitrate')) {
													if(empty($video_taken)){
														$video_taken['bitrate'] = (int)$var->bitrate;
														$video_taken['content_type'] = $var->content_type;
														$video_taken['url']	= $var->url;
													} else {
														if(!empty($video_taken) && ($var->content_type == 'video/mp4')){
															if((int)$var->bitrate < $video_taken['bitrate']) {
																$video_taken['bitrate'] = (int)$var->bitrate;
																$video_taken['content_type'] = $var->content_type;
																$video_taken['url']	= $var->url;
															}
														}
													}
												} else {
													$video_no_mp4['content_type'] = $var->content_type;
													$video_no_mp4['url']	= $var->url;
												}
											}

											if(! empty($video_taken)){
												$media['media_type'] = $video_taken['content_type'];
												$media['media_url'] = $video_taken['url'];
											}else {
												$media['media_type'] = $video_no_mp4['content_type'];
												$media['media_url'] = $video_no_mp4['url'];
											}
										}

										$tweet_media[] = $media;

										$result_to_show['media'][] = array(
											'media_id'	=> $media['media_id'],
											'media_type'	=> $media['media_type'],
											'media_url'	=> $media['media_url']
										);
									}
								}
							}
						}

						if(count($item->retweeted_status->entities->urls) > 0) {
							 foreach($item->retweeted_status->entities->urls as $url_item) {
							 	$tweet_url = array();

							 	$tweet_url['tweet_id'] = $tweet['id'];
							 	$tweet_url['url']      = (property_exists($url_item, 'url')) ? $url_item->url : "";

							 	$tweet_urls[] = $tweet_url;

							 	$result_to_show['url'][] = $tweet_url['url'];
							 }
						}
					}
					else
					{
						if(count($item->entities->urls) > 0){
							foreach($item->entities->urls as $url_item){
								$tweet_url = array();

								$tweet_url['tweet_id'] = $tweet['id'];
								$tweet_url['url']      = (property_exists($url_item, 'url')) ? $url_item->url : "";

								$tweet_urls[] = $tweet_url;
								$result_to_show['url'][] = $tweet_url['url'];
							}
						}

						if(property_exists($item, 'extended_entities')) {
							if(property_exists($item->extended_entities, 'media')) {
								foreach($item->extended_entities->media as $media_item) {
									$media = array();

									$media['media_id']	= (property_exists($media_item, 'id_str')) ? $media_item->id_str : "";
									$media['tweet_id']	= $tweet['id'];
									$media['media_type']	= (property_exists($media_item, 'type')) ? $media_item->type : "";
									
									if($media['media_type'] == 'photo') {
										$media['media_url'] = (property_exists($media_item, 'media_url')) ? $media_item->media_url : "";
									} else {
										$media['media_type'] = $media_item->video_info->variants[0]->content_type;
										$media['media_url'] = $media_item->video_info->variants[0]->url;
									}

									$tweet_media[] = $media;

									$result_to_show['media'][] = array(
										'media_id'	=> $media['media_id'],
										'media_type'	=> $media['media_type'],
										'media_url'	=> $media['media_url']
									);
								} 
							}
						}
					}

					$user['id']                = (property_exists($item, 'user')) ? $item->user->id : "";
					$user['name']              = (property_exists($item, 'user')) ? $item->user->name : "";
					$user['screen_name']       = (property_exists($item, 'user')) ? $item->user->screen_name : "";
					$user['location']          = (property_exists($item, 'user')) ? $item->user->location : "";
					$user['description']       = (property_exists($item, 'user')) ? $item->user->description : "";
					$user['followers_count']   = (property_exists($item, 'user')) ? $item->user->followers_count : "";
					$user['friends_count']     = (property_exists($item, 'user')) ? $item->user->friends_count : "";
					$user['listed_count']      = (property_exists($item, 'user')) ? $item->user->listed_count : "";
					$user['favorites_count']   = (property_exists($item, 'user')) ? $item->user->favourites_count : "";
					$user['statuses_count']    = (property_exists($item, 'user')) ? $item->user->statuses_count : "";
					$user['profile_image_url'] = (property_exists($item, 'user')) ? $item->user->profile_image_url : "";
					$user['created_at']        = (property_exists($item, 'user')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->user->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";

					if(empty($users)){
						$users[] = $user;
					} else {
						if($this->searchForId($user['id'], $users) === FALSE){
							$users[] = $user;
						}
					}

					$tweets[] = $tweet;

					$result_to_show['user'] = $user;

					$pure_result[] = $item;
					$sanitized_result[] = $result_to_show;
				}
			}
			$return_data = array(
				'status'      => 1,
				'response' => array(
					'pure_result' => $pure_result,
					'sanitized_result'	=> $sanitized_result,
					'tweets'      => $tweets,
					'users'       => $users,
					'hashtags'    => $hashtags,
					'urls'        => $tweet_urls,
					'media'       => $tweet_media,
					'metas'       => array(
						'max_id'      => $response->search_metadata->max_id,
						'query'       => $response->search_metadata->query,
						'refresh_url' => (property_exists($response->search_metadata, 'refresh_url')) ? $response->search_metadata->refresh_url : "",
						'next_result' => (property_exists($response->search_metadata, 'next_results')) ? $response->search_metadata->next_results : ""
					)
				)
			);

			if(count($tweets) == 0 || count($pure_result) == 0 || count($users) == 0){
				$return_data['status'] = -1;
			}

			return $return_data;
		}
	}

	public function searchUser($search_string, $params)
	{
		$parameter = array();
		$users = array();
		
		$parameter['q']	= urlencode($search_string);

		if( !is_null($params) || !empty($params)){
			if(array_key_exists('count', $params)){
				$parameter['count'] = $params['count']; // How many data to be grabbed
			}

			if(array_key_exists('page', $params)){
				$parameter['page'] = $params['page']; //Specifies the page of results to retrieve.
			}

			if(array_key_exists('include_entities', $params)){
				$parameter['include_entities'] = $params['include_entities']; // true/false. Depends on whether to include tweet entities or not
			}
		}

		$this->conn = new Abraham\TwitterOAuth\TwitterOAuth($this->credentials['consumer_key'],$this->credentials['consumer_secret'],$this->credentials['token'],$this->credentials['token_secret']);
		$response = $this->conn->get('users/search', $parameter);
		
		// check the last HTTP status code returned
		// if (property_exists($response[0], 'errors')) {
		// 	return array(
		// 		'status'	=> 0,
		// 		'response'	=> array(
		// 			'code'	=> $response->errors[0]->code,
		// 			'message'	=> $response->errors[0]->message,
		// 		)
		// 	);
		// 	// throw new Exception("Search request failed! {$this->providerId} returned an error. " . $this->errorMessageByStatus($this->api->http_code));
		// }

		if (empty($response)) {
			return array(
				'status'	=> 0,
				'response'	=> array(
					'code'	=> 0,
					'message'	=> 'No user with this screen_name',
				)
			);
		}

		if ($response && count($response)) {
			foreach ($response as $item) {
				$user = array();

				$user['id']                = (property_exists($item, 'id_str')) ? $item->id_str : "";
				$user['name']              = (property_exists($item, 'name')) ? $item->name : "";
				$user['screen_name']       = (property_exists($item, 'screen_name')) ? $item->screen_name : "";
				$user['location']          = (property_exists($item, 'location')) ? $item->location : "";
				$user['description']       = (property_exists($item, 'description')) ? $item->description : "";
				$user['followers_count']   = (property_exists($item, 'followers_count')) ? $item->followers_count : "";
				$user['friends_count']     = (property_exists($item, 'friends_count')) ? $item->friends_count : "";
				$user['listed_count']      = (property_exists($item, 'listed_count')) ? $item->listed_count : "";
				$user['favorites_count']   = (property_exists($item, 'favourites_count')) ? $item->favourites_count : "";
				$user['statuses_count']    = (property_exists($item, 'statuses_count')) ? $item->statuses_count : "";
				$user['last_tweet_date']   = (property_exists($item, 'status')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->status->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";
				$user['last_tweet_text']   = (property_exists($item, 'status')) ? $item->status->text : "";
				$user['profile_image_url'] = (property_exists($item, 'profile_image_url')) ? $item->profile_image_url : "";
				$user['created_at']        = (property_exists($item, 'created_at')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";
				
				$users[]                   = $user;
			}
			
			$return_data = array(
				'status'      => 1,
				'response' => array(
					'users'       => $users
				)
			);

			if( count($users) == 0 ){
				$return_data['status'] = 0;

			}

			return $return_data;
		}
	}

	public function lookupUser($user_list, $search_type = 'screen_name')
	{
		$parameter = array();
		$users = array();
		
		if($search_type === 'screen_name'){
			$parameter['screen_name']	= $user_list; //search by username, e.g someone_cool
		} else if($search_type === 'user_id'){
			$parameter['user_id']	= $user_list; // search by user id, e.g 111020394929
		}

		$this->conn = new Abraham\TwitterOAuth\TwitterOAuth($this->credentials['consumer_key'],$this->credentials['consumer_secret'],$this->credentials['token'],$this->credentials['token_secret']);
		$response = $this->conn->get('users/lookup', $parameter);
		
		if(empty($response)){
			return array(
				'status'	=> 0,
				'response'	=> array(
					'code'	=> 0,
					'message'	=> 'No user with this screen_name',
				)
			);
		}

		if ($response && count($response)) {
			foreach ($response as $item) {
				
				$user = array();

				$user['id']                = (property_exists($item, 'id_str')) ? $item->id_str : "";
				$user['name']              = (property_exists($item, 'name')) ? $item->name : "";
				$user['screen_name']       = (property_exists($item, 'screen_name')) ? $item->screen_name : "";
				$user['location']          = (property_exists($item, 'location')) ? $item->location : "";
				$user['description']       = (property_exists($item, 'description')) ? $item->description : "";
				$user['followers_count']   = (property_exists($item, 'followers_count')) ? $item->followers_count : "";
				$user['friends_count']     = (property_exists($item, 'friends_count')) ? $item->friends_count : "";
				$user['listed_count']      = (property_exists($item, 'listed_count')) ? $item->listed_count : "";
				$user['favorites_count']   = (property_exists($item, 'favourites_count')) ? $item->favourites_count : "";
				$user['statuses_count']    = (property_exists($item, 'statuses_count')) ? $item->statuses_count : "";
				$user['last_tweet_date']   = (property_exists($item, 'status')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->status->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";
				$user['last_tweet_text']   = (property_exists($item, 'status')) ? $item->status->text : "";
				$user['profile_image_url'] = (property_exists($item, 'profile_image_url')) ? $item->profile_image_url : "";
				$user['created_at']        = (property_exists($item, 'created_at')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";
				
				$users[]                   = $user;
			}

			$return_data = array(
				'status'      => 1,
				'response' => array(
					'users'       => $users
				)
			);

			return $return_data;
		}
	}

	public function retrieve_user_timeline($screen_name, $params = NULL)
	{
		$parameter         = array();
		$tweets            = array();
		$hashtags          = array();
		$tweet_urls        = array();
		$pure_result       = array();
		$tweet_media       = array();
		$tweet_create_date = array();

		$params['save']		= true;
		// $parameter['q']	= urlencode('#'.$search_string);

		if($this->input->is_ajax_request()){
			$parameter['screen_name'] = $screen_name; // The ID of the user for whom to return results.

			if( !is_null($params) || !empty($params)){
				if(array_key_exists('since_id', $params)){
					$parameter['since_id'] = $params['since_id']; // Returns results with an ID greater than (that is, more recent than) the specified ID. There are limits to the number of Tweets that can be accessed through the API. If the limit of Tweets has occured since the since_id, the since_id will be forced to the oldest ID available.
				}

				if(array_key_exists('count', $params)){
					$parameter['count'] = (empty($params['count'])) ? 100 : $params['count']; // Specifies the number of Tweets to try and retrieve, up to a maximum of 200 per distinct request. The value of count is best thought of as a limit to the number of Tweets to return because suspended or deleted content is removed after the count has been applied. We include retweets in the count, even if include_rts is not supplied. It is recommended you always send include_rts=1 when using this API method.
				} else {
					$parameter['count'] = 100;
				}

				if(array_key_exists('max_id', $params)){
					$parameter['max_id'] = $params['max_id']; // Returns results with an ID less than (that is, older than) or equal to the specified ID.
				}

				if(array_key_exists('trim_user', $params)){
					$parameter['trim_user'] = $params['trim_user']; //t/true/1, When set to either true , t or 1 , each Tweet returned in a timeline will include a user object including only the status authors numerical ID. Omit this parameter to receive the complete user object.
				}

				if(array_key_exists('exclude_replies', $params)){
					$parameter['exclude_replies'] = $params['exclude_replies']; // true/false. This parameter will prevent replies from appearing in the returned timeline. Using exclude_replies with the count parameter will mean you will receive up-to count tweets — this is because the count parameter retrieves that many Tweets before filtering out retweets and replies.
				}

				if(array_key_exists('include_rts', $params)){
					$parameter['include_rts'] = $params['include_rts']; // true/false. When set to false , the timeline will strip any native retweets (though they will still count toward both the maximal length of the timeline and the slice selected by the count parameter). Note: If you’re using the trim_user parameter in conjunction with include_rts, the retweets will still contain a full user object.
				}

				if(array_key_exists('save', $params)){
					$save = $params['save'];
				}
			}

			$this->conn = new Abraham\TwitterOAuth\TwitterOAuth($this->credentials['consumer_key'],$this->credentials['consumer_secret'],$this->credentials['token'],$this->credentials['token_secret']);
			$response = $this->conn->get('statuses/user_timeline', $parameter);

			// check the last HTTP status code returned
			if (empty($response)) {
				return array(
					'status'	=> 0,
					'response'	=> array(
						'code'	=> '404',
						'message'	=> 'No Tweets',
					)
				);
				// throw new Exception("Search request failed! {$this->providerId} returned an error. " . $this->errorMessageByStatus($this->api->http_code));
			}

			if ($response && count($response)) {
				$result_to_show = array();
				$tweet  = array();
				$user = array();

				foreach ($response as $item_index => $item) {
					if($item_index == 5) break;
					$tweet = array();
					$user  = array();
					$result_to_show = array();

					$id = (property_exists($item, 'id_str')) ? $item->id_str : "";

					// if($this->tweet_is_exist($id) == FALSE){
						$tweet['id']         = (property_exists($item, 'id_str')) ? $item->id_str : "";
						$tweet['text']       = (property_exists($item, 'text')) ? $item->text : "";
						$tweet['created_at'] = (property_exists($item, 'created_at')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";
						$tweet['tweet_uid']  = (property_exists($item, 'user')) ? $item->user->id : "";

						$result_to_show['id'] = $tweet['id'];
						$result_to_show['text'] = $tweet['text'];
						$result_to_show['created_at'] = $tweet['created_at'];

						if(!in_array(date('Y-m-d', strtotime($result_to_show['created_at'])), $tweet_create_date)){
							array_push($tweet_create_date, date('Y-m-d', strtotime($result_to_show['created_at'])));
						}

						if(count($item->entities->hashtags) > 0){
							foreach($item->entities->hashtags as $hashtag_item){
								$hashtag = array();

								$hashtag['tweet_id'] = $tweet['id'];
								$hashtag['hashtag']  = (property_exists($hashtag_item, 'text')) ? $hashtag_item->text : "";

								$hashtags[] = $hashtag;

								$result_to_show['hashtag'][] = $hashtag['hashtag'];
							}
						}

						

						if(property_exists($item, 'retweeted_status'))
						{
							if(count($item->retweeted_status->entities->urls) > 0) {
								 foreach($item->retweeted_status->entities->urls as $url_item) {
								 	$tweet_url = array();

								 	$tweet_url['tweet_id'] = $tweet['id'];
								 	$tweet_url['url']      = (property_exists($url_item, 'url')) ? $url_item->url : "";

								 	$tweet_urls[] = $tweet_url;

								 	$result_to_show['url'][] = $tweet_url['url'];
								 }
							}

							if(property_exists($item->retweeted_status, 'extended_entities')) {
								if(property_exists($item->retweeted_status->extended_entities, 'media')) {
									if(count($item->retweeted_status->extended_entities->media) > 0) {
										foreach($item->retweeted_status->extended_entities->media as $media_item) {
											$media = array();

											$media['media_id']	= (property_exists($media_item, 'id_str')) ? $media_item->id_str : "";
											$media['tweet_id']	= $tweet['id'];
											$media['media_type']	= (property_exists($media_item, 'type')) ? $media_item->type : "";
											
											if($media['media_type'] == 'photo') {
												$media['media_url'] = (property_exists($media_item, 'media_url')) ? $media_item->media_url : "";
											} else {
												$media['media_type'] = $media_item->video_info->variants[0]->content_type;
												$media['media_url'] = $media_item->video_info->variants[0]->url;
											}

											$tweet_media[] = $media;

											$result_to_show['media'][] = array(
												'media_id'	=> $media['media_id'],
												'media_type'	=> $media['media_type'],
												'media_url'	=> $media['media_url']
											);
										} 
									}
								}
							}
						}
						else
						{
							if(count($item->entities->urls) > 0){
								foreach($item->entities->urls as $url_item){
									$tweet_url = array();

									$tweet_url['tweet_id'] = $tweet['id'];
									$tweet_url['url']      = (property_exists($url_item, 'url')) ? $url_item->url : "";

									$tweet_urls[] = $tweet_url;

									$result_to_show['url'][] = $tweet_url['url'];
								}
							}

							if(property_exists($item, 'extended_entities')) {
								if(property_exists($item->extended_entities, 'media')) {
									foreach($item->extended_entities->media as $index => $media_item) {
										
										$media = array();

										$media['media_id']	= (property_exists($media_item, 'id_str')) ? $media_item->id_str : "";
										$media['tweet_id']	= $tweet['id'];
										$media['media_type']	= (property_exists($media_item, 'type')) ? $media_item->type : "";
										
										if($media['media_type'] == 'photo') {
											$media['media_url'] = (property_exists($media_item, 'media_url')) ? $media_item->media_url : "";
										} else {
											$media['media_type'] = $media_item->video_info->variants[0]->content_type;
											$media['media_url'] = $media_item->video_info->variants[0]->url;
										}

										$tweet_media[] = $media;
										$result_to_show['media'][] = array(
											'media_id'	=> $media['media_id'],
											'media_type'	=> $media['media_type'],
											'media_url'	=> $media['media_url']
										);
									} 
								}
							}
						}

						$tweets[] = $tweet;

						$user['id']                = (property_exists($item, 'user')) ? $item->user->id_str : "";
						$user['name']              = (property_exists($item, 'user')) ? $item->user->name : "";
						$user['screen_name']       = (property_exists($item, 'user')) ? $item->user->screen_name : "";
						$user['location']          = (property_exists($item, 'user')) ? $item->user->location : "";
						$user['description']       = (property_exists($item, 'user')) ? $item->user->description : "";
						$user['followers_count']   = (property_exists($item, 'user')) ? $item->user->followers_count : "";
						$user['friends_count']     = (property_exists($item, 'user')) ? $item->user->friends_count : "";
						$user['listed_count']      = (property_exists($item, 'user')) ? $item->user->listed_count : "";
						$user['favorites_count']   = (property_exists($item, 'user')) ? $item->user->favourites_count : "";
						$user['statuses_count']    = (property_exists($item, 'user')) ? $item->user->statuses_count : "";
						$user['profile_image_url'] = (property_exists($item, 'user')) ? $item->user->profile_image_url : "";
						$user['created_at']        = (property_exists($item, 'user')) ? date('Y-m-d H:i:s', strtotime(DateTime::createFromFormat('D M j H:i:s P Y', $item->user->created_at)->format('Y-m-d H:i:s')) + (3600 * 7)) : "";

						$pure_result[] = $result_to_show;
					// }
				}

				if(! empty($user)){
					$user['last_tweet_text']	= $tweets[0]['text'];
					$user['last_tweet_date']	= $tweets[0]['created_at'];
				}

				$return_data = array(
					'status'      => 1,
					'response' => array(
						'user'	=> $user,
						'pure_result' => $pure_result,
						'hashtags'    => $hashtags,
						'tweet_date'	=> $tweet_create_date
					)
				);

				if(count($tweets) == 0 || count($pure_result) == 0 || count($user) == 0){
					$return_data['status'] = -1;
				}
				else
				{
					if($save){
						$save_data = array();

						$save_data['response']['tweets']   = $tweets;
						$save_data['response']['hashtags'] = $hashtags;
						$save_data['response']['urls']     = $tweet_urls;
						$save_data['response']['media']    = $tweet_media;

						if($this->saveTweets($save_data)){
							$return_data['status'] = 2;
							unset($save_data);
						} else {
							$return_data['status'] = 1;
							unset($save_data);
						}

						$save_data['users'] = $user;

						if($this->saveUser($save_data))
						{
							$return_data['status'] = 2;
						} else {
							$return_data['status'] = 1;
						}
					}
				}

				return $return_data;
			}
		}
		else
		{
			$data = array();
			$return_data = array();
			$hashtag = array();

			$user = $this->get_user($screen_name);
			$user_tweets = $this->get_user_tweets($screen_name, 'screen_name',5);
			for($i = 0; $i < count($user_tweets); $i++)
			{
				if($this->tweet_has_any_media($user_tweets[$i]['id'])){
					$user_tweets[$i]['media'] = $this->get_tweet_media($user_tweets[$i]['id']);
				}

				if($this->tweet_has_any_url($user_tweets[$i]['id'])){
					$user_tweets[$i]['url'] = $this->get_tweet_urls($user_tweets[$i]['id']);
				}

				if($this->tweet_has_any_hashtag($user_tweets[$i]['id'])){
					$user_tweets[$i]['hashtag'] = $this->get_tweet_hashtag($user_tweets[$i]['id']);
				}
			}
			$tweet_create_date = $this->get_user_tweet_datestamp($screen_name, 'screen_name');
			
			if(empty($user)){
				$return_data['status'] = 0;
				$return_data['response'] = array(
					'code'	=> '404',
					'message'	=> 'User Not Exist',
				);
			} else {
				$data = array(
					'user'	=> $user,
					'pure_result'	=> $user_tweets,
					'tweet_date'	=> $tweet_create_date
				);

				$return_data = array(
					'status'	=> 1,
					'response' => $data
				);
			}

			return $return_data;
		}
	}

	public function get_tweet_media_list($val, $param = 'hashtag', $limit = FALSE, $offset = FALSE)
	{
		$tweet_temp = array();
		$tweet_list = array();
		// echo "<pre>";
		// print_r ($limit);
		// echo "</pre>";
		// echo "<pre>";
		// print_r ($offset);
		// echo "</pre>";
		if($param == 'hashtag')
		{
			$select = array(
				't.id',
				't.text',
				't.created_at',
				'm.media_id',
				'm.media_url',
				'u.id as userid',
				'u.name',
				'u.screen_name',
				'u.profile_image_url'
			);

			$this->db->select($select);
			$this->db->from('tweets t');
			$this->db->join('tweet_media m', 't.id = m.tweet_id', 'inner');
			$this->db->join('tweet_url_attached tu', 't.id = tu.tweet_id', 'left');
			$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
			$this->db->join('tweet_hashtag h', 't.id = h.tweet_id', 'left');
			$this->db->where('h.hashtag', $val);
			$this->db->order_by('t.created_at', 'desc');
			$this->db->order_by('t.id', 'desc');

			if($limit !== FALSE && $offset !== FALSE){
				$this->db->limit(1000000000, $offset);
			}

			$user_tweets = $this->db->get()->result_array();
			// echo $this->db->last_query();
			
			$index = 0;
			for($i = $offset; $i < count($user_tweets); $i++)
			{
				if($index < $limit)
				{
					if(! in_array($user_tweets[$i]['id'], $tweet_temp))
					{
						$tweet_temp[$index] = $user_tweets[$i]['id'];
						$tweet_list[$index] = $user_tweets[$i];

						if($this->tweet_has_any_media($user_tweets[$i]['id'])){
							$tweet_list[$index]['media'] = $this->get_tweet_media($user_tweets[$i]['id']);
						}

						if($this->tweet_has_any_url($user_tweets[$i]['id'])){
							$tweet_list[$index]['url'] = $this->get_tweet_urls($user_tweets[$i]['id']);
						}

						if($this->tweet_has_any_hashtag($user_tweets[$i]['id'])){
							$tweet_list[$index]['hashtag'] = $this->get_tweet_hashtag($user_tweets[$i]['id']);
						}

						$index++;
					}
				}
			}
		}

		$return_data = array(
			'status'	=> (count($tweet_list) > 0) ? 1 : 0,
			'user_tweets'	=> (count($tweet_list) > 0) ? $tweet_list : array()
		);

		return $return_data;
	}

	public function count_all_tweet_media($val, $param = 'hashtag')
	{
		if($param == 'hashtag')
		{
			$select = array(
				't.id',
				't.text',
				't.created_at',
				'u.id as userid',
				'u.name',
				'u.screen_name',
				'u.profile_image_url',
			);

			$this->db->select($select);
			$this->db->from('tweets t');
			$this->db->join('tweet_media m', 't.id = m.tweet_id', 'inner');
			$this->db->join('tweet_url_attached tu', 't.id = tu.tweet_id', 'left');
			$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
			$this->db->join('tweet_hashtag h', 't.id = h.tweet_id', 'left');
			$this->db->where('h.hashtag', $val);
			$this->db->order_by('t.created_at', 'desc');
			$this->db->order_by('t.id', 'desc');

			return $this->db->count_all_results();
		}
	}
	public function get_hashtag_list()
	{

		$this->db->select('s.search_string as hashtag, count(hashtag) as hashtag_count');
		$this->db->from('tweet_hashtag h');
		$this->db->join('(SELECT DISTINCT search_string FROM saved_search) s', 'h.hashtag = s.search_string', 'right', FALSE);
		$this->db->group_by('LOWER(h.hashtag)');
		if($this->input->post('order')){
			$ordering = $this->input->post('order');
			switch ($ordering) {
				case 1:
					$this->db->order_by('hashtag', 'asc');
					break;
				case 2:
					$this->db->order_by('hashtag', 'desc');
					break;
				case 3:
					$this->db->order_by('count(hashtag)', 'asc');
					$this->db->order_by('hashtag', 'asc');
					break;
				case 4:
				default:
					$this->db->order_by('count(hashtag)', 'desc');
					$this->db->order_by('hashtag', 'asc');
					break;
			}
		} else {
			$this->db->order_by('count(hashtag)', 'desc');
		}

		if($this->input->post('keyword', TRUE)) {
			$this->db->like('hashtag', $this->input->post('keyword', TRUE), 'BOTH');
		}

		$result = $this->db->get()->result();

		return $result;
	}

	public function get_tweets_by_hashtag($hashtag, $limit = FALSE, $offset = FALSE)
	{
		if($offset === FALSE){
			$offset = 0;
		}
		if($limit === FALSE){
			$limit = 12;
		}
		$this->db->select('t.id as tweet_id, t.text, t.created_at as tweet_created, u.id as user_id, u.name, u.screen_name, u.location, u.description, u.followers_count, u.friends_count, u.statuses_count, u.profile_image_url, u.created_at as user_created');
		$this->db->from('tweets t');
		$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->order_by('t.created_at', 'desc');
		$this->db->where('h.hashtag', $hashtag);
		$this->db->limit($limit, $offset);

		$result = $this->db->get()->result();
		
		return $result;
	}

	public function get_users_by_hashtag($hashtag, $limit = FALSE, $offset = FALSE)
	{
		if($offset === FALSE){
			$offset = 0;
		}
		if($limit === FALSE){
			$limit = 12;
		}

		$select = array(
			'u.id',
			'u.name',
			'u.screen_name',
			'u.followers_count',
			'u.friends_count',
			'u.listed_count',
			'u.favorites_count',
			'u.statuses_count',
			'u.profile_image_url',
			'u.created_at',
			'u.status'
		);

		$this->db->select($select);
		$this->db->from('twitter_user u');
		$this->db->join('tweets t', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->where('h.hashtag', $hashtag);
		$this->db->group_by('u.id');
		$this->db->limit($limit, $offset);

		$result = $this->db->get()->result();
		
		return $result;
	}

	public function count_users_by_hashtag($hashtag){
		$select = array(
			'u.id',
			'u.name',
			'u.screen_name',
			'u.followers_count',
			'u.friends_count',
			'u.listed_count',
			'u.favorites_count',
			'u.statuses_count',
			'u.profile_image_url',
			'u.created_at',
			'u.status'
		);

		$this->db->select($select);
		$this->db->from('twitter_user u');
		$this->db->join('tweets t', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->where('h.hashtag', $hashtag);
		$this->db->group_by('u.id');
		$result = $this->db->get()->num_rows();
		return $result;
	}

	public function count_hashtag($hashtag)
	{
		$this->db->select('t.id, t.text, t.created_at as tweet_created, u.id, u.name, u.screen_name, u.location, u.description, u.followers_count, u.friends_count, u.statuses_count, u.profile_image_url, u.created_at as user_created');
		$this->db->from('tweets t');
		$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->where('h.hashtag', $hashtag);

		return $this->db->count_all_results();
	}

	public function get_media_by_hashtag($hashtag)
	{
		$this->db->select('t.id, t.text, t.created_at as tweet_created, u.id, u.name, u.screen_name, u.location, u.description, u.followers_count, u.friends_count, u.statuses_count, u.profile_image_url, u.created_at as user_created, m.media_url, m.media_type, CASE WHEN m.media_type LIKE "video%" THEN 1 WHEN m.media_type LIKE "%x-mpegURL" THEN 1 WHEN m.media_type LIKE "photo" THEN 0 ELSE 0 END as is_video', FALSE);
		$this->db->from('tweets t');
		$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');
		$this->db->join('tweet_hashtag h', 'h.tweet_id = t.id', 'left');
		$this->db->join('tweet_media m', 'm.tweet_id = t.id', 'inner');
		$this->db->where('h.hashtag', $hashtag);

		$result = $this->db->get()->result();

		return $result;
	}

	public function saveTweets($data)
	{
		$this->db->trans_start();
		
		if(array_key_exists('tweets', $data['response']))
		{
			foreach($data['response']['tweets'] as $tweet){
				if($this->is_user_exist($tweet['tweet_uid']) === FALSE){
					$index = $this->searchForId($tweet['tweet_uid'], $data['response']['users']);

					if($index !== FALSE){
						$this->db->insert('twitter_user', $data['response']['users'][$index]);
					}
				}
				if($this->tweet_is_exist($tweet['id']) === FALSE){
					$this->db->insert('tweets', $tweet);
					// echo "keinsert: $tweet[id]";
				}
			}
		}

		if(array_key_exists('hashtags', $data['response']))
		{
			foreach($data['response']['hashtags'] as $hashtag){
				if($this->tweet_has_hashtag($hashtag['tweet_id'],$hashtag['hashtag']) == FALSE){
					$this->db->insert('tweet_hashtag', $hashtag);
				}
			}
		}
		
		if(array_key_exists('urls', $data['response']))
		{
			foreach($data['response']['urls'] as $url){
				if($this->tweet_has_url($url['tweet_id'],$url['url']) == FALSE){
					$this->db->insert('tweet_url_attached', $url);
				}
			}
		}
		
		if(array_key_exists('media', $data['response']))
		{
			foreach($data['response']['media'] as $media){
				if($this->tweet_has_media($media['tweet_id'], $media['media_id']) == FALSE){
					$this->db->insert('tweet_media', $media);
				}
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE){
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function tweet_has_any_media($tweet_id)
	{
		$this->db->where('tweet_id', $tweet_id);
		$count = $this->db->count_all_results('tweet_media');
		
		if($count > 0){
			return TRUE;
		}

		return FALSE;
	}
	public function tweet_has_any_hashtag($tweet_id)
	{
		$this->db->where('tweet_id', $tweet_id);
		$count = $this->db->count_all_results('tweet_hashtag');
		
		if($count > 0){
			return TRUE;
		}

		return FALSE;
	}
	public function tweet_has_any_url($tweet_id)
	{
		$this->db->where('tweet_id', $tweet_id);
		$count = $this->db->count_all_results('tweet_url_attached');
		
		if($count > 0){
			return TRUE;
		}

		return FALSE;
	}
	public function get_user($val, $param = 'screen_name'){
		$this->db->where($param,$val);
		$result = $this->db->get('twitter_user')->row_array();

		return $result;
	}

	public function get_user_tweets($val, $param = 'tweet_uid', $limit)
	{
		if($param == 'tweet_uid')
		{	
			$this->db->select('id, text, created_at');
			$this->db->where('tweet_uid', $val);
			$this->db->order_by('created_at', 'desc');
			$this->db->limit($limit);
			$result = $this->db->get('tweets')->result_array();
		}
		else if($param == 'screen_name')
		{
			$this->db->select('t.id, t.text, t.created_at');
			$this->db->from('tweets t');
			$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');

			$this->db->where('u.screen_name', $val);
			$this->db->order_by('t.created_at', 'desc');
			$this->db->limit($limit);
			$result = $this->db->get()->result_array();

			return $result;
		}
		

		return $result;
	}

	public function get_tweet_urls($tweet_id)
	{
		$result = array();

		$this->db->select('id, url');
		$this->db->where('tweet_id', $tweet_id);
		$urls = $this->db->get('tweet_url_attached')->result_array();

		foreach($urls as $url)
		{
			$result[] = $url['url'];
		}

		return $result;
	}

	public function get_tweet_media($tweet_id)
	{
		$this->db->select('id, media_id, media_type, media_url');
		$this->db->where('tweet_id', $tweet_id);
		$result = $this->db->get('tweet_media')->result_array();

		return $result;
	}

	public function get_tweet_hashtag($tweet_id)
	{
		$result = array();

		$this->db->select('id, hashtag');
		$this->db->where('tweet_id', $tweet_id);
		$hashtags = $this->db->get('tweet_hashtag')->result_array();

		foreach($hashtags as $hashtag)
		{
			$result[] = $hashtag['hashtag'];
		}

		return $result;
	}

	public function get_user_tweet_datestamp($val, $param = 'tweet_uid')
	{
		$tweet_datestamp = array();
		if($param == 'tweet_uid')
		{
			$this->db->select('DATE_FORMAT(created_at, "%Y-%m-%d") as created_at');
			$this->db->where('tweet_uid', $val);
			$this->db->group_by('DATE_FORMAT(created_at, "%Y-%m-%d")');
			$this->db->order_by('created_at', 'desc');
			$datestamp = $this->db->get('tweets')->result_array();
		}
		else if($param == 'screen_name')
		{
			$this->db->select('DATE_FORMAT(t.created_at, "%Y-%m-%d") as created_at');
			$this->db->from('tweets t');
			$this->db->join('twitter_user u', 't.tweet_uid = u.id', 'left');

			$this->db->where('u.screen_name', $val);
			$this->db->group_by('DATE_FORMAT(t.created_at, "%Y-%m-%d")');
			$this->db->order_by('t.created_at', 'desc');
			$datestamp = $this->db->get()->result_array();
		}

		foreach($datestamp as $date){
			$tweet_datestamp[] = $date['created_at'];
		}
		

		return $tweet_datestamp;
	}

	public function saveUser($data)
	{
		$this->db->trans_start();
		
		if(array_key_exists('response', $data)) {
			foreach($data['response']['users'] as $user){
				if($this->is_user_exist($user['id']) == FALSE){
					
					$this->db->insert('twitter_user', $user);
				}
			}
		} else {
			if(array_key_exists('users', $data))
			{
				if(is_array_multi($data['users'])) {
					foreach($data['users'] as $user){
						if($this->is_user_exist($user['id']) == FALSE){
							
							$this->db->insert('twitter_user', $user);
						}
					}
				} else {
					if($this->is_user_exist($data['users']['id']) == FALSE){
						$this->db->insert('twitter_user', $data['users']);
					} else {
						$this->db->where('id', $data['users']['id']);
						$this->db->update('twitter_user', $data['users']);
					}
				}
			}
		}

		$this->db->trans_complete();

		if($this->db->trans_status() === FALSE){
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function is_user_exist($id) // check if user with id $id is exist
	{
		$this->db->where('id', $id);
		$this->db->from('twitter_user');

		if($this->db->count_all_results() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function searchForId($id, $array) {
	   foreach ($array as $key => $val) {
	       if ($val['id'] === $id) {
	           return $key;
	       }
	   }
	   return FALSE;
	}

	public function tweet_has_hashtag($id, $hashtag)
	{
		$this->db->where('tweet_id', $id);
		$this->db->where('hashtag', $hashtag);
		$this->db->from('tweet_hashtag');
		if($this->db->count_all_results() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function tweet_has_media($tweet_id, $media_id)
	{
		$this->db->where('media_id', $media_id);
		$this->db->where('tweet_id', $tweet_id);
		$this->db->from('tweet_media');
		if($this->db->count_all_results() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function tweet_has_url($id, $url)
	{
		$this->db->where('tweet_id', $id);
		$this->db->where('url', $url);
		$this->db->from('tweet_url_attached');
		if($this->db->count_all_results() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function tweet_is_exist($id)
	{
		$this->db->where('id', $id);
		$this->db->from('tweets');
		if($this->db->count_all_results() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function save_search_param($search_string)
	{
		$data = array(
			'search_string' => $search_string,
			'max_id'        => $this->input->post('max_id', TRUE),
			'until'         => $this->input->post('until', TRUE),
			'search_type'   => $this->input->post('search_type', TRUE),
			'result_type'   => $this->input->post('result_type', TRUE)
		);

		if($this->query_is_exist($data)){
			$this->db->where('search_string', $data['search_string']);
			$this->db->where('until', $data['until']);
			$this->db->where('search_type', $data['search_type']);
			$this->db->where('result_type', $data['result_type']);

			$this->db->set('max_id', $data['max_id']);
			if($this->db->update('saved_search')){
				// echo $this->db->last_query();
				return array(
					'status'	=> 1,
					'max_id'	=> $data['max_id']
				);
			} else {
				return array(
					'status'	=> 0
				);
			}
		} else {
			if($this->db->insert('saved_search', $data)){
				// echo $this->db->last_query();
				return array(
					'status'	=> 1,
					'max_id'	=> $data['max_id']
				);
			} else {
				return array(
					'status'	=> 0
				);
			}
		}
	}

	public function get_search_param()
	{
		$this->db->select('max_id');
		$this->db->where('search_string', $this->input->post('search_string'));
		$this->db->where('until', $this->input->post('until'));
		$this->db->where('search_type', $this->input->post('search_type'));
		$this->db->where('result_type', $this->input->post('result_type'));

		$result = $this->db->get('saved_search');
		// echo $this->db->last_query();

		if($result->num_rows() > 0){
			return $result->row()->max_id;
		}

		return null;
	}

	public function query_is_exist($param)
	{
		$this->db->where('search_string', $param['search_string']);
		$this->db->where('until', $param['until']);
		$this->db->where('search_type', $param['search_type']);
		$this->db->where('result_type', $param['result_type']);
		$this->db->from('saved_search');

		$result = $this->db->count_all_results();

		if($result > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file Twitter_model.php */
/* Location: ./application/models/Twitter_stream.php */