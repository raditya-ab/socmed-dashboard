<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=@$title;?> | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3 -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css');?>">
  <!-- FontAwesome -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/font-awesome/css/font-awesome.min.css');?>">
  <!-- IonIcons -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/Ionicons/css/ionicons.min.css');?>">
  <!-- SweetAlert 2 -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/sweetalert2/sweetalert2.min.css');?>">
  <!-- Animate -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/animate/animate.css');?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/minimal/red.css');?>">
  <!-- Bootstrap Datepicker -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap-daterangepicker/daterangepicker.css');?>">
  <!-- AdminLTE Themes -->
  <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">
  <link rel="stylesheet" href="<?=base_url('assets/custom/css/style.css');?>">

  <?php if(@$use_datatable): ?>
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/DataTables/datatables.min.css');?>"/>
  <?php endif; ?>

  <!-- <script type="text/javascript" src="<?=base_url('assets/bower_components/tinymce/js/tinymce/tinymce.js');?>"></script> -->

  <?php if(@$page_css): ?>
    <?php foreach($page_css as $link): ?>
      <link rel="stylesheet" href="<?=$link;?>">
    <?php endforeach ?>
  <?php endif; ?>

  <?php if(@$header_script): ?>
    <?php foreach($header_script as $script): ?>
      <script src="<?=$script;?>" type="text/javascript"></script>
    <?php endforeach ?>
  <?php endif; ?>

  <!-- <script>
    tinymce.init({
      selector: '#articleEditor',
      height: 500,
      theme: 'modern',
      plugins: [
          "advlist autolink lists link image charmap print preview anchor",
          "searchreplace visualblocks code fullscreen",
          "insertdatetime media table contextmenu paste imagetools"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css'
        ]
    });
  </script> -->

  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red-light sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">

  <?=@$navbar;?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?=@$sidebar;?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=@$title;?>
        <small><?=@$subtitle;?></small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

      <?=@$body_content;?>
      
      <div class="modal fade" id="loading-modal">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-body text-center">
              <i class="fa fa-spin fa-refresh fa-3x"></i>
              <h1>Please Wait..</h1>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 <a href="https://www.narrada.com">Narrada Communications</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- JQuery -->
<script type="text/javascript" src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js');?>"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="<?=base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- Slimscroll -->
<script type="text/javascript" src="<?=base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll');?>.min.js"></script>
<!-- Fastclick -->
<script type="text/javascript" src="<?=base_url('assets/bower_components/fastclick/lib/fastclick.js');?>"></script>
<!-- SweetAlert 2 -->
<script src="<?=base_url('assets/bower_components/sweetalert2/sweetalert2.min.js');?>" type="text/javascript"></script>
<!-- iCheck -->
<script type="text/javascript src="<?=base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>

<!-- AdminLTE -->
<script type="text/javascript" src="<?=base_url('assets/dist/js/adminlte.min.js');?>"></script>

<?php if(@$use_datatable): ?>
  <!-- DataTables -->
  <script type="text/javascript" src="<?=base_url('assets/plugins/DataTables/datatables.min.js');?>"></script>
<?php endif; ?>
<script type="text/javascript" src="<?=base_url('assets/custom/js/global.js');?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/momentjs/moment.js');?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bower_components/bootstrap-daterangepicker/daterangepicker.js');?>"></script>

<?php if(@$page_js):?>
  <?php foreach($page_js as $link): ?>
    <script src="<?=$link;?>"></script>
  <?php endforeach; ?>
<?php endif; ?>


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
