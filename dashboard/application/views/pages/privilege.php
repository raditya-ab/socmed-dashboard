
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Privilege Management</h3>&nbsp;
    <div class="btn-group">
      <button type="button" class="btn btn-danger btn-sm" data-toggle="dropdown">Add New</button>
      <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a data-toggle="modal" href='#modal_add_group'>Privilege Group</a></li>
        <li><a data-toggle="modal" href='#modal_add_privilege'>Access Permission</a></li>
      </ul>
    </div>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body">
    <table class="table table-hover" id="tbl_priv_list" width="100%">
      <thead>
        <tr>
          <th>No.</th>
          <th>Privilege Name</th>
          <th>Allowed Group</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->

<div class="modal fade" id="modal_add_group">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add New Group</h4>
      </div>
      <form action="<?=base_url('privilege/add/group');?>" id="priv_add_group">
        <div class="modal-body">
          <div class="form-group">
            <label for="">Group Name</label>
            <input type="text" class="form-control" name="group_name">
          </div>
          <div class="form-group">
            <label for="">Permission</label>
            <div class="well well-sm">
              <?php foreach($permission_list as $permission): ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="permission_name[]" value="<?=$permission->permission_name;?>">
                    <?=$permission->permission_name;?>
                  </label>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_add_privilege">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add New Privilege</h4>
      </div>
      <form action="<?=base_url('privilege/add/permission');?>" id="priv_add_permission">
        <div class="modal-body">
          <div class="form-group">
            <label for="">Privilege Name</label>
            <input type="text" class="form-control" name="permission_name">
          </div>
          <div class="form-group">
            <label for="">Assign To</label>
            <div class="well well-sm">
              <?php foreach($group_list as $group): ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="group_name[]" value="<?=$group->group_name;?>">
                    <?=ucwords($group->group_name);?>
                  </label>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_edit_privilege">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">List Group Allowed</h4>
      </div>
      <form action="<?=base_url('privilege/edit');?>" id="priv_edit">
        <div class="modal-body">
          <input type="hidden" name="permission_id" value="">
          <table class="table tbl-striped" width="100%" id="priv_list">
            <thead>
              <tr>
                <th>User Group</th>
                <th>Allow</th>
                <th>Deny</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($group_list as $group): ?>
                <tr>
                  <td><?=ucwords($group->group_name);?></td>
                  <td class="text-center">
                    <input type="radio" name="rbo_<?=$group->group_name;?>" value="1" >
                  </td>
                  <td class="text-center">
                    <input type="radio" name="rbo_<?=$group->group_name;?>" value="0">
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>