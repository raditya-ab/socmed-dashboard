<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Narrada Dashboard| Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url('assets');?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets');?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url('assets');?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets');?>/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url('assets');?>/plugins/iCheck/square/blue.css">
  <!-- SweetAlert 2 -->
  <link rel="stylesheet" href="<?=base_url('assets');?>/bower_components/sweetalert2/sweetalert2.min.css"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="<?=site_url();?>">
      <img src="<?=base_url('assets/dist/img/logo-nrd.png');?>" alt="Narrada">
    </a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="<?=site_url('register');?>" method="post" id="formRegister">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Retype password" required>
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <select name="gender" id="gender" class="form-control" required>
          <option value="">Select</option>
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="terms" id="terms" required> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit" id="btn-register" disabled>Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="<?=site_url('login');?>" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?=base_url('assets');?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url('assets');?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url('assets');?>/plugins/iCheck/icheck.min.js"></script>
<!-- SweetAlert 2 -->
<script src="<?=base_url('assets');?>/bower_components/sweetalert2/sweetalert2.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('input[type=checkbox]').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

    $("#terms").on("ifChecked ifUnchecked", function(e){
      if(e.type == 'ifChecked'){
        $("#btn-register").removeAttr('disabled');
      } else {
        $("#btn-register").prop('disabled', true);
      }
    });

    $("#formRegister").on("submit", function(e){
      e.preventDefault();

      $("#btn-register").html('<i class="fa fa-reload fa-spin"></i>');

      var formData = $(this).serialize();
      var formURL = $(this).attr("action");
      var formMethod = $(this).attr("method");

      $.ajax({
        url: formURL,
        type: formMethod,
        dataType: 'json',
        data: formData,
      })
      .done(function(res) {
        var redirectURL = window.location.protocol + '//' + window.location.host + '/' + window.location.pathname.split('/')[1];
        
        if(res.status){
          swal(
            "Success",
            res.response,
            "success"
          )
          .then(function(){
            window.location.replace(redirectURL);
          }, function(dismiss){
            window.location.replace(redirectURL);
          });
        } else {
          swal(
            "Error",
            res.response,
            "error"
          );
        }
      })
      .fail(function(thrownError) {
        console.log(thrownError);
      });

      $("#btn-register").html('Register');
    })
  });
</script>
</body>
</html>
