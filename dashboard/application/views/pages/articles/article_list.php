<?php 
	$privilege = $this->session->userdata('loggedIn')['privilege'];
?>
<div class="box">
	<div class="box-header with-border">
		<div class="row">
			<div class="col-md-9">
				<form id="filter" method="post">
					<div class="row">
						<div class="col-md-2">
							<select name="date_filter" id="date_filter" class="form-control">
							  <option value="">All dates</option>
							  <?php foreach($article_date as $date): ?>
							  	<option value="<?=date("Y-m", strtotime($date->date_created));?>"><?=date("M Y", strtotime($date->date_created));?></option>
							  <?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-4">
							<select name="category_filter" id="category_filter" class="form-control">
							  <option value="">All categories</option>
							  <?php foreach($article_category as $category): ?>
							  	<option value="<?=$category->id;?>"><?=$category->name;?></option>
							  <?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-3">
							<select name="author_filter" id="author_filter" class="form-control">
							  <option value="">All Author</option>
							  <?php foreach($article_author as $author): ?>
							  	<option value="<?=$author->author;?>"><?=ucwords($author->first_name);?></option>
							  <?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-block btn-warning" id="btn-filter">Filter</button>
						</div>
					</div>
				</form>
			</div>
			<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
				<div class="col-md-3">
					<div class="row">
						<div class="col-md-12 text-right">
							<a href="<?=site_url('article/add');?>" class="btn btn-danger" id="btn-addPost">Add New</a>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="box-body">
		<table class="table tbl-striped" id="tbl_article" width="100%">
			<thead>
				<tr>
					<th><input type="checkbox" id="chbAll"></th>
					<th width="30%" style="text-align: left !important">Title</th>
					<th width="15%">Author</th>
					<th width="20%">Categories</th>
					<th width="5%"><i class="fa fa-comments"></i></th>
					<th >Date</th>
					<th width="10%">Status</th>
					<th width="10%">Action</th>
				</tr>
			</thead>
			<tbody>
				<!-- <tr>
					<td><input type="checkbox" id="chbAll"></td>
					<td>Lorem ipsum dolor sit.</td>
					<td>Lorem.</td>
					<td>Lorem.,Lorem.</td>
					<td>10</td>
					<td>
						27-01-2017
					</td>
					<td>Published</td>
				</tr> -->
			</tbody>
		</table>
	</div>
	<div class="box-footer with-border">
		<div class="row">
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-8">
						<select name="bulk_action" id="bulk_action" class="form-control" disabled>
						  	<option value="">Bulk Action</option>
						  	<option value="publish">Publish</option>
						  	<option value="unpublish">Save to Draft</option>
						  	<?php if ( stripos($privilege,"Delete") !== FALSE): ?>
							  	<option value="delete">Delete</option>
							<?php endif; ?>
						</select>
					</div>
					<div class="col-md-4">
						<button type="button" class="btn btn-default" id="btn-apply-bulk-action" disabled>Apply</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>