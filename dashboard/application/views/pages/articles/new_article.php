<div class="row">
	<form action="<?=base_url('article/post_new');?>" id="form_post_article" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="status" value="">
		<input type="hidden" name="featured_image" value="">
		<div class="col-md-9">
			<div class="box">
				<div class="box-header">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="">Title</label>
								<input type="text" class="form-control" id="title" name="title" placeholder="Enter title here" style="font-size: 24px" required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="">Subtitle</label>
								<input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Enter subtitle here" required>
							</div>
						</div>
					</div>
				</div>
				<div class="box-body">
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active" style="text-align: right">
								<a href="#editor" aria-controls="editor" role="tab" data-toggle="tab">Editor</a>
							</li>
							<li role="presentation">
								<a href="#view" aria-controls="view" role="tab" data-toggle="tab">View</a>
							</li>
						</ul>
					
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="editor">
								<div class="row">
									<div class="col-md-12">
										<textarea name="content" id="articleEditor" cols="30" rows="30" required></textarea>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="view">
								<div class="row" style="margin-top: 20px !important">
									<div class="col-md-12">
										<div id="articlePreview"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<span><strong>Publish</strong></span>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							    	<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<div class="pull-left">
								<button type="button" class="btn btn-sm btn-default" id="btn-saveDraft">Save as Draft</button>
							</div>
							<div class="pull-right">
								<button type="button" class="btn btn-sm btn-primary" id="btn-savePublish">Publish</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<span><strong>Categories</strong></span>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							    	<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<div class="well well-sm" id="post_category">
										<?php foreach($category_list as $category): ?>
											<div class="checkbox">
												<label>
													<input type="checkbox" name="category_id[]" value="<?=$category->id;?>">
													<?=$category->name;?>
												</label>
											</div>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="text" name="category_name" class="form-control" style="display: none">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pull-left">
										<button type="button" class="btn btn-default btn-sm" id="btn-cancelAdd" style="display: none">Cancel</button>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group pull-right">
										<button type="button" class="btn btn-default btn-sm" id="btn-toggleAddCategory">Add New</button>
										<button type="button" class="btn btn-default btn-sm" id="btn-addCategory" style="display: none">Add New</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<span><strong>More Options</strong></span>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
							    	<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Slug</label>
										<input type="text" class="form-control" id="slug" name="slug" placeholder="Article slug" style="font-style: italic" required>
									</div>
									<div class="form-group">
										<label for="">Set as featured</label>
										<select name="featured_articles" class="form-control" required>
											<option value="0">No</option>
											<option value="1">Yes</option>
										</select>
									</div>
									<div class="form-group">
										<label for="">Featured Image</label><br>
										<div id="featured_image"></div>
										<br>
										<div class="pull-left">
											<a class="btn btn-default btn-sm" data-toggle="modal" href='#show-gallery'>Choose Image</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="modal fade" id="show-gallery">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Image Gallery</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					
				</div>
			</div>
			<div class="modal-footer">
				<div class="pull-left">
					<label for="uploaded_file" class="btn btn-danger btn-sm">Upload</label>
					<input type="file" class="btn btn-danger" id="uploaded_file" name="uploaded_file" style="display: none">
				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="btn-select_img">Select</button>
			</div>
		</div>
	</div>
</div>
