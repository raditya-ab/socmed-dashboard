<div class="box">
	<form action="<?=site_url('twitter_stream/grab_tweet');?>" id="tweet_search" class="form">
		<input type="hidden" name="max_id">
		<input type="hidden" name="param" value="">
		<div class="box-body">
			<div class="col-md-12">
				<div class="row">
					<div class="form">
						<div class="col-md-4">
							<div class="form-group">
								<label for="" class="control-label">
									<span class="text-red"> * </span>Search string
								</label>
								<input type="text" name="search_string" class="form-control" placeholder="Search.. (without hashtag)">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="" class="control-label clearfix">Result type</label>
								<div class="radio" style="margin-top: -5px">
									<label>
										<input type="radio" name="search_type" value="hashtag">
										Hashtag
									</label><br>
									<label>
										<input type="radio" name="search_type" value="screen_name">
										Screen name
									</label><br>
								</div>
							</div>
						</div>
						<div id="keyword-field" style="display: none">
							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label clearfix">Result type</label>
									<div class="radio" style="margin-top: -5px">
										<label>
											<input type="radio" name="result_type" value="mixed" disabled>
											Mixed
										</label><br>
										<label>
											<input type="radio" name="result_type" value="popular" disabled>
											Popular
										</label><br>
										<label>
											<input type="radio" name="result_type" value="recent" checked disabled>
											Recent
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group" id="until_date_field">
									<label for="" class="control-label">Date Limit</label>
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
										<input type="text" class="form-control datepicker" id="until" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly disabled>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group">
									<div class="checkbox">
										<label>
											<input type="checkbox" value="1" id="ignore_date" disabled>
											Ignore Date
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label">Result Count</label>
									<input type="number" class="form-control" id="count" name="count" placeholder="21" value="21" min="0" disabled>
								</div>
							</div>
						</div>
						<div id="user-field" style="display: none">
							<div class="col-md-2">
								<div class="form-group">
									<label for="" class="control-label">Result Count</label>
									<input type="number" class="form-control" id="datepicker" name="count" placeholder="20" value="20" min="0" max="100" disabled>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							<button class="btn btn-danger btn-block" id="btn-submit" type="button" disabled>
								Search
							</button>
						</div>
					</div>
					<div class="col-md-2">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="save_result" value="1" checked>
								Save Result
							</label>
						</div>
					</div>
					<div class="col-md-8" id="hashtag-container" style="display: none">
						<label for="">Hashtag retrieved</label><br>
						<div id="hashtag-retrieved">
							
						</div>
						<!-- <a href="" class="btn btn-danger btn-xs">#AniesSandi</a>
						<a href="" class="btn btn-danger btn-xs">#Gubernur</a>
						<a href="" class="btn btn-danger btn-xs">#Jakarta2017</a>
						<a href="" class="btn btn-danger btn-xs">#5th</a> -->
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="row" id="search_result" style="margin-bottom: 50px">
	
</div>
<div id="next_result" style="display: none">
	<center><h2>No more result</h2></center>
</div>
<div id="load_more_button" style="display: none">
	<center><button class="btn btn-danger" id="btn-load-more">Scroll or click to load more..</button></center>
</div>