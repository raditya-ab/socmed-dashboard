<?php $privilege = $this->session->userdata('loggedIn')['privilege']; ?>

<input type="hidden" id="tweet_count" value="<?=$tweet_count;?>">
<input type="hidden" id="media_count" value="<?=$media_count;?>">
<input type="hidden" id="contributor_count" value="<?=$contributor_count;?>">

<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-pills" role="tablist">
		<li role="presentation">
			<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag/tweet");?>">Tweets (<span id="tweet_count"><?=$tweet_count;?></span>)</a>
		</li>
		<li>
			<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag/media");?>">Tweet Media (<span id="media_count"><?=$media_count;?></span>)</a>
		</li>
		<li  class="active">
			<a href="#list_contributor" aria-controls="List Tweet" role="tab" data-toggle="tab">Tweet Contributors (<span id="contributor_count"><?=$contributor_count;?></span>)</a>
		</li>
		<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
			<li style="float: right">
				<div class="btn-group">
					<button class="btn btn-danger grab-more-tweet">Grab more</button>
					<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class=><i class="fa fa-cog"></i></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu keep-open" id="form-top" role="menu" style="padding: 10px;">
						<form id="grab_tweet-top">
							<input type="hidden" name="max_id">
							<input type="hidden" name="search_string" value="<?=$hashtag;?>" readonly>
							<input type="hidden" name="search_type" value="hashtag" readonly>
							<input type="hidden" name="param" value="hashtag" readonly>
							<input type="hidden" name="save_result" value="1" readonly>
							<li>
								<div class="form-group">
									<label for="" class="control-label clearfix">Result type</label>
									<div class="radio" style="margin-top: -5px">
										<label>
											<input type="radio" name="result_type" value="mixed">
											Mixed
										</label><br>
										<label>
											<input type="radio" name="result_type" value="popular">
											Popular
										</label><br>
										<label>
											<input type="radio" name="result_type" value="recent" checked>
											Recent
										</label>
									</div>
								</div>
							</li>
							<li>
								<label for="" class="control-label">Date Limit</label>
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
									<input type="text" class="form-control datepicker" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly>
								</div>
							</li>
							<li>
								<div class="form-group">
									<label for="" class="control-label">Result Count</label>
									<input type="number" class="form-control" name="count" placeholder="21" value="21" min="0">
								</div>
							</li>
							<li>
								<button class="btn btn-danger grab-more-tweet-form">Grab more</button>
							</li>
						</form>
					</ul>
				</div>
			</li>
		<?php endif; ?>
	</ul>
	
	<!-- Tab panes -->
	<div class="tab-content" style="margin-top: 20px">
		<div role="tabpanel" class="tab-pane active" id="list_tweet">
			<?php $contributor_index = 1; ?>
			<?php foreach($contributor_list as $index_contributor => $contributor): ?>
				<?php if($index_contributor % 3 == 0): ?><div class="row" id="contributor_list"><?php endif; ?>
					<div class="col-md-4">
						<div class="box box-widget widget-user" data-id="<?=$contributor->id;?>">
							<div class="widget-user-header bg-aqua-active">
								<h3 class="widget-user-username clearfix"><span class="pull-left" style="font-size: 16px; font-weight: 600"><?=$contributor->name;?></span><span class="pull-right" style="font-size: 14px"><a href="https://twitter.com/<?=$contributor->screen_name;?>" target="_blank" style="color: #FFF">@<?=$contributor->screen_name;?></a></span></h3>
							</div>
							<div class="widget-user-image">
								<img class="img-circle" src="<?=$contributor->profile_image_url;?>" alt="User Avatar">
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-4 border-right">
										<div class="description-block">
											<h5 class="description-header"><?=number_format($contributor->statuses_count);?></h5>
											<span class="description-text">TWEETS</span>
										</div>
									</div>
									<div class="col-sm-4 border-right">
										<div class="description-block">
											<h5 class="description-header"><?=number_format($contributor->followers_count);?></h5>
											<span class="description-text">FOLLOWERS</span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="description-block">
											<h5 class="description-header"><?=number_format($contributor->friends_count);?></h5>
											<span class="description-text">FOLLOWING</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<a class="btn bg-aqua btn-block" href="<?=site_url("twitter_stream/user_detail/$contributor->screen_name");?>" target="_blank">View User</a>
									</div>
									<div class="col-md-6">
										<a class="btn bg-aqua btn-block" href="https://twitter.com/<?=$contributor->screen_name;?>" target="_blank">View On Twitter <i class="fa fa-twitter"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php if($contributor_index % 3 == 0 && $contributor_index != 1): ?></div><?php endif;?>
				<?php $contributor_index++; ?>
			<?php endforeach; ?>
			<div class="row">
				<div class="col-md-12">
					<div class="pagination_links">
	                    <?php echo $links; ?>
	                </div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
<div class="btn-page-nav" style="position: fixed; bottom: 40px; right: 20px">
	<div class="btn-group">
		<button class="btn btn-danger grab-more-tweet">Grab more</button>
		<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<span class=><i class="fa fa-cog"></i></span>
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" id="form-bottom" role="menu" style="padding: 10px;">
			<form id="grab_tweet-bottom">
				<input type="hidden" name="max_id">
				<input type="hidden" name="search_string" value="<?=$hashtag;?>" readonly>
				<input type="hidden" name="search_type" value="hashtag" readonly>
				<input type="hidden" name="param" value="hashtag" readonly>
				<input type="hidden" name="save_result" value="true" readonly>
				<li>
					<div class="form-group">
						<label for="" class="control-label clearfix">Result type</label>
						<div class="radio" style="margin-top: -5px">
							<label>
								<input type="radio" name="result_type" value="mixed">
								Mixed
							</label><br>
							<label>
								<input type="radio" name="result_type" value="popular">
								Popular
							</label><br>
							<label>
								<input type="radio" name="result_type" value="recent" checked>
								Recent
							</label>
						</div>
					</div>
				</li>
				<li>
					<label for="" class="control-label">Date Limit</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" class="form-control datepicker" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly>
					</div>
				</li>
				<li>
					<div class="form-group">
						<label for="" class="control-label">Result Count</label>
						<input type="number" class="form-control" name="count" placeholder="21" value="21" min="0">
					</div>
				</li>
				<li>
					<button class="btn btn-danger grab-more-tweet-form">Grab more</button>
				</li>
			</form>
		</ul>
	</div>
	<button class="btn btn-default" ><i class="fa fa-chevron-up"></i></button>
	
</div>
<?php endif; ?>


