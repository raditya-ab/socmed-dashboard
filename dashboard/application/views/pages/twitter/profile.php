<?php 
  $privilege = $this->session->userdata('loggedIn')['privilege'];
  if(! empty($user_timeline)){
    $tweets = $user_timeline[0]; 
  }
?>
<input type="hidden" name="screen_name" value="<?=$user_detail['screen_name'];?>">
<div class="row">
  <div class="col-md-3">

    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="<?=$user_detail['profile_image_url'];?>" alt="User profile picture">

        <h3 class="profile-username text-center"><?=$user_detail['name'];?></h3>

        <p class="profile-screenname text-muted text-center">@<?=$user_detail['screen_name'];?></p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Followers</b> <a class="profile-followers-count pull-right"><?=number_format($user_detail['followers_count']);?></a>
          </li>
          <li class="list-group-item">
            <b>Following</b> <a class="profile-friends-count pull-right"><?=number_format($user_detail['friends_count']);?></a>
          </li>
          <li class="list-group-item">
            <b>Favorites</b> <a class="profile-favorites-count pull-right"><?=number_format($user_detail['favorites_count']);?></a>
          </li>
          <li class="list-group-item">
            <b>List</b> <a class="profile-listed-count pull-right"><?=number_format($user_detail['listed_count']);?></a>
          </li>
          <li class="list-group-item">
            <b>Total Tweets</b> <a class="profile-statuses-count  pull-right"><?=number_format($user_detail['statuses_count']);?></a>
          </li>
          <li class="list-group-item">
            <b>Last Tweet</b> <a class="profile-last-tweet pull-right"><?=date('M j, Y H:i:s', strtotime($user_detail['last_tweet_date']));?></a>
          </li>
        </ul>

        <a href="https://twitter.com/<?=$user_detail['screen_name'];?>" target="_blank" class="btn bg-aqua btn-block">View on Twitter <i class="fa fa-twitter"></i></a>
        <?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
          <button id="btn-update-last" class="btn btn-danger btn-block" type="button">Update Latest Info</button>
        <?php endif; ?>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- About Me Box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">About Me</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i> Description</strong>

        <p class="profile-description text-muted">
          <?=$user_detail['description'];?>
        </p>

        <hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

        <p class="profile-location text-muted">
          <?=$user_detail['location'];?>
        </p>

        <hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Date Created</strong>

        <p class="profile-date-created text-muted"><?=$user_detail['created_at'];?></p>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#activity" data-toggle="tab">Last Activity</a></li>
        <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
        <!-- <li><a href="#settings" data-toggle="tab">Settings</a></li> -->
      </ul>
      <div class="tab-content">
        <div class="active tab-pane" id="activity">
          <?php if(! empty($user_timeline)): ?>
            <!-- Post -->
            <div class="post">
              <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?=$user_detail['profile_image_url'];?>" alt="user image" id="latest_post-user-img">
                <span class="username">
                  <a class="profile-username" href="https://twitter.com/<?=$user_detail['screen_name'];?>"><?=$user_detail['name'];?></a>
                  <span class="pull-right text-right">
                    <small id='latest_post-date'>
                      <?=date('l M j, Y', strtotime($tweets['created_at']));?>
                    </small><br>
                    <small id='latest_post-time'>
                      <?=date('H:i:s', strtotime($tweets['created_at']));?>
                    </small><br>
                    <small id='latest_post-timestring'>
                      <?=time_elapsed_string($tweets['created_at']);?>
                    </small>
                  </span>
                </span>
                <span class="description">
                  <a class="profile-screenname" href="https://twitter.com/<?=$user_detail['screen_name'];?>" target="_blank">@<?=$user_detail['screen_name'];?></a>
                </span>
              </div>
              <!-- /.user-block -->
              <div class="row">
                <div class="col-md-12 tweet-content" id="latest_tweet-text">
                  <p><?=$tweets['text'];?></p><br>
                </div>
              </div>
              <?php if(array_key_exists('media', $tweets)):?>
                <div class="row" id="latest_tweet-media">
                  <?php foreach($tweets['media'] as $media): ?>
                    <?php if(count($tweets['media']) == 1): ?>
                        <div class="col-md-12">
                      <?php elseif(count($tweets['media']) == 2): ?>
                        <div class="col-md-6">
                      <?php else: ?>
                          <div class="col-md-4">
                    <?php endif; ?>
                    <?php if($media['media_type'] == 'photo'): ?>
                      <a href="<?=$media['media_url'];?>" target="_blank"><img src="<?=$media['media_url'];?>" class="img-responsive"></a>
                    <?php else: ?>
                      <a href="<?=$media['media_url'];?>" target="_blank">
                        <video width="100%" controls>
                          <source src="<?=$media['media_url'];?>" type="<?=$media['media_type'];?>">
                          Your browser doesn't support HTML5 video
                        </video>
                      </a>
                    <?php endif; ?>
                    </div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
              <?php if(array_key_exists('url', $tweets)): ?>
                <div class="row" id="latest_tweet-url">
                  <?php foreach($tweets['url'] as $url): ?>
                    <div class="col-md-12">
                      <div class="fr-embedly " data-original-embed="<a href='<?=$url;?>' class='embedly-card'></a>">
                        <a href="<?=$url;?>" class="embedly-card"></a>
                      </div>

                    </div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
            </div>
            <!-- /.post -->
            <?php else: ?>
              <div class="row">
                <div class="col-md-12">
                  <center>
                    <h4>@<?=$user_detail['screen_name'];?> <strong>hasn't Tweeted</strong></h4>
                    <p>When they do, their Tweets will show up here.</p>
                  </center>
                </div>
              </div>
          <?php endif; ?>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="timeline">
          <?php if(! empty($user_timeline)): ?>
            <!-- The timeline -->
            <ul class="timeline timeline-inverse">
              <?php foreach($tweet_datestamp as $tweet_date): ?>
                <!-- timeline time label -->

                <li class="time-label">
                      <span class="bg-red">
                        <?=date('M j, Y', strtotime($tweet_date));?>
                      </span>
                </li>
                <!-- /.timeline-label -->
                <?php foreach($user_timeline as $tweets): ?>
                  <?php if( date('Y-m-d', strtotime($tweets['created_at'])) == date('Y-m-d', strtotime($tweet_date))): ?>
                    <!-- timeline item -->
                    <li>
                      <i class="fa fa-twitter bg-aqua"></i>

                      <div class="timeline-item" data-id="<?=$tweets['id'];?>">
                        <span class="time text-right">
                          <i class="fa fa-calendar"></i> <?=date('l M j, Y', strtotime($tweets['created_at']));?><br>
                          <i class="fa fa-clock-o"></i> <?=date('H:i:s', strtotime($tweets['created_at']));?><br>
                          <?=time_elapsed_string($tweets['created_at']);?>
                        </span>

                        <h3 class="timeline-header">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="<?=$user_detail['profile_image_url'];?>" alt="user image">
                            <span class="username">
                              <a href="https://twitter.com/<?=$user_detail['screen_name'];?>" target="_blank"><?=$user_detail['name'];?></a>
                            </span>
                            <span class="description" style="margin-top: 10px">@<?=$user_detail['screen_name'];?></span>
                          </div>
                        </h3>

                        <div class="timeline-body">
                          <div class="row">
                            <div class="col-md-12 tweet-content">
                              <p><?=$tweets['text'];?></p><br>
                            </div>
                          </div>
                          <?php if(array_key_exists('media', $tweets)):?>
                            <div class="row">
                              <?php foreach($tweets['media'] as $media): ?>
                                <?php if(count($tweets['media']) == 1): ?>
                                    <div class="col-md-12">
                                  <?php elseif(count($tweets['media']) == 2): ?>
                                    <div class="col-md-6">
                                  <?php else: ?>
                                      <div class="col-md-4">
                                <?php endif; ?>
                                <?php if($media['media_type'] == 'photo'): ?>
                                  <a href="<?=$media['media_url'];?>" target="_blank"><img src="<?=$media['media_url'];?>" class="img-responsive"></a>
                                <?php else: ?>
                                  <a href="<?=$media['media_url'];?>" target="_blank">
                                    <video width="100%" controls>
                                      <source src="<?=$media['media_url'];?>" type="<?=$media['media_type'];?>">
                                      Your browser doesn't support HTML5 video
                                    </video>
                                  </a>
                                <?php endif; ?>
                                </div>
                              <?php endforeach; ?>
                            </div>
                          <?php endif; ?>
                          <?php if(array_key_exists('url', $tweets)): ?>
                            <div class="row">
                              <?php foreach($tweets['url'] as $url): ?>
                                <div class="col-md-12">
                                  <div class="fr-embedly " data-original-embed="<a href='<?=$url;?>' class='embedly-card'></a>">
                                    <a href="<?=$url;?>" class="embedly-card"></a>
                                  </div>

                                </div>
                              <?php endforeach; ?>
                            </div>
                          <?php endif; ?>
                        </div>
                        
                        <?php if(array_key_exists('hashtag', $tweets)):?>
                          <div class="timeline-footer text-right">
                            <?php foreach($tweets['hashtag'] as $hashtag): ?>
                              <a class="btn btn-danger btn-xs">#<?=$hashtag;?></a>
                            <?php endforeach; ?>
                          </div>
                        <?php endif; ?>

                      </div>
                    </li>
                    <!-- END timeline item -->
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endforeach; ?>
              <li>
                <i class="fa fa-clock-o bg-gray"></i>
              </li>
            </ul>
            <?php else: ?>
            <div class="row">
              <div class="col-md-12">
                <center>
                  <h4>@<?=$user_detail['screen_name'];?> <strong>hasn't Tweeted</strong></h4>
                    <p>When they do, their Tweets will show up here.</p>
                </center>
              </div>
            </div>
          <?php endif; ?>
        </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->
</div>