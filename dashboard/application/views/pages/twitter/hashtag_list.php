<div class="row">
	<form id="hashtag-filter">
		<div class="col-md-5">
			<div class="row">
				<div class="col-md-12">
					<label for="">Search</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<input type="text" class="form-control" name="keyword" id="keyword">
				</div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-danger btn-block" id="btn-filter-hashtag">Apply</button>
				</div>
			</div>
		</div>
		<div class="col-md-3 pull-right">
			<div class="form-group">
				<label for="">Order</label>
				<select name="order" class="form-control" id="order">
					<option value="">---</option>
					<option value="1">Hashtag Ascending</option>
					<option value="2">Hashtag Descending</option>
					<option value="3">Popularity Ascending</option>
					<option value="4">Popularity Descending</option>
				</select>
			</div>	
		</div>
	</form>
</div>
<br>
<div class="row" id="hashtag_list">
	<?php foreach($hashtag_list as $hashtag): ?>
		<div class="col-md-3">
			<div style="margin: 5px">
				<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag->hashtag");?>" class="btn btn-danger">#<?=$hashtag->hashtag;?> <b>(<?=number_format($hashtag->hashtag_count);?>)</b></a>
			</div>
		</div>
	<?php endforeach; ?>
</div>