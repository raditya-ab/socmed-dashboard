<?php $privilege = $this->session->userdata('loggedIn')['privilege']; ?>

<input type="hidden" id="tweet_count" value="<?=$tweet_count;?>">
<input type="hidden" id="media_count" value="<?=$media_count;?>">
<input type="hidden" id="contributor_count" value="<?=$contributor_count;?>">
<input type="hidden" id="oldest_tweet" value="<?=date('Y-m-d H:i:s', strtotime($oldest_tweet));?>">
<input type="hidden" id="hashtag_name" value="<?=$hashtag;?>">

<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-pills" role="tablist">
		<li role="presentation" class="active">
			<a href="#list_tweet" aria-controls="List Tweet" role="tab" data-toggle="tab">Tweets (<span id="tweet_count"><?=$tweet_count;?></span>)</a>
		</li>
		<li>
			<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag/media");?>">Tweet Media (<span id="media_count"><?=$media_count;?></span>)</a>
		</li>
		<li>
			<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag/contributor");?>">Tweet Contributors (<span id="media_count"><?=$contributor_count;?></span>)</a>
		</li>
		<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
			<li style="float: right">
				<div class="btn-group">
					<button class="btn btn-danger grab-more-tweet">Grab more</button>
					<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class=><i class="fa fa-cog"></i></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu keep-open" id="form-top" role="menu" style="padding: 10px;">
						<form id="grab_tweet-top">
							<input type="hidden" name="max_id">
							<input type="hidden" name="search_string" value="<?=$hashtag;?>" readonly>
							<input type="hidden" name="search_type" value="hashtag" readonly>
							<input type="hidden" name="param" value="hashtag" readonly>
							<input type="hidden" name="save_result" value="1" readonly>
							<li>
								<div class="form-group">
									<label for="" class="control-label clearfix">Result type</label>
									<div class="radio" style="margin-top: -5px">
										<label>
											<input type="radio" name="result_type" value="mixed">
											Mixed
										</label><br>
										<label>
											<input type="radio" name="result_type" value="popular">
											Popular
										</label><br>
										<label>
											<input type="radio" name="result_type" value="recent" checked>
											Recent
										</label>
									</div>
								</div>
							</li>
							<li>
								<label for="" class="control-label">Date Limit</label>
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
									<input type="text" class="form-control datepicker" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly>
								</div>
							</li>
							<li>
								<div class="form-group">
									<label for="" class="control-label">Result Count</label>
									<input type="number" class="form-control" name="count" placeholder="21" value="21" min="0">
								</div>
							</li>
							<li>
								<button class="btn btn-danger grab-more-tweet-form">Grab more</button>
							</li>
						</form>
					</ul>
				</div>
			</li>
			<li style="float: right">
				<div class="btn-group">
					<button class="btn bg-olive download-tweet"><i class="fa fa-download"></i> Download CSV</button>
					<button type="button" class="btn bg-olive dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class=><i class="fa fa-cog"></i></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu keep-open" id="download_form-top" role="menu" style="padding: 10px;">
						<form id="download_tweet-top" method="get" action="<?=site_url("twitter_stream/download_csv/$hashtag/tweet");?>">
							<li>
								<div class="form-group">
									<label for="" class="control-label clearfix">Field</label>
									<div class="checkbox" style="margin-top: -5px">
										<label>
											<input type="checkbox" name="tweet_uid" value="1">
											User ID
										</label><br>
										<label>
											<input type="checkbox" name="name" value="1" checked>
											User Name
										</label><br>
										<label>
											<input type="checkbox" name="screen_name" value="1" checked>
											User Screen Name
										</label><br>
										<label>
											<input type="checkbox" name="text" value="1" checked>
											Tweet Text
										</label><br>
										<label>
											<input type="checkbox" name="created_at" value="1" checked>
											Date Posted
										</label>
									</div>
								</div>
							</li>
							<li>
								<div class="form-group date-container">
									<label for="" class="control-label">Date Range</label>
									<div class="input-group">
										<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
										<input type="text" class="form-control daterangepicker" name="tweet_daterange" readonly style="margin-top: 0px; top: 0px; left: 0px">
									</div>
								</div>
								<div class="form-group">
									<div class="checkbox">
										<label>
											<input type="checkbox" name="grab_all_tweet" value="1">
											Grab All Tweets
										</label>
									</div>
								</div>
							</li>
							<li>
								<button class="btn bg-olive btn-block download-tweet-form" type="button">Download</button>
							</li>
						</form>
					</ul>
				</div>
			</li>
		<?php endif; ?>
	</ul>
	
	<!-- Tab panes -->
	<div class="tab-content" style="margin-top: 20px">
		<div role="tabpanel" class="tab-pane active" id="list_tweet">
			<?php $tweet_index = 1; ?>
			<?php foreach($tweet_list as $index_tweet => $tweet): ?>
				<?php if($index_tweet % 3 == 0): ?><div class="row" id="tweet_list"><?php endif; ?>
					<div class="col-md-4">
						<div class="box box-widget widget-user" data-id="<?=$tweet->tweet_id;?>">
							<div class="widget-user-header bg-aqua-active">
								<h3 class="widget-user-username clearfix"><span class="pull-left" style="font-size: 16px; font-weight: 600"><?=$tweet->name;?></span><span class="pull-right" style="font-size: 14px"><a href="https://twitter.com/<?=$tweet->screen_name;?>" target="_blank" style="color: #FFF">@<?=$tweet->screen_name;?></a></span></h3>
							</div>
							<div class="widget-user-image">
								<img class="img-circle" src="<?=$tweet->profile_image_url;?>" alt="User Avatar">
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-4 border-right">
										<div class="description-block">
											<h5 class="description-header"><?=number_format($tweet->statuses_count);?></h5>
											<span class="description-text">TWEETS</span>
										</div>
									</div>
									<div class="col-sm-4 border-right">
										<div class="description-block">
											<h5 class="description-header"><?=number_format($tweet->followers_count);?></h5>
											<span class="description-text">FOLLOWERS</span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="description-block">
											<h5 class="description-header"><?=number_format($tweet->friends_count);?></h5>
											<span class="description-text">FOLLOWING</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="description-block tweet-text">
											<p align="center"><?=$tweet->text;?></p>
											<p><?=date('M j, Y H:i:s',strtotime($tweet->tweet_created));?></p>
											<p><?=time_elapsed_string(date('M j Y',strtotime($tweet->tweet_created)));?></p>
										</div>
									</div>
									<div class="col-md-6">
										<a class="btn bg-aqua btn-block" href="<?=site_url("twitter_stream/user_detail/$tweet->screen_name");?>" target="_blank">View User</a>
									</div>
									<div class="col-md-6">
										<a class="btn bg-aqua btn-block" href="https://twitter.com/<?=$tweet->screen_name;?>" target="_blank">View On Twitter <i class="fa fa-twitter"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php if($tweet_index % 3 == 0 && $tweet_index != 1): ?></div><?php endif;?>
				<?php $tweet_index++; ?>
			<?php endforeach; ?>
			<div class="row">
				<div class="col-md-12">
					<div class="pagination_links"> 
	                    <?php echo $links; ?>
	                </div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
	<div class="btn-page-nav" style="position: fixed; bottom: 40px; right: 20px">
		<div class="btn-group">
			<button class="btn bg-olive download-tweet"><i class="fa fa-download"></i> Download CSV</button>
			<button type="button" class="btn bg-olive dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<span class=><i class="fa fa-cog"></i></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu keep-open" id="download_form-bottom" role="menu" style="padding: 10px;">
				<form id="download_tweet-bottom" method="get" action="<?=site_url("twitter_stream/download_csv/$hashtag/tweet");?>">
					<li>
						<div class="form-group">
							<label for="" class="control-label clearfix">Field</label>
							<div class="checkbox" style="margin-top: -5px">
								<label>
									<input type="checkbox" name="tweet_uid" value="1">
									User ID
								</label><br>
								<label>
									<input type="checkbox" name="name" value="1" checked>
									User Name
								</label><br>
								<label>
									<input type="checkbox" name="screen_name" value="1" checked>
									User Screen Name
								</label><br>
								<label>
									<input type="checkbox" name="text" value="1" checked>
									Tweet Text
								</label><br>
								<label>
									<input type="checkbox" name="created_at" value="1" checked>
									Date Posted
								</label>
							</div>
						</div>
					</li>
					<li>
						<div class="form-group date-container">
							<label for="" class="control-label">Date Range</label>
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
								<input type="text" class="form-control daterangepicker" name="tweet_daterange" readonly style="margin-top: 0px; top: 0px; left: 0px">
							</div>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="grab_all_tweet" value="1">
									Grab All Tweets
								</label>
							</div>
						</div>
					</li>
					<li>
						<button class="btn bg-olive btn-block download-tweet-form" type="button">Download</button>
					</li>
				</form>
			</ul>
		</div>
		<div class="btn-group">
			<button class="btn btn-danger grab-more-tweet">Grab more</button>
			<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<span class=><i class="fa fa-cog"></i></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu" id="form-bottom" role="menu" style="padding: 10px;">
				<form id="grab_tweet-bottom">
					<input type="hidden" name="max_id">
					<input type="hidden" name="search_string" value="<?=$hashtag;?>" readonly>
					<input type="hidden" name="search_type" value="hashtag" readonly>
					<input type="hidden" name="param" value="hashtag" readonly>
					<input type="hidden" name="save_result" value="true" readonly>
					<li>
						<div class="form-group">
							<label for="" class="control-label clearfix">Result type</label>
							<div class="radio" style="margin-top: -5px">
								<label>
									<input type="radio" name="result_type" value="mixed">
									Mixed
								</label><br>
								<label>
									<input type="radio" name="result_type" value="popular">
									Popular
								</label><br>
								<label>
									<input type="radio" name="result_type" value="recent" checked>
									Recent
								</label>
							</div>
						</div>
					</li>
					<li>
						<label for="" class="control-label">Date Limit</label>
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
							<input type="text" class="form-control datepicker" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly>
						</div>
					</li>
					<li>
						<div class="form-group">
							<label for="" class="control-label">Result Count</label>
							<input type="number" class="form-control" name="count" placeholder="21" value="21" min="0">
						</div>
					</li>
					<li>
						<button class="btn btn-danger grab-more-tweet-form">Grab more</button>
					</li>
				</form>
			</ul>
		</div>
		<button class="btn btn-default" ><i class="fa fa-chevron-up"></i></button>
		
	</div>
<?php endif; ?>


