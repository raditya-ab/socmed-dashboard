<?php $privilege = $this->session->userdata('loggedIn')['privilege']; ?>

<input type="hidden" id="tweet_count" value="<?=$tweet_count;?>">
<input type="hidden" id="media_count" value="<?=$media_count;?>">
<input type="hidden" id="contributor_count" value="<?=$contributor_count;?>">

<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-pills" role="tablist">
		<li role="presentation" >
			<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag/tweet");?>">Tweets (<span id="tweet_count"><?=$tweet_count;?></span>)</a>
		</li>
		<li role="presentation" class="active">
			<a href="#list_media">Tweet Media (<span id="media_count"><?=$media_count;?></span>)</a>
		</li>
		<li>
			<a href="<?=site_url("twitter_stream/hashtag_detail/$hashtag/contributor");?>">Tweet Contributors (<span id="media_count"><?=$contributor_count;?></span>)</a>
		</li>
		<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
			<li style="float: right">
				<div class="btn-group">
					<button class="btn btn-danger grab-more-tweet">Grab more</button>
					<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class=><i class="fa fa-cog"></i></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu keep-open" id="form-top" role="menu" style="padding: 10px;">
						<form id="grab_tweet-top">
							<input type="hidden" name="max_id">
							<input type="hidden" name="search_string" value="<?=$hashtag;?>" readonly>
							<input type="hidden" name="search_type" value="hashtag" readonly>
							<input type="hidden" name="param" value="hashtag" readonly>
							<input type="hidden" name="save_result" value="1" readonly>
							<li>
								<div class="form-group">
									<label for="" class="control-label clearfix">Result type</label>
									<div class="radio" style="margin-top: -5px">
										<label>
											<input type="radio" name="result_type" value="mixed">
											Mixed
										</label><br>
										<label>
											<input type="radio" name="result_type" value="popular">
											Popular
										</label><br>
										<label>
											<input type="radio" name="result_type" value="recent" checked>
											Recent
										</label>
									</div>
								</div>
							</li>
							<li>
								<label for="" class="control-label">Date Limit</label>
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
									<input type="text" class="form-control datepicker" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly>
								</div>
							</li>
							<li>
								<div class="form-group">
									<label for="" class="control-label">Result Count</label>
									<input type="number" class="form-control" name="count" placeholder="21" value="21" min="0">
								</div>
							</li>
							<li>
								<button class="btn btn-danger grab-more-tweet-form">Grab more</button>
							</li>
						</form>
					</ul>
				</div>
			</li>
		<?php endif; ?>
	</ul>
	
	<!-- Tab panes -->
	<div class="tab-content" style="margin-top: 20px">
		<div role="tabpanel" class="tab-pane active" id="list_media">
			<?php $media_index = 1; ?>

			<?php foreach($media_list as $index_media => $tweet_media): ?>
				<?php if($index_media % 2 == 0): ?><div class="row"><?php endif; ?>
						<div class="col-md-6 media-card">
							<div class="box box-widget widget-media" data-tweet_id="<?=$tweet_media['id'];?>">
						        <div class="box-header with-border">
									<div class="user-block">
							            <img class="img-circle" src="<?=$tweet_media['profile_image_url'];?>" alt="<?=$tweet_media['screen_name'];?>">
							            <span class="username"><a href="https://twitter.com/<?=$tweet_media['screen_name'];?>"><?=$tweet_media['name'];?></a></span>
							            <span class="description">@<?=$tweet_media['screen_name'];?></span>
							            <span class="description"><?=date("M, j Y", strtotime($tweet_media['created_at']));?></span>
									</div>
								</div>
						        <div class="box-body">
						        	<div class="row">
						        		<div class="col-md-12 tweet-content">
											<p><?=trim($tweet_media['text']);?></p>
						        		</div>
						        	</div>
						        	<?php if(array_key_exists('media', $tweet_media)):?>
						        	  	<div class="row" style="margin-bottom: 20px">
						        	    	<?php foreach($tweet_media['media'] as $media): ?>
						        	      	<?php if(count($tweet_media['media']) == 1): ?>
						        	        <div class="col-md-12">
						        	        <?php elseif(count($tweet_media['media']) > 1): ?>
						        	        <div class="col-md-6">
						        	      	<?php endif; ?>
						        	      	<?php if($media['media_type'] == 'photo'): ?>
						        	        	<a href="<?=$media['media_url'];?>" target="_blank"><img src="<?=$media['media_url'];?>" class="img-responsive img-thumbnail"></a>
						        	      	<?php else: ?>
						        	        	<a href="<?=$media['media_url'];?>" target="_blank">
						        	          		<video width="100%" controls>
						        	            		<source src="<?=$media['media_url'];?>" type="<?=$media['media_type'];?>">
						        	            		Your browser doesn't support HTML5 video
						        	          		</video>
						        	        	</a>
						        	      	<?php endif; ?>
						        	      	</div>
						        	    	<?php endforeach; ?>
						        	  </div>
						        	<?php endif; ?>
						        	<?php if(array_key_exists('url', $tweet_media)): ?>
						        		<?php if(!empty($tweet_media['url'])): ?>
						        	    <?php foreach($tweet_media['url'] as $url): ?>
						        	  	<div class="row" id="latest_tweet-url">
						        	      <div class="col-md-12">
						        	        <div class="fr-embedly " data-original-embed="<a href='<?=$url;?>' class='embedly-card'></a>">
						        	          <a href="<?=$url;?>" class="embedly-card"></a>
						        	        </div>
						        	      </div>
						        	    <?php endforeach; ?>
						        	  </div>
						        	<?php endif; ?>
						        	<?php endif; ?>
								</div>
					      </div>
					    </div>
				<?php if($media_index % 2 == 0 && $media_index != 0): ?></div><?php endif; ?>
				<?php $media_index++; ?>
				
			<?php endforeach; ?>
			<div class="row">
				<div class="col-md-12">
					<div class="pagination_links"> 
	                    <?php echo $links; ?>
	                </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if ( stripos($privilege,"Create") !== FALSE || stripos($privilege,"Edit") !== FALSE): ?>
<div class="btn-page-nav" style="position: fixed; bottom: 40px; right: 15px">
	<div class="btn-group">
		<button class="btn btn-danger grab-more-tweet">Grab more</button>
		<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<span class=><i class="fa fa-cog"></i></span>
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" id="form-bottom" role="menu" style="padding: 10px;">
			<form id="grab_tweet-bottom">
				<input type="hidden" name="max_id">
				<input type="hidden" name="search_string" value="<?=$hashtag;?>" readonly>
				<input type="hidden" name="search_type" value="hashtag" readonly>
				<input type="hidden" name="param" value="hashtag" readonly>
				<input type="hidden" name="save_result" value="true" readonly>
				<li>
					<div class="form-group">
						<label for="" class="control-label clearfix">Result type</label>
						<div class="radio" style="margin-top: -5px">
							<label>
								<input type="radio" name="result_type" value="mixed">
								Mixed
							</label><br>
							<label>
								<input type="radio" name="result_type" value="popular">
								Popular
							</label><br>
							<label>
								<input type="radio" name="result_type" value="recent" checked>
								Recent
							</label>
						</div>
					</div>
				</li>
				<li>
					<label for="" class="control-label">Date Limit</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" class="form-control datepicker" name="until" placeholder="<?=date('Y-m-d');?>" value="<?=date('Y-m-d');?>" readonly>
					</div>
				</li>
				<li>
					<div class="form-group">
						<label for="" class="control-label">Result Count</label>
						<input type="number" class="form-control" name="count" placeholder="21" value="21" min="0">
					</div>
				</li>
				<li>
					<button class="btn btn-danger grab-more-tweet-form">Grab more</button>
				</li>
			</form>
		</ul>
	</div>
	<button class="btn btn-default" ><i class="fa fa-chevron-up"></i></button>
</div>
<?php endif; ?>