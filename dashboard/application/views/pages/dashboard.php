<?php $privilege = $this->session->userdata("loggedIn")['privilege']; ?>
<?php if ( stripos($this->session->userdata("loggedIn")['privilege'],"View") !== FALSE): ?>
  <div class="row">
    
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box btn-danger">
        <span class="info-box-icon"><i class="fa fa-google-plus"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Users</span>
          <span class="info-box-number" style="font-size: 40px"><?=$google_user->num;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box btn-primary">
        <span class="info-box-icon"><i class="fa fa-facebook"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Users</span>
          <span class="info-box-number" style="font-size: 40px"><?=$facebook_user->num;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box btn-info">
        <span class="info-box-icon"><i class="fa fa-twitter"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Users</span>
          <span class="info-box-number" style="font-size: 40px"><?=$twitter_user->num;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box btn-default">
        <span class="info-box-icon"><i class="fa fa-envelope"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Users</span>
          <span class="info-box-number" style="font-size: 40px"><?=$email_user->num;?></span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
  </div>
  <div class="row">
    <div class="news-list" style="display: block;">
      <div class="col-md-6 news-container"></div>
      <?php foreach($article_list as $index_article => $article): ?>
        <div class="col-md-6 news-card">
          <div class="box box-widget" data-id="<$article->id;?>">
            <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="<?=base_url("$article->author_photo");?>" alt="User Image">
                <span class="username"><a href="#" style="font-size: 14px !important"><?=$article->author;?></a></span>
                <span class="description" style="font-size: 11px">Created <?=time_elapsed_string($article->date_created);?></span>
                <?php if(! is_null($article->date_updated) && ! is_null($article->updater)): ?>
                <span class="description" style="font-size: 11px">Updated <?=time_elapsed_string($article->date_updated);?> at <?=date('H:i:s', strtotime($article->date_updated));?> by <?=$article->updater;?></span>
                <?php endif; ?>
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
                <?php if ( stripos($privilege,"Edit") !== FALSE || stripos($privilege,"Create") !== FALSE): ?>
                  <a href="<?=site_url("article/edit/$article->id");?>" class="btn btn-danger btn-xs">Edit</a>
                <?php endif; ?>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="article-header" style="margin-bottom: 20px">
                <div class="article-title" style="font-size: 18px; font-weight: 700"><?=$article->title;?></div>
                <div class="article-subtitle" style="font-size: 16px"><small><em><?=@$article->subtitle;?></em></small></div>
              </div>
              <div class="article-container" style="overflow: hidden">
                <?=$article->content;?>
              </div>
              <!-- Social sharing buttons -->
              <!-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-share"></i> Share</button> -->
              <!-- <button type="button" class="btn btn-default btn-xs"><i class="fa fa-thumbs-o-up"></i> Like</button> -->
              <!-- <span class="pull-right text-muted"><?=$article->comment_count;?> comments</span> -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
<?php endif; ?>