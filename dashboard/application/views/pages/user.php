
<!-- Default box -->
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">User Approval</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
              title="Collapse">
        <i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <table class="table table-hover" id="tbl_user_list" width="100%">
      <thead>
        <tr>
          <th><input type="checkbox" class="chbUserAll" id="chbAll"></th>
          <th>No.</th>
          <th>Email Address</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Provider</th>
          <th>Privilege</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
  <!-- /.box-body -->
  <div class="box-footer with-border">
    <div class="row">
      <div class="col-md-3">
        <select name="bulk_action" id="bulk_action" class="form-control" disabled>
          <option value="">Bulk Action</option>
          <option value="approve">Approve</option>
          <option value="block">Block</option>
          <option value="delete">Delete</option>
        </select>
      </div>
      <div class="col-md-9">
        <button type="button" class="btn btn-default" id="btn-apply-bulk-action" disabled>Apply</button>
      </div>
    </div>
  </div>
</div>
<!-- /.box -->

<div class="modal fade" id="edit_user_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit User</h4>
      </div>
      <form action="<?=base_url('users/edit');?>" id="edit_user_form">
        <div class="modal-body">
          <input type="hidden" name="id">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" id="email" required readonly>
          </div>
          <div class="form-group">
            <label for="email">First Name</label>
            <input type="text" class="form-control" name="first_name" id="first_name" required readonly>
          </div>
          <div class="form-group">
            <label for="email">Last Name</label>
            <input type="text" class="form-control" name="last_name" id="last_name" required readonly>
          </div>
          <div class="form-group">
            <label for="email">Display Name</label>
            <input type="text" class="form-control" name="screen_name" id="screen_name" required readonly>
          </div>
          <div class="form-group">
            <label for="email">Access Permission</label>
            <select name="group_id" class="form-control" name="group_id" id="group_id" required>
              <option value="">Select</option>
              <?php foreach($group_list as $group): ?>
                <option value="<?=$group->group_id;?>"><?=ucwords($group->group_name);?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>