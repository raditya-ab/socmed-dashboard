
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Manage Image</h3>
		<div class="box-tools">
			<form action="<?=base_url('image/upload');?>" enctype="multipart/form-data" id="formUpload">
				<label for="uploaded_file" class="btn btn-danger btn-sm">Upload</label>
				<input type="file" class="btn btn-danger" id="uploaded_file" name="uploaded_file" style="display: none">
			</form>
		  <!-- <button class="btn btn-danger btn-sm">
		  	<i class="fa fa-upload"></i> Upload
		  </button> -->
		</div>
		<!-- /.box-tools -->
	</div>
	<div class="box-body">
		<div class="row">
			<div class="image-list">
				<div class="col-md-3 image-width"></div>
				<?php foreach($image_list as $image): ?>
					<div class="col-md-3 image-card" style="padding: 10px; margin-bottom: 10px !important">
						<div class="img-container" style="overflow: hidden; position: relative;">
							<div class="img-overlay" style="position: absolute;top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0,0,0,.4); display: none">
								<div class="img-control" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);">
									<button class="btn btn-danger btn-delete" data-id="<?=$image['id'];?>" style="border-radius: 50%"><i class="fa fa-trash"></i></button>
								</div>
							</div>
							<img src="<?=$image['thumb'];?>" alt="" id="img_<?=$image['id'];?>" class="img-responsive img-thumbnail" style="width: 100%;">
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>