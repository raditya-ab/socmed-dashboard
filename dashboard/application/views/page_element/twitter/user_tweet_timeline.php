<ul class="timeline timeline-inverse">
  <?php foreach($retrieved_data['response']['tweet_date'] as $tweet_date): ?>
    <!-- timeline time label -->

    <li class="time-label">
          <span class="bg-red">
            <?=date('M j, Y', strtotime($tweet_date));?>
          </span>
    </li>
    <!-- /.timeline-label -->
    <?php foreach($retrieved_data['response']['pure_result'] as $tweets): ?>
      <?php if( date('Y-m-d', strtotime($tweets['created_at'])) == date('Y-m-d', strtotime($tweet_date))): ?>
        <!-- timeline item -->
        <li>
          <i class="fa fa-twitter bg-aqua"></i>

          <div class="timeline-item" data-id="<?=$tweets['id'];?>">
            <span class="time text-right">
              <i class="fa fa-calendar"></i> <?=date('l M j, Y', strtotime($tweets['created_at']));?><br>
              <i class="fa fa-clock-o"></i> <?=date('H:i:s', strtotime($tweets['created_at']));?><br>
              <?=time_elapsed_string(date('H:i:s', strtotime($tweets['created_at'])));?>
            </span>

            <h3 class="timeline-header">
              <div class="user-block">
                <img class="img-circle img-bordered-sm" src="<?=$retrieved_data['response']['user']['profile_image_url'];?>" alt="user image">
                <span class="username">
                  <a href="https://twitter.com/<?=$retrieved_data['response']['user']['screen_name'];?>" target="_blank"><?=$retrieved_data['response']['user']['name'];?></a>
                </span>
                <span class="description" style="margin-top: 10px">@<?=$retrieved_data['response']['user']['screen_name'];?></span>
              </div>
            </h3>

            <div class="timeline-body">
              <div class="row">
                <div class="col-md-12 tweet-content">
                  <p><?=$tweets['text'];?></p><br>
                </div>
              </div>
              <?php if(array_key_exists('media', $tweets)):?>
                <div class="row">
                  <?php foreach($tweets['media'] as $media): ?>
                    <?php if(count($tweets['media']) == 1): ?>
                        <div class="col-md-12">
                      <?php elseif(count($tweets['media']) == 2): ?>
                        <div class="col-md-6">
                      <?php else: ?>
                          <div class="col-md-4">
                    <?php endif; ?>
                    <?php if($media['media_type'] == 'photo'): ?>
                      <a href="<?=$media['media_url'];?>" target="_blank"><img src="<?=$media['media_url'];?>" class="img-responsive"></a>
                    <?php else: ?>
                      <a href="<?=$media['media_url'];?>" target="_blank">
                        <video width="100%" controls>
                          <source src="<?=$media['media_url'];?>" type="<?=$media['media_type'];?>">
                          Your browser doesn't support HTML5 video
                        </video>
                      </a>
                    <?php endif; ?>
                    </div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
              <?php if(array_key_exists('url', $tweets)): ?>
                <div class="row">
                  <?php foreach($tweets['url'] as $url): ?>
                    <div class="col-md-12">
                      <div class="fr-embedly " data-original-embed="<a href='<?=$url;?>' class='embedly-card'></a>">
                        <a href="<?=$url;?>" class="embedly-card"></a>
                      </div>

                    </div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
            </div>
            
            <?php if(array_key_exists('hashtag', $tweets)):?>
              <div class="timeline-footer text-right">
                <?php foreach($tweets['hashtag'] as $hashtag): ?>
                  <a class="btn btn-danger btn-xs">#<?=$hashtag;?></a>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>

          </div>
        </li>
        <!-- END timeline item -->
      <?php endif; ?>
    <?php endforeach; ?>
  <?php endforeach; ?>
  <li>
    <i class="fa fa-clock-o bg-gray"></i>
  </li>
</ul>