<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| HybridAuth settings
| -------------------------------------------------------------------------
| Your HybridAuth config can be specified below.
|
| See: https://github.com/hybridauth/hybridauth/blob/v2/hybridauth/config.php
*/
$config['hybridauth'] = array(
  "providers" => array(
    // openid providers
    "OpenID" => array(
      "enabled" => FALSE,
    ),
    "Yahoo" => array(
      "enabled" => FALSE,
      "keys" => array("id" => "", "secret" => ""),
    ),
    "AOL" => array(
      "enabled" => FALSE,
    ),
    "Google" => array(
      "enabled" => TRUE,
      "keys" => array("id" => "174012338855-31jh6vbk46d6sr06coujcqnmalb0s5ub.apps.googleusercontent.com", "secret" => "ZEOCXc2KbMqMllrYlqSCU7CZ"),
    ),
    "Facebook" => array(
      "enabled" => TRUE,
      "keys" => array("id" => "555069504884928", "secret" => "e780a6904520a8081354d4123a372db0"),
      // "keys" => array("id" => "1883628891956053", "secret" => "610e4fc9b3f9a7af54929e2eb030793f"),
      "trustForwarded" => FALSE,
    ),
    "Twitter" => array(
      "enabled" => TRUE,
      "keys" => array("key" => "17PZ7QyiM3ZCfsu3nX4lMb6hn", "secret" => "A55tmO8U1K0sadhXy9breVh8GLBXTKeRNKwCIJqw2furOaDh4R"),
      "includeEmail" => TRUE,
    ),
    "Live" => array(
      "enabled" => FALSE,
      "keys" => array("id" => "", "secret" => ""),
    ),
    "LinkedIn" => array(
      "enabled" => FALSE,
      "keys" => array("id" => "", "secret" => ""),
      "fields" => array(),
    ),
    "Foursquare" => array(
      "enabled" => FALSE,
      "keys" => array("id" => "", "secret" => ""),
    ),
  ),
  // If you want to enable logging, set 'debug_mode' to true.
  // You can also set it to
  // - "error" To log only error messages. Useful in production
  // - "info" To log info and error messages (ignore debug messages)
  // "debug_mode" => ENVIRONMENT === 'development',
  "debug_mode" => FALSE,
  // Path to file writable by the web server. Required if 'debug_mode' is not false
  "debug_file" => APPPATH . 'logs/hybridauth.log',
);
