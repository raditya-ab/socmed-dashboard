<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('is_array_multi'))
{
	function is_array_multi($var)
	{
	    foreach ($var as $v) {
            if (is_array($v)) return true;
        }
        return false;
	}
}

/* End of file Time_helper.php */
/* Location: ./application/helpers/Time_helper.php */