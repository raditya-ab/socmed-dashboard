<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('getMimeType'))
{
	function getMimeType($filename)
	{
	    if(function_exists('finfo_fopen')) {
	        $finfo = finfo_open(FILEINFO_MIME_TYPE);
	        $mimetype =  finfo_file($finfo, $filename);
	        finfo_close($finfo);
	    } elseif(function_exists('mime_content_type')) {
	       $mimetype = mime_content_type($filename);
	    }
	    return $mimetype;
	}
}

/* End of file Time_helper.php */
/* Location: ./application/helpers/Time_helper.php */