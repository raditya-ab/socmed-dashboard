<?php 
	class Session_validator
	{

	    private $ci;
	    private $strRedirectUrl = "logout";
	    private $currentController;
	    private $arrExludedControllers = array("login","register");



	    public function __construct()
	    {
	        $this->ci =& get_instance();
	        $this->currentController = $this->ci->router->class;
	    }

	    public function initialize()
	    {
	        if (!$this->ci->session->userdata("loggedIn") && !in_array($this->currentController, $this->arrExludedControllers))
	        {
	            redirect($this->strRedirectUrl);
	        }
	    }

	}   
?>