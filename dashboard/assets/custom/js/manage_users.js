var user_tbl;
var user_selected = [];
var total_user_per_page = 0;

$(document).ready(function() {
	user_tbl = $("#tbl_user_list").DataTable({
		'order':[[1, 'asc']],
	  	'columns': [
		  	{ 'data': 'checkbox'},
		    { 'data':"no" },
		    { 'data':"email" },
		    { 'data':"first_name" },
		    { 'data':"last_name" },
		    { 'data':"provider_name" },
		    { 'data':"group" },
		    { 'data':"status" },
		    { 'data': "action" }
	  	],
		'columnDefs': [
		    {
		      'targets': "_all",
		      'className': 'text-center',
		    },
		    {
		    	'targets': 1,
		    	'className': 'text-left'
		    },
		    {
		    	'targets': 0,
		    	'orderable': false
		    }
	 	],
	  	'ajax': {
		    'url': baseURL + '/users/get_user_list',
		    'type': 'post'
	  	},
	});

	$("#tbl_user_list").on("click",".btn-approve", function(e){
	  e.preventDefault();

	  var userid = $(this).data('id');

	  swal({
	    title: "Approve User",
	    text: "Are you sure to approve this user?",
	    type: "question",
	    showCancelButton: true
	  })
	  .then(function(){
	    $.ajax({
	      url: baseURL + '/users/approve',
	      type: 'POST',
	      dataType: 'json',
	      data: {userid: userid},
	    })
	    .done(function(res) {
	      if(res.status){
	        swal(
	          "Success",
	          res.response,
	          "success"
	        )
	        .then(function(){
	          user_tbl.ajax.reload();
	        }, function(dismiss){
	          user_tbl.ajax.reload();
	        })
	      } else {
	        swal(
	          "Error",
	          res.response,
	          "error"
	        )
	        .then(function(){
	          user_tbl.ajax.reload();
	        }, function(dismiss){
	          user_tbl.ajax.reload();
	        })
	      }
	    })
	    .fail(function() {
	      console.log("error");
	    });
	  }, function(dismiss){
	    user_tbl.ajax.reload();
	  })
	})

	$("#tbl_user_list").on("click",".btn-blocks", function(e){
	  e.preventDefault();

	  var userid = $(this).data('id');

	  swal({
	    title: "Block User",
	    text: "Are you sure to block this user?",
	    type: "question",
	    showCancelButton: true
	  })
	  .then(function(){
	    $.ajax({
	      url: baseURL + '/users/block',
	      type: 'POST',
	      dataType: 'json',
	      data: {userid: userid},
	    })
	    .done(function(res) {
	      if(res.status){
	        swal(
	          "Success",
	          res.response,
	          "success"
	        )
	        .then(function(){
	          user_tbl.ajax.reload();
	        }, function(dismiss){
	          user_tbl.ajax.reload();
	        })
	      } else {
	        swal(
	          "Error",
	          res.response,
	          "error"
	        )
	        .then(function(){
	          user_tbl.ajax.reload();
	        }, function(dismiss){
	          user_tbl.ajax.reload();
	        })
	      }
	    })
	    .fail(function() {
	      console.log("error");
	    });
	  }, function(dismiss){
	    user_tbl.ajax.reload();
	  })
	})

	$("#tbl_user_list").on("click",".btn-edit", function(e){
		e.preventDefault();

		var userid = $(this).data('id');

		$.ajax({
			url: baseURL + '/users/get_user_data',
			type: 'POST',
			dataType: 'json',
			data: {id: userid},
		})
		.done(function(res) {
			if(res.status) {
				$.each(res.response, function(index, val) {
					$("#edit_user_form [name="+index+"]").val(val)
				});

				$("#edit_user_modal").modal('show');
			} else {
				swal(
					"Error",
					res.response,
					"error"
				);
			}
		})
		.fail(function() {
			swal(
				"Error",
				res.response,
				"error"
			);
		});
	})

	$("#edit_user_form").on("submit", function(e){
		e.preventDefault();

		var formData = $(this).serialize();
		$.ajax({
			url: baseURL + '/users/edit/form',
			type: 'POST',
			dataType: 'json',
			data: formData,
		})
		.done(function(res) {
			if(res.status){
				swal(
					"Success",
					res.response,
					"success"
				);
			} else {
				swal(
					"Error",
					res.response,
					"error"
				);
			}

			$("#edit_user_modal").modal('hide');
			user_tbl.ajax.reload();
		})
		.fail(function() {
			swal(
				"Error",
				res.response,
				"error"
			);
		});
	})

	$("#tbl_user_list").on("click", "#chbAll", function(){
		$("#tbl_user_list .chbUser").prop('checked', $("#chbAll").prop('checked'));

		$(".chbUser").each(function(index, el) {
			var val = $(this).val();
			if(!this.checked){
				user_selected.splice(user_selected.indexOf(val),1);
			} else {
				user_selected.push($(this).val())
			}
		});

		if(user_selected.length > 0){
			$("#bulk_action").prop('disabled', false);
			$("#btn-apply-bulk-action").prop('disabled', false);
		} else {
			$("#bulk_action").attr('disabled', 'disabled');
			$("#btn-apply-bulk-action").attr('disabled', 'disabled');
		}

		console.log(user_selected);
	})

	$("#tbl_user_list").on("click", ".chbUser", function(){
		var totalRecords = user_tbl.page.info().recordsDisplay;

		var val = $(this).val();

		if(!this.checked){
			user_selected.splice(user_selected.indexOf(val),1);
		} else {
			user_selected.push($(this).val())
		}

		if(user_selected.length > 0){
			$("#bulk_action").prop('disabled', false);
			$("#btn-apply-bulk-action").prop('disabled', false);

			if(user_selected.length == totalRecords){
				$("#chbAll").prop('checked', true);
			} else {
				$("#chbAll").prop('checked', false);
			}
		} else {
			$("#bulk_action").attr('disabled', 'disabled');
			$("#btn-apply-bulk-action").attr('disabled', 'disabled');
			$("#chbAll").prop('checked', false);
		}
		console.log(user_selected);
	})

	$("#btn-apply-bulk-action").on("click", function(e){
		e.preventDefault();

		var type = $("#bulk_action").val();

		if(type !== '' && user_selected.length > 0){
			swal({
				title: "Mass " + $("#bulk_action option:selected").text() + " User",
				text: "Are you sure?",
				type: "warning",
				showCancelButton: true
			})
			.then(function(){
				$.ajax({
					url: baseURL + '/users/edit/bulk_action',
					type: 'POST',
					dataType: 'json',
					data: {
						type: type,
						users: user_selected
					},
				})
				.done(function(res) {
					if(res.status){
						swal(
							"Success",
							res.response,
							"success"
						);
					} else {
						swal(
							"Error",
							res.response,
							"error"
						);
					}
					$("#chbAll").prop("checked", false);
					user_tbl.ajax.reload();
				})
				.fail(function() {
					console.log("error");
				});
				
			})
		}

	})
	$("#bulk_action").on("change", function(){
		var type = $(this).val();

		if(type == ''){
			$("#btn-apply-bulk-action").attr('disabled', 'disabled');
		} else {
			$("#btn-apply-bulk-action").prop('disabled', false);
		}
	})
});