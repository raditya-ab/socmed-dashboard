// Variable to store your files
var files;

$(document).ready(function() {
	initMasonry();
	$(".img-container").hover(function() {
		$(this).find('.img-overlay').show('fast');
	}, function() {
		$(this).find('.img-overlay').hide('fast');
	});

	$(".img-container").find(".btn-delete").click(function(e) {
		e.preventDefault();

		var imgID = $(this).data('id');

		swal({
			title: "Are you sure ?",
			text: "This image will be removed, make sure no articles related to this image",
			type: "warning",
			showCancelButton: true,
		})
		.then(function(){
			$.ajax({
				url: baseURL + '/image/delete',
				type: 'POST',
				dataType: 'json',
				data: {
					id: imgID
				},
			})
			.done(function(res) {
				if(res.status){
					swal(
						"Success",
						"Image Deleted",
						"success"
					);
				} else {
					swal(
						"Error",
						"Image Not Deleted",
						"error"
					);
				}
				window.location.reload();
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		}, function(){});
	});

	$("#uploaded_file").on('change', uploadFiles);

	function uploadFiles(event)
	{
	  	event.stopPropagation(); // Stop stuff happening
	   	event.preventDefault(); // Totally stop stuff happening

	    // START A LOADING SPINNER HERE

	    // Create a formdata object and add the files
	    files = $('#uploaded_file').prop('files')[0];  
	    var formData = new FormData();
	    formData.append('uploaded_file', files);
	    
    	$.ajax({
    		url: baseURL + '/image/upload',
    		type: 'POST',
    		dataType: 'json',
    		data: formData,
    		cache: false,
    		contentType: false,
    		processData: false
    	})
    	.done(function(res) {
    		if(res.status){
    			swal(
    				"Success",
    				res.response,
    				"success"
				)
				.then(function(){
					window.location.reload();
				}, function(dismiss){
					window.location.reload();
				})
    		} else {
    			swal(
    				"Error",
    				res.response,
    				"error"
				)
				.then(function(){
					window.location.reload();
				}, function(dismiss){
					window.location.reload();
				})
    		}
    		initMasonry();
    	})
	}
});	

function initMasonry()
{
	$(".image-list").masonry({
		itemSelector: ".image-card",
		columnWidth: ".image-width",
		percentPosition: true
	});
}		