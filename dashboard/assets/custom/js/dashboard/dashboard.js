$(document).ready(function() {
	$(".news-list").masonry({
		itemSelector: '.news-card',
		columnWidth: '.news-container',
		percentPosition: true
	});
});