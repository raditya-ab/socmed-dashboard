var form = $("#form_post_article");
var formData;

$(document).ready(function() {

	$('#articleEditor').froalaEditor({
		toolbarButtons: ['paragraphFormat', 'fontFamily', 'fontSize', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|',  'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'color', 'inlineStyle', 'paragraphStyle','|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'embedly', 'fullscreen'],
		// saveURL: baseURL + 'article/post_new',
		theme: 'gray',
		height: 500,
		// Set the file upload parameter.
        fileUploadParam: 'uploaded_file',
 
        // Set the file upload URL.
        fileUploadURL: baseURL + '/article/upload_article_image',
 
        // Set request type.
        fileUploadMethod: 'POST',
 
        // Set max file size to 20MB.
        fileMaxSize: 20 * 1024 * 1024,
 
        // Allow to upload any file.
        fileAllowedTypes: ['application/pdf'],

        // Set the image upload parameter.
        imageUploadParam: 'uploaded_file',
 
        // Set the image upload URL.
        imageUploadURL: baseURL + '/article/upload_article_image',
 
        // Set request type.
        imageUploadMethod: 'POST',
 
        // Set max image size to 5MB.
        imageMaxSize: 5 * 1024 * 1024,
 
        // Allow to upload PNG and JPG.
        imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif'],
        imageManagerLoadURL: baseURL + '/image/get_gallery'
    })
    .on("froalaEditor.contentChanged", function(e, editor){
    	$('#articlePreview').html(editor.html.get());
    });

    $("#btn-saveDraft").on('click', function(e){
    	e.preventDefault();

    	form.find("input[name=status]").val(0);

    	form.submit();
    })

    $("#btn-savePublish").on('click', function(e){
    	e.preventDefault();

    	form.find("input[name=status]").val(1);

    	form.submit();
    })

    form.on('submit', function(e){
    	e.preventDefault();

    	var formData = new FormData(this);

    	// alert('haha');

    	// console.log(formData);
    	$.ajax({
    		url: baseURL + 'article/add',
    		type: 'POST',
    		dataType: 'json',
    		data: formData,
    		cache: false,
    		contentType: false,
    		processData: false
    	})
    	.done(function(res) {
    		if(res.status){
    			swal(
    				"Success",
    				res.response,
    				"success"
				)
				.then(function(){
					window.location.replace(baseURL+'article');
				}, function(dismiss){
					window.location.replace(baseURL+'article');
				})

    		} else {
    			swal(
    				"Error",
    				res.response,
    				"error"
				);
    		}
    	})
    	.fail(function(thrownError) {
    		swal(
    			"Error",
    			thrownError.textResponse,
    			"error"
			)
    	});
    });

    form.find("[name=title]").on("keyup paste",function(event) {
        var text = $(this).val();
        form.find("[name=slug]").val(convertToSlug(text));
    });

    $("#show-gallery").on("show.bs.modal", function(e){
        get_gallery();
    })

    var img_selected = null;
    var img_selected_href = null

    $("#show-gallery").on("click",".gallery-img", function(e){
        if(img_selected == null){
            $(this).css('outline', '3px solid #DB473A');
            img_selected = $(this).data('id');
            img_selected_href = $(this).attr('src');
            console.log(img_selected_href);
        } else if(img_selected == $(this).data('id')){
            $(this).css('outline', 'none');
            img_selected = null;
        }
    })

    $("#show-gallery").on("click","#btn-select_img", function(e){
        e.preventDefault();

        $("#form_post_article").find("[name=featured_image]").val(img_selected);
        $("#form_post_article").find("#featured_image").html('<img src="'+img_selected_href+'" alt="" />');
        $("#show-gallery").modal('hide');
    });

    $("#btn-toggleAddCategory").on("click", function(e){
        e.stopPropagation();
        e.preventDefault();

        $("[name=category_name]").prop('required', 'required');
        $("[name=category_name]").val("");
        $("[name=category_name]").show('fast');
        $("#btn-cancelAdd").show('fast');
        $("#btn-addCategory").show('fast');
        $(this).hide('fast')

    });

    $("#btn-addCategory").click(function(e){
        e.stopPropagation();
        e.preventDefault();

        if($("[name=category_name]").val().trim() !== ''){

            var category_name = $("[name=category_name]").val().trim();

            $.ajax({
                url: baseURL + '/category/add_new/',
                type: 'POST',
                dataType: 'json',
                data: {
                    category_name: category_name
                },
            })
            .done(function(res) {
                if(res.status){
                    $("#post_category").append('<div class="checkbox"><label><input type="checkbox" name="category_id[]" value="'+res.response.id+'">'+res.response.category_name+'</label></div>');
                    $("[name=category_name]").hide('fast');
                    $("#btn-cancelAdd").hide('fast');
                    $("#btn-addCategory").hide('fast');
                    $("#btn-toggleAddCategory").show('fast');
                } else {
                    swal(
                        "Error",
                        res.message,
                        "error"
                    );
                }
            })
            .fail(function(textResponse) {
                swal(
                    "Error",
                    textResponse.responseText,
                    "error"
                );
            });
        }
    })

    $("#btn-cancelAdd").click(function(e){
        e.preventDefault();

        $("[name=category_name]").removeAttr('required');
        $("[name=category_name]").hide('fast');
        $(this).hide('fast');
        $("#btn-addCategory").hide('fast');
        $("#btn-toggleAddCategory").show('fast');
    })

    $("#uploaded_file").on('change', uploadFiles);
});

function uploadFiles(event)
{
    event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening

    // START A LOADING SPINNER HERE

    // Create a formdata object and add the files
    files = $('#uploaded_file').prop('files')[0];  
    var formData = new FormData();
    formData.append('uploaded_file', files);
    
    $.ajax({
        url: baseURL + '/image/upload',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
    .done(function(res) {
        if(res.status){
            swal(
                "Success",
                res.response,
                "success"
            )
            .then(function(){
                get_gallery();
            }, function(dismiss){
                get_gallery();
            })
        } else {
            swal(
                "Error",
                res.response,
                "error"
            )
            .then(function(){
                get_gallery();
            }, function(dismiss){
                get_gallery();
            })
        }
    })
}

function convertToSlug(text)
{
    return text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}

function get_gallery()
{
    $.ajax({
        url: baseURL + '/image/get_gallery',
        type: 'POST',
        dataType: 'json',
        data: {
            type: 'image'
        },
    })
    .done(function(res) {
        console.log(res);
        var cntnt = '';
        for(var i = 0 ; i < res.length ; i++){
           cntnt += '<div class="col-md-3" style="margin-bottom: 30px"><img src="' + res[i].thumb+'" class="img-responsive gallery-img" data-id="'+res[i].id+'" style="max-width: 100%;" alt="Image"></div>'; 
        }
        
        $("#show-gallery").find(".modal-body > .row").html(cntnt);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

