function getFormData(formID){
	var form = $(formID);

	var form_data = form.serializeArray();

	return form_data;
}

function getFormDataField(formID, key)
{
	var form_data = getFormData(formID);

	for(var i = 0; i < form_data.length; i++){
		if(form_data[i].name == key){
			return form_data[i].value;

			break;
		}
	}

	return null;
}

function appendFormData(formID, key, val)
{
	// update form field value
	if(updateFormField(formID, key, val)) {

		// update formData variable in global scope
		formData = getFormData(formID);

		// flag if form field with name of variable 'key' is exist
		var found = false;

		// check for every
		for(var i = 0; i < formData.length; i++)
		{
			if(formData[i].name == key){
				formData[i].value = val;
				found = true;
				break;
			}
		}

		if(found == false) {
			formData.push({name: key, value: val});
			console.log("Append form data fails");
			return false;
		} else {
			console.log("Append form data success");
			return true;
		}
	}
}

function updateFormField(formID, key, val)
{
	var form = $(formID);
	
	var field = form.find("[name='"+key+"']");

	if(field.length > 0){
		field.attr('value', val);

		return true;
	}

	return false;

}

function save_search_query(data)
{
	$.ajax({
		url: baseURL + 'twitter_stream/save_search_param',
		type: 'post',
		dataType: 'json',
		data: data,
	})
	.done(function(res) {
		if(!res.status)
		{
			$.notify({
				icon: 'fa fa-thumbs-up',
				type: 'warning',
				animate: {
					enter: 'animated bounceInDown',
					exit: 'animated bounceOutUp'
				},
				message: "Search query is not saved"
			})
		}
	})
	.fail(function(xmlHttpRequest, textStatus, errorThrown) {
		console.log(xmlHttpRequest);
		console.log(textStatus);
		console.log(errorThrown);
		return false;
	});
}

function get_search_query(data)
{
	$.ajax({
		url: baseURL + 'twitter_stream/get_search_param',
		type: 'post',
		dataType: 'json',
		data: data,
		async: false
	})
	.done(function(res) {
		$(form_id).find("[name='max_id']").val(res);
	})
	.fail(function(xmlHttpRequest, textStatus, errorThrown) {
		console.log(xmlHttpRequest);
		console.log(textStatus);
		console.log(errorThrown);
	});
}