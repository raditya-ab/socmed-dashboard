var next_result = null;
var refresh_url = null;
var retrieved_hashtag = [];
var formData = {};
var form_id = null;
var card_index = 1;

$(window).scroll(function() {
   if(($(window).scrollTop() + $(window).height() == $(document).height()) && ($("#load_more_button").css('display') !== 'none')) {
		$(this).html('<i class="fa fa-spinner fa-spin"></i>');
   		form_id    = "#tweet_search";
   		
   		formData   = getFormData(form_id);

   		var type = getFormDataField(form_id, 'search_type');

   		grab_data(type);

   		console.log('Masuk scroll');
   		$(this).html('Scroll or click to load more..');		
   }
});

$(document).ready(function() {

	$("#btn-submit").click(function(e){
		e.preventDefault();

		$("#search_result").empty();
		$("#hashtag-retrieved").empty();
		
		form_id    = "#tweet_search";
		
   		formData   = getFormData(form_id);

		var type = getFormDataField(form_id, 'search_type');
		
		grab_data(type);
	})

	$("[name=search_type]").on("change", function(e){
		var param = $(this).val();

		$("body").find("#tweet_search").find("#btn-submit").removeAttr('disabled');

		if(param == 'hashtag'){
			$("#user-field").hide('fast');
			$("#keyword-field").show('fast');
			$("body").find("#tweet_search").find("#keyword-field").find("input").removeAttr('disabled');
			$("body").find("#tweet_search").find("#user-field").find("input").prop('disabled', true);
		} else {
			$("#keyword-field").hide('fast');
			$("#user-field").show('fast');
			$("body").find("#tweet_search").find("#keyword-field").find("input").prop('disabled', true);
			$("body").find("#tweet_search").find("#user-field").find("input").removeAttr('disabled');
		}

		$("body").find("#tweet_search").find("[name=param]").val(param);
	});

	$("#ignore_date").change(function(event) {
		var checked = $(this).is(":checked");


		if(checked){
			$("body").find("#tweet_search").find("#keyword-field").find("#until_date_field").hide('fast');
			$("body").find("#tweet_search").find("#keyword-field").find("#until").prop('disabled', true);
		} else {
			$("body").find("#tweet_search").find("#keyword-field").find("#until_date_field").show('fast');
			$("body").find("#tweet_search").find("#keyword-field").find("#until").removeAttr('disabled')
		}
	});

	$("body").find("#btn-load-more").on('click', function(e){
		e.preventDefault();

		$(this).html('<i class="fa fa-spinner fa-spin"></i>');

		orm_id    = "#tweet_search";
   		
   		formData   = getFormData(form_id);
   		
   		var type = getFormDataField(form_id, 'search_type');

   		grab_data(type);

   		$(this).html('Scroll or click to load more..');
	});
});

function grab_data(type)
{
	$("#loading-modal").modal('show');
	if(type == 'hashtag'){
		get_search_query(formData);
   		formData   = getFormData(form_id);
	}

	$.ajax({
		url: baseURL + 'twitter_stream/grab_data',
		type: 'POST',
		dataType: 'json',
		data: formData,
		async: false
	})
	.done(function(res) {
		
		if(res.status == 1 || res.status == 2)
		{
			if(type == 'hashtag')
			{
				if(res.status == 2 && res.response.pure_result.length > 0){
					$.notify({
						icon: 'fa fa-thumbs-up',
						type: 'success',
						animate: {
							enter: 'animated bounceInDown',
							exit: 'animated bounceOutUp'
						},
						message: res.response.pure_result.length + " Search Result successfully saved"
					})
				}

				if(res.response.hashtags.length > 0){
					
					$.each(res.response.hashtags, function(index, val) {
						if($.inArray(val.hashtag, retrieved_hashtag) == -1){
							retrieved_hashtag.push(val.hashtag);
						
							var elem = '<a target="_blank" href="'+baseURL+'twitter_stream/hashtag_detail/'+val.hashtag+'" class="btn btn-danger btn-xs" style="margin: auto 2px">#'+val.hashtag+'</a>';
							$("#hashtag-retrieved").append(elem);
							$("body").find("#hashtag-container").fadeIn('fast');
						}
					});
				}

				if(res.response.metas.next_result == ""){
					$("#next_result").fadeIn('fast');
					$("#load_more_button").fadeOut('fast');
				} else {
					$("#next_result").fadeOut('fast');
					$("#load_more_button").fadeIn('fast');
					var max_id = deserializeString(res.response.metas.next_result)['max_id'];
					console.log("MAX:"+max_id);
					appendFormData(form_id, 'max_id', max_id);
				}

				
				$.each(res.response.sanitized_result, function(index, val) {
					if(index % 3 == 0){
						$("#search_result").append("<div class='row'>");
					}

					$("#search_result").append(draw_cards(val, type));

					if(index !== 0 && card_index % 3 == 0){
						$("#search_result").append("</div>");
					}

					card_index++;
				});

				console.log("===ON AJAX===BEFORE SAVING QUERY===")
				console.log(formData);
				save_search_query(formData)
			}
			else
			{
				var users_temp = [];
				$.each(res.response.users, function(index, val) {
					if($.inArray(val.id, users_temp) !== -1){
						if(card_index % 3 == 0){
							$("#search_result").append("<div class='row'>");
						}

						$("#search_result").append(draw_cards(val, type));

						if(card_index !== 0 && card_index % 3 == 0){
							$("#search_result").append("</div>");
						}

						card_index++;
						users_temp.push(val.id);
					}
				});
			}

			$("#loading-modal").modal('hide');
			$(".tweet-card").linkify();
		}
		else if(res.status == -1){
			if(res.response.metas.next_result == ""){
				$("#load_more_button").fadeOut('fast');
			} else {
				$("#load_more_button").fadeIn('fast');
			}

			$("#search_result").append("<center><h1>No Result..</h1></center>");
			$("#loading-modal").modal('hide');
		} else {
			swal(
				'Error',
				res.response.message,
				'error'
			);

			$("#loading-modal").modal('hide');
		}
	})
	.fail(function(xmlHttpRequest, textStatus, errorThrown) {
		console.log(xmlHttpRequest);
		console.log(textStatus);
		console.log(errorThrown);
	});
}

function getFormData(formID){
	var form = $(formID);

	var form_data = form.serializeArray();

	return form_data;
}

function getFormDataField(formID, key)
{
	var form_data = getFormData(formID);

	for(var i = 0; i < form_data.length; i++){
		if(form_data[i].name == key){
			return form_data[i].value;

			break;
		}
	}

	return null;
}

function appendFormData(formID, key, val)
{
	// update form field value
	if(updateFormField(formID, key, val)) {

		// update formData variable in global scope
		formData = getFormData(formID);
		console.log(formData);

		// flag if form field with name of variable 'key' is exist
		var found = false;

		// check for every
		for(var i = 0; i < formData.length; i++)
		{
			if(formData[i].name == key){
				formData[i].value = val;
				found = true;
				console.log("ketemu, val: "+val);
				break;
			}
		}

		if(found == false) {
			formData.push({name: key, value: val});
			console.log("Append form data fails");
			return false;
		} else {
			console.log("Append form data success");
			return true;
		}
	}
}

function updateFormField(formID, key, val)
{
	var form = $(formID);
	console.log("val : "+val);
	var field = form.find("[name='"+key+"']");
	console.log(field.attr('name'));

	if(field.length > 0){
		field.attr('value', val);
		console.log("masuk");
		console.log(field);

		return true;
	}

	return false;

}

function draw_cards(val, type)
{
	
	

	var elem = '';
	if(type == 'hashtag')
	{
		// var tweet_date = moment(val.created_at,"ddd MMM D H:mm:ss Z YYYY").fromNow();
		var tweet_date = moment(val.created_at,"YYYY-MM-DD HH:mm:ss").fromNow();
		elem = 	'<div class="col-md-4 tweet-card">' +
				'<div class="box box-widget">' +
			        '<div class="box-header with-border">' +
						'<div class="user-block">' +
				            '<img class="img-circle" src="'+val.user.profile_image_url+'" alt="'+val.user.name+'">' +
				            '<span class="username"><a href="#">'+val.user.name+'</a></span>' +
				            '<span class="description">@'+val.user.screen_name+'</span>' +
				            '<span class="description">'+tweet_date+'</span>' +
						'</div>' +
						'<div class="box-tools">' +
							'<button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">' +
								'<i class="fa fa-circle-o"></i>' +
							'</button>' +
				            '<button type="button" class="btn btn-box-tool" data-widget="collapse">' +
				            	'<i class="fa fa-minus"></i>' +
				            '</button>' +
				            '<button type="button" class="btn btn-box-tool" data-widget="remove">' +
				            	'<i class="fa fa-times"></i>' +
					        '</button>' +
						'</div>' +
					'</div>' +
			        '<div class="box-body">' +
						'<p>'+ val.text +'</p>' +
						'<div class="pull-right">';

						// $.each(val.entities.hashtags, function(index, hashtag) {
						$.each(val.hashtag, function(index, hashtags) {
							elem += '<a href="'+baseURL+'twitter_stream/hashtag_detail/'+hashtags+'" class="btn btn-danger btn-xs hashtag-label" style="font-size: 10px">#'+hashtags+'</a>&nbsp;';
						});
							
						elem += '</div>' +
					'</div>' +
		      '</div>' +
		    '</div>';
	}
	else
	{
		elem = 	
		'<div class="col-md-4">'+
			'<div class="box box-widget widget-user">' +
				'<div class="widget-user-header bg-aqua-active">' +
					'<h3 class="widget-user-username clearfix"><span class="pull-left">'+val.name+'</span><span class="pull-right" style="font-size: 14px">@'+val.screen_name+'</span></h3>' +
					'<br /><br />' + 
				'</div>' +
				'<div class="widget-user-image">' +
					'<img class="img-circle" src="'+val.profile_image_url+'" alt="User Avatar">' +
				'</div>' +
				'<div class="box-footer">' +
					'<div class="row">' +
						'<div class="col-sm-4 border-right">' +
							'<div class="description-block">' +
								'<h5 class="description-header">'+new Intl.NumberFormat().format(val.statuses_count)+'</h5>' +
			                    '<span class="description-text">TWEETS</span>' +
							'</div>' +
		                '</div>' +
		                '<div class="col-sm-4 border-right">' +
							'<div class="description-block">' +
			                    '<h5 class="description-header">'+new Intl.NumberFormat().format(val.followers_count)+'</h5>' +
			                    '<span class="description-text">FOLLOWERS</span>' +
							'</div>' +
						'</div>' +
		                '<div class="col-sm-4">' +
							'<div class="description-block">' +
			                    '<h5 class="description-header">'+new Intl.NumberFormat().format(val.friends_count)+'</h5>' +
			                    '<span class="description-text">FOLLOWING</span>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div class="row">' +
						'<div class="col-sm-12">' +
							'<div class="description-block">' +
								'<h5 class="description-header">'+val.description+'</h5>' +
			                    // '<span class="description-text">DESCRIPTION</span>' +
							'</div>' +
		                '</div>' +
		                '<div class="col-md-6">' +
		                	'<a class="btn bg-aqua btn-block" href="'+baseURL+'twitter_stream/user_detail/'+val.screen_name+'">View Detail</a>' +
		                '</div>' +
		                '<div class="col-md-6">' +
							'<a class="btn bg-aqua btn-block" href="https://twitter.com/'+val.screen_name+'" target="_blank">View On Twitter <i class="fa fa-twitter"></i></a>' +
		                '</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';
	}
	
	return elem;
}

function save_search_query(data)
{
	$.ajax({
		url: baseURL + 'twitter_stream/save_search_param',
		type: 'post',
		dataType: 'json',
		data: data,
	})
	.done(function(res) {
		return res.status
	})
	.fail(function(xmlHttpRequest, textStatus, errorThrown) {
		console.log(xmlHttpRequest);
		console.log(textStatus);
		console.log(errorThrown);
		return false;
	});
}

function get_search_query(data)
{
	$.ajax({
		url: baseURL + 'twitter_stream/get_search_param',
		type: 'post',
		dataType: 'json',
		data: data,
		async: false
	})
	.done(function(res) {
		$(form_id).find("[name='max_id']").val(res);
	})
	.fail(function(xmlHttpRequest, textStatus, errorThrown) {
		console.log(xmlHttpRequest);
		console.log(textStatus);
		console.log(errorThrown);
	});
}

