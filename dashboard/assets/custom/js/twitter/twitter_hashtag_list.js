$(document).ready(function() {
	$("#order").on("change", function(e){
		grab_hashtag();
	});

	$("#btn-filter-hashtag").on('click', function(e){
		grab_hashtag();
	});

	$("#hashtag-filter").submit(function(e){
		e.stopPropagation();
		e.preventDefault();
	})
});

function grab_hashtag()
{
	var ordering = $("#order").val();
	var keyword = $.trim($("#keyword").val());

	$("#loading-modal").modal('show');

	$.ajax({
		url: baseURL + 'twitter_stream/grab_hashtag_list',
		type: 'post',
		dataType: 'json',
		data: {
			order: ordering,
			keyword: keyword
		},
	})
	.done(function(res) {
		$("#hashtag_list").empty();
		$.each(res, function(index, val) {
			$("#hashtag_list").append(draw_hashtag(val));
		});
		$("#loading-modal").modal('hide');
	})
	.fail(function() {
		$("#loading-modal").modal('hide');
		swal(
			"Error",
			"Ordering Failed",
			"error"
		);
	});
	
}

function draw_hashtag(val)
{
	var elem = "";
	elem = '<div class="col-md-3">' +
			'<div style="margin: 5px">' +
				'<a href="' + baseURL + 'twitter_stream/hashtag_detail/' + val.hashtag + '" class="btn btn-danger">#'+ val.hashtag +' <b>('+ new Intl.NumberFormat().format(val.hashtag_count)+')</b></a>' +
			'</div>' +
		'</div>';

	return elem;
}