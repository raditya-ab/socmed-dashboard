var formData = {};


$(document).ready(function() {
	$(".tweet-content").linkify();
	
	$("#btn-update-last").on("click", function(e){
		e.preventDefault();

		updateLatestTimeline();
	});

	function updateLatestTimeline()
	{
		var screen_name =  $("[name='screen_name']").val();
		grabLatestTimeline(screen_name);


	}

	function grabLatestTimeline(screen_name_val)
	{
		$("#loading-modal").modal('show');
		$.ajax({
			url: baseURL + 'twitter_stream/user_detail/' + screen_name_val,
			type: 'post',
			dataType: 'json',
			data: {
				screen_name: screen_name_val
			},
		})
		.done(function(res) {
			var elem = "";
			
			$("#latest_post-date").text(moment(res.user_timeline[0].created_at).format('dddd MMM D, YYYY'));
			$("#latest_post-time").text(moment(res.user_timeline[0].created_at).format('HH:mm:ss'));
			$("#latest_post-timestring").text(moment(res.user_timeline[0].created_at).fromNow());
			$("#latest_tweet-text > p").text(res.user_timeline[0].text);
			if(res.user_timeline[0].hasOwnProperty('media')){
				var media = res.user_timeline[0].media;
				if(media.length > 0){
					for(var i=0;i<media.length;i++)
					{
						if(media.length == 1){
							elem += '<div class="col-md-12">';
						} else if(media.length == 2){
							elem += '<div class="col-md-6">';
						} else if(media.length > 2){
							elem += '<div class="col-md-4">';
						}
						if(media[i].media_type == 'photo'){
							elem += '<a href="'+media[i].media_url+'" target="_blank"><img src="'+media[i].media_url+'" class="img-responsive"></a>';
						} else {
							// var video = $("<video />",{
							// 	id: media[i].media_id,
							// 	src: media[i].media_url,
							// 	type: media[i].media_type,
							// 	controls: true
							// });
							elem += '<a href="'+media[i].media_url+'" target="_blank">';
								elem += '<video width="100%" controls>';
									elem += '<source src="'+media[i].media_url+'" type="'+media[i].media_type+'">';
									elem += 'Your browser does not support HTML5 video';
								elem += '</video>';
							elem += '</a>';
						}
						elem += '</div>';
					}
					// $("#latest_tweet-media").html();
					$("#latest_tweet-media").html(elem);
				}
			}
			elem = "";
			if(res.user_timeline[0].hasOwnProperty('url')){
				var url = res.user_timeline[0].url;
				if(url.length > 0){
					for(var $i=0; i< url.length; i++){
						elem += '<div class="col-md-12">';
							elem += '<div class="fr-embedly " data-original-embed="<a href=\''+url[$i]+'\' class=\'embedly-card\'></a>">'
		                    	elem += '<a href="'+url[$i]+'" class="embedly-card"></a>';
	                    	elem += '</div>';
						elem += '</div>';
					}
				}
				$("#latest_tweet-url").html(elem);
			}

			$("#timeline").html(res.response);
			$("#loading-modal").modal('hide');
		})
		.fail(function() {
			console.log("error");
		});
		
	}
});