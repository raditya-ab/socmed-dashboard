var formData = {};
var form_id = null;
var total_media = $("#media_count").val();

$(".dropdown-menu").css('left', -($(".dropdown-menu").width() * 0.5));
$("#form-bottom.dropdown-menu").css('top', -($(".dropdown-menu").outerHeight() + 10));
$('input, label, .dropdown-menu').click(function (e) { e.stopPropagation(); });

$(document).ready(function() {

	$("body").find(".widget-media").find(".tweet-content").find("p").linkify();

	$(".grab-more-tweet").on('click', function(e){
		e.preventDefault();

		form_id    = "#" + $(this).siblings('ul').children('form').attr('id');
		
   		formData   = getFormData(form_id);
		
		grab_data();
	});

	$(".grab-more-tweet-form").on('click', function(e){
		e.preventDefault();

		form_id    = "#"+ $(this).parent().parent().attr('id');
		
   		formData   = getFormData(form_id);
		
		grab_data();
	});
});

function grab_data()
{
	$("#loading-modal").modal('show');
	
	get_search_query(formData);
	formData   = getFormData(form_id);

	$.ajax({
		url: baseURL + 'twitter_stream/grab_data',
		type: 'POST',
		dataType: 'json',
		data: formData,
		async: false
	})
	.done(function(res) {
		if(res.response.pure_result.length === 0 || res.response.pure_result.length == null){
			$.notify({
				icon: 'fa fa-thumbs-up',
				type: 'success',
				animate: {
					enter: 'animated bounceInDown',
					exit: 'animated bounceOutUp'
				},
				message: "No more result"
			});
		} else {
			$.notify({
				icon: 'fa fa-thumbs-up',
				type: 'success',
				animate: {
					enter: 'animated bounceInDown',
					exit: 'animated bounceOutUp'
				},
				message: res.response.pure_result.length + " Search Result successfully saved & loaded"
			})
		}

		if(res.status == 1 || res.status == 2)
		{
			if(res.response.metas.next_result != "" || res.response.metas.next_result != null){
				var max_id = deserializeString(res.response.metas.next_result)['max_id'];
				appendFormData(form_id, 'max_id', max_id);
			}

			save_search_query(formData)

			$("#media_count").text(total_media+res.response.pure_result.length);
		}
		else if(res.status == -1){
			$.notify({
				icon: 'fa fa-exclamation-circle',
				type: 'warning',
				animate: {
					enter: 'animated bounceInDown',
					exit: 'animated bounceOutUp'
				},
				message: "No data loaded"
			})
		} else {
			swal(
				'Error',
				res.response.message,
				'error'
			);
		}
		$("#loading-modal").modal('hide');
	})
	.fail(function(xmlHttpRequest, textStatus, errorThrown) {
		console.log(xmlHttpRequest);
		console.log(textStatus);
		console.log(errorThrown);
	});
}

// function draw_cards(val)
// {
// 	var elem = '';
	
// 	elem = 	
// 		'<div class="col-md-4">'+
// 			'<div class="box box-widget widget-user">' +
// 				'<div class="widget-user-header bg-aqua-active">' +
// 					'<h3 class="widget-user-username clearfix"><span class="pull-left">'+val.user.name+'</span><span class="pull-right" style="font-size: 14px">@'+val.user.screen_name+'</span></h3>' +
// 					'<br /><br />' + 
// 				'</div>' +
// 				'<div class="widget-user-image">' +
// 					'<img class="img-circle" src="'+val.user.profile_image_url+'" alt="User Avatar">' +
// 				'</div>' +
// 				'<div class="box-footer">' +
// 					'<div class="row">' +
// 						'<div class="col-sm-4 border-right">' +
// 							'<div class="description-block">' +
// 								'<h5 class="description-header">'+new Intl.NumberFormat().format(val.user.statuses_count)+'</h5>' +
// 			                    '<span class="description-text">TWEETS</span>' +
// 							'</div>' +
// 		                '</div>' +
// 		                '<div class="col-sm-4 border-right">' +
// 							'<div class="description-block">' +
// 			                    '<h5 class="description-header">'+new Intl.NumberFormat().format(val.user.followers_count)+'</h5>' +
// 			                    '<span class="description-text">FOLLOWERS</span>' +
// 							'</div>' +
// 						'</div>' +
// 		                '<div class="col-sm-4">' +
// 							'<div class="description-block">' +
// 			                    '<h5 class="description-header">'+new Intl.NumberFormat().format(val.user.friends_count)+'</h5>' +
// 			                    '<span class="description-text">FOLLOWING</span>' +
// 							'</div>' +
// 						'</div>' +
// 					'</div>' +
// 					'<div class="row">' +
// 						'<div class="col-sm-12">' +
// 							'<div class="description-block tweet-text">' +
// 								'<p>'+val.text+'</p>' +
// 								'<p>'+moment(val.created_at).format('MMM D, YYYY HH:mm:ss')+'</p>' +
// 								'<p>'+moment(val.created_at,"ddd MMM D H:mm:ss Z YYYY").fromNow()+'</p>' +

// 							'</div>' +
// 		                '</div>' +
// 		                '<div class="col-md-6">' +
// 		                	'<a class="btn bg-aqua btn-block" href="'+baseURL+'twitter_stream/user_detail/'+val.user.screen_name+'" target="_blank">View User</a>' +
// 		                '</div>' +
// 		                '<div class="col-md-6">' +
// 							'<a class="btn bg-aqua btn-block" href="https://twitter.com/'+val.user.screen_name+'" target="_blank">View On Twitter <i class="fa fa-twitter"></i></a>' +
// 		                '</div>' +
// 					'</div>' +
// 				'</div>' +
// 			'</div>' +
// 		'</div>';
	
// 	return elem;
// }
