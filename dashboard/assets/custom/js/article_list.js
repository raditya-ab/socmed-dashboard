var tbl_article;
var article_selected = [];
var total_article_per_page = 0;
var filter = {};

$(document).ready(function() {
	tbl_article = $("#tbl_article").DataTable({
		'columns': [
		  	{ 'data': 'checkbox'},
		    { 'data':"title", 'className': 'text-left' },
		    { 'data':"author" },
		    { 'data':"article_category" },
		    { 'data':"comment_count" },
		    { 'data':"date_created" },
		    { 'data':"status" },
		    { 'data':"action"}
	  	],
		'columnDefs': [
			{
				'targets': "_all",
				'className': 'text-center'
			},
			{
				'targets': 1,
				'className': 'text-left'
			},
			{
				'targets': 0,
				'orderable': false
			}
		],
		'order': [[5,'desc']],
		'ajax': {
		    'url': baseURL + '/article/get_article_list',
		    'type': 'post',
		    data : function(d){
		        return $.extend( {}, d, filter);
		    },
	  	},
	});

	$('#filter').on("submit", function(e){
	    e.preventDefault();
	    
	    $.each($(this).serializeArray(),function(key,val){
	        filter[val.name] = val.value;
	    });

	    console.log(filter);

	    tbl_article.ajax.reload();
	    
	});

	tbl_article.on("click",".btn-delete", function(e){
		var id = $(this).data('id');

		swal({
			title: "Are you sure ?",
			html: "This post will be removed <b>forever</b>",
			type: "warning",
			showCancelButton: true
		})
		.then(function(){
			$.ajax({
				url: baseURL + '/article/delete/' + id,
				type: 'POST',
				dataType: 'json',
				data: {article_id: id},
			})
			.done(function(res) {
				if(res.status){
					swal(
						"Success",
						res.response,
						"success"
					);
				} else {
					swal(
						"Error",
						res.response,
						"error"
					);
				}
			})
			.fail(function() {
				swal(
					"Error",
					"Something wrong",
					"error"
				);
			});
			tbl_article.ajax.reload();
		}).catch(swal.noop);
	})

	$("#tbl_article").on("click", "#chbAll", function(){
		$("#tbl_article .chbArticle").prop('checked', $("#chbAll").prop('checked'));

		$(".chbArticle").each(function(index, el) {
			var val = $(this).val();
			if(!this.checked){
				article_selected.splice(article_selected.indexOf(val),1);
			} else {
				article_selected.push($(this).val())
			}
		});

		if(article_selected.length > 0){
			$("#bulk_action").prop('disabled', false);
			$("#btn-apply-bulk-action").prop('disabled', false);
		} else {
			$("#bulk_action").attr('disabled', 'disabled');
			$("#btn-apply-bulk-action").attr('disabled', 'disabled');
		}

		console.log(article_selected);
	})

	$("#tbl_article").on("click", ".chbArticle", function(){
		var totalRecords = tbl_article.page.info().recordsDisplay;

		var val = $(this).val();

		if(!this.checked){
			article_selected.splice(article_selected.indexOf(val),1);
		} else {
			article_selected.push($(this).val())
		}

		if(article_selected.length > 0){
			$("#bulk_action").prop('disabled', false);
			$("#btn-apply-bulk-action").prop('disabled', false);

			if(article_selected.length == totalRecords){
				$("#chbAll").prop('checked', true);
			} else {
				$("#chbAll").prop('checked', false);
			}
		} else {
			$("#bulk_action").attr('disabled', 'disabled');
			$("#btn-apply-bulk-action").attr('disabled', 'disabled');
			$("#chbAll").prop('checked', false);
		}
		console.log(article_selected);
	})

	$("#btn-apply-bulk-action").on("click", function(e){
		e.preventDefault();

		var type = $("#bulk_action").val();

		if(type !== '' && article_selected.length > 0){
			swal({
				title: "Mass " + $("#bulk_action option:selected").text() + " of Articles",
				text: "Are you sure?",
				type: "warning",
				showCancelButton: true
			})
			.then(function(){
				$.ajax({
					url: baseURL + '/article/bulk_edit',
					type: 'POST',
					dataType: 'json',
					data: {
						type: type,
						articles: article_selected
					},
				})
				.done(function(res) {
					if(res.status){
						swal(
							"Success",
							"Action completed",
							"success"
						);
					} else {
						swal(
							"Error",
							"Error on action",
							"error"
						);
					}
					$("#chbAll").prop("checked", false);
					tbl_article.ajax.reload();
				})
				.fail(function() {
					console.log("error");
				});
				
			})
		}

	})
});