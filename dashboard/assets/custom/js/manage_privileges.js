var priv_tbl;

$(document).ready(function() {
	priv_tbl = $("#tbl_priv_list").DataTable({
	  'columns': [
	    { 'data':"no" },
	    { 'data':"permission_name" },
	    { 'data':"group" },
	    { 'data': 'action'}
	  ],
	  'columnDefs': [
	    {
	      'targets': "_all",
	      'className': 'text-center',
	    },
	    {
	    	'targets': 1,
	    	'className': 'text-left'
	    },
	    {
	    	'width': "45%",
	    	'targets': 2
	    }
	  ],
	  'ajax': {
	    'url': baseURL + '/privilege/get_privilege_list',
	    'type': 'post'
	  }
	});

	$("#tbl_priv_list").on("click", ".btn-edit-priv", function(e){
		e.preventDefault();

		var id = $(this).data('id');
		$("body").find("#modal_edit_privilege").modal('show');

		$("body").find("#modal_edit_privilege").find("input[name=permission_id]").val(id);

		$.ajax({
			url: baseURL + '/privilege/get_group_privilege',
			type: 'POST',
			dataType: 'json',
			data: {permission_id: id},
		})
		.done(function(res) {
			$.each(res, function(index, val) {
				console.log(index);
				$("#modal_edit_privilege").find("input[name=rbo_"+val.group_name+"][value="+val.permission_type+"]").prop('checked', 'checked');
				console.log(val);
				console.log("======");
			});
		})
		.fail(function() {
			console.log("error");
		});
		
	});

	$("#priv_edit").on("submit", function(e){
		e.preventDefault();

		var formData = $(this).serialize();

		$.ajax({
			url: baseURL + '/privilege/group',
			type: 'post',
			dataType: 'json',
			data: formData,
		})
		.done(function(res) {
			$("#modal_edit_privilege").modal('hide');
			if(res.status){
				priv_tbl.ajax.reload();

				swal(
					"Success",
					res.response,
					"success"
				);
			} else {
				priv_tbl.ajax.reload();

				swal(
					"Error",
					res.response,
					"error"
				);
			}
		})
		.fail(function() {
			$("#modal_edit_privilege").modal('hide');
			swal(
				"Error",
				"Something wrong happened",
				"error"
			);
		});
		
	})

	$("#priv_add_group").on("submit", function(e){
		e.preventDefault();

		var formData = $(this).serialize();

		$.ajax({
			url: baseURL + '/privilege/add/group',
			type: 'POST',
			dataType: 'json',
			data: formData,
		})
		.done(function(res) {
			if(res.status){
				swal(
					"Success",
					res.response,
					"success"
				);
			} else {
				swal(
					"Error",
					res.response,
					"error"
				);
			}
			priv_tbl.ajax.reload();
		})
		.fail(function(thrownError) {
			swal(
				"Error",
				thrownError.textResponse,
				"error"
			);
		});
		
	})

	$("#priv_add_permission").on("submit", function(e){
		e.preventDefault();

		var formData = $(this).serialize();

		$.ajax({
			url: baseURL + '/privilege/add/permission',
			type: 'POST',
			dataType: 'json',
			data: formData,
		})
		.done(function(res) {
			if(res.status){
				swal(
					"Success",
					res.response,
					"success"
				);
			} else {
				swal(
					"Error",
					res.response,
					"error"
				);
			}
			priv_tbl.ajax.reload();
		})
		.fail(function(thrownError) {
			swal(
				"Error",
				thrownError.textResponse,
				"error"
			);
			priv_tbl.ajax.reload();
		});
		
	})
	
});