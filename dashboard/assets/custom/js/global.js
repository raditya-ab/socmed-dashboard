var baseURL;

var protocol = window.location.protocol;
var host = window.location.host;
if(host == 'localhost' || host.split('/')[(host.split('.').length -1)] != 'com')
{
	baseURL = protocol + '//' + host + '/' + window.location.pathname.split('/')[1] + '/dashboard/index.php/';
	// console.log(baseURL);
}
else {
	baseURL = protocol + '//' + host + '/dashboard/';
}


$(document).ready(function() {
    $('.datepicker').daterangepicker({
        "showDropdowns": true,
        "singleDatePicker": true,
        "startDate": new Date(),
         "locale": {
            "format": 'YYYY-MM-DD'
        },
        "todayHighlight": true
    });

	// $(".datepicker").daterangepicker({
	// 	autoclose: true,
	// 	todayBtn: true,
	// 	todayHighlight: true,
	// 	format: 'yyyy-mm-dd',
	// 	setDate: new Date()
	// });
});

function deserializeString(str)
{
	var form_data = [];
	var data = str.substring(1);
	var splitted_data  = data.split("&");
	
	for(var i = 0; i < splitted_data.length ; i++){
		var temp = splitted_data[i].split('=');
		form_data[temp[0]] = temp[1];
	}

	return form_data;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}