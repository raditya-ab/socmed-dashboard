-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 02, 2017 at 02:03 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `narrada_auth`
--

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE `group_permissions` (
  `permission_id` int(11) NOT NULL,
  `permission_type` varchar(16) DEFAULT NULL,
  `group_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` (`permission_id`, `permission_type`, `group_id`) VALUES
(1, '0', 1),
(1, '0', 2),
(1, '1', 99),
(2, '0', 1),
(2, '0', 2),
(2, '1', 99),
(3, '0', 1),
(3, '1', 2),
(3, '1', 99),
(4, '1', 1),
(4, '1', 2),
(4, '1', 99),
(5, '0', 1),
(5, '1', 2),
(5, '1', 99),
(6, '0', 1),
(6, '1', 2),
(6, '1', 99);

-- --------------------------------------------------------

--
-- Table structure for table `permission_list`
--

CREATE TABLE `permission_list` (
  `id` int(11) NOT NULL,
  `permission_name` varchar(50) NOT NULL,
  `permission_description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_list`
--

INSERT INTO `permission_list` (`id`, `permission_name`, `permission_description`) VALUES
(1, 'User Approval', ''),
(2, 'Privilege Management', ''),
(3, 'Create', ''),
(4, 'View', ''),
(5, 'Edit', ''),
(6, 'Delete', '');

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `group_id` int(10) NOT NULL,
  `group_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usergroups`
--

INSERT INTO `usergroups` (`group_id`, `group_name`) VALUES
(1, 'user'),
(2, 'editor'),
(99, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `group_id` int(10) NOT NULL DEFAULT '1' COMMENT 'group permission, 1 = user, 2 = editor, 99 = admin',
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `screen_name` varchar(100) DEFAULT NULL,
  `profile_image_url` text,
  `location` varchar(100) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(15) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `email`, `password`, `first_name`, `last_name`, `gender`, `screen_name`, `profile_image_url`, `location`, `last_login`, `last_login_ip`, `created_at`, `updated_at`, `status`) VALUES
(1, 99, 'admin@admin.com', '60260b6351d67a91f47fe9493c959448', 'admin', 'admin', 'male', 'admin admin', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-02 13:59:01', '::1', '2017-09-29 18:52:13', NULL, 1),
(2, 1, 'editor@editor.com', 'acd61093ba7826b3cc2c9c990e1d4110', 'editor', 'editor', 'female', 'editor editor', 'assets/dist/img/female_placeholder.jpg', NULL, '2017-10-02 05:46:55', '::1', '2017-09-29 18:52:32', '2017-10-02 13:59:58', 0),
(3, 1, 'user@user.com', '5d6b3097f4156748ccda679cadb461e2', 'user', 'user', 'male', 'user user', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-02 13:57:25', '::1', '2017-10-02 09:41:55', '2017-10-02 13:57:18', 1),
(4, 1, 'radityaadibaskara@gmail.com', '$2y$10$Szje6vr/T5EaDCjCrKN05eb46aLKH9imedpzOi.mmJCpsxrTPScw.', 'Raditya Adi', 'Baskara', 'male', 'Raditya Adi Baskara', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-02 13:47:46', '::1', '2017-10-02 09:48:43', '2017-10-02 13:59:41', 0),
(5, 1, 'domain.nrd@gmail.com', '$2y$10$KLgA/btnYQXyNyX27iu9neSNW7/LhWvI1VNXo5bPiyHvJ2rUrEzf2', 'Narrada Comm', 'Jakarta', 'male', 'Narrada Comm Jakarta', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-02 08:26:42', '::1', '2017-10-02 13:11:25', '2017-10-02 13:59:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `pid` int(10) NOT NULL,
  `permission_name` varchar(100) NOT NULL,
  `permission_type` int(1) DEFAULT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_provider`
--

CREATE TABLE `user_provider` (
  `user_id` int(11) NOT NULL,
  `provider_uid` varchar(255) NOT NULL,
  `provider_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_provider`
--

INSERT INTO `user_provider` (`user_id`, `provider_uid`, `provider_name`) VALUES
(4, '113260370215683665916', 'Google'),
(5, '1811182608895174', 'Facebook');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD PRIMARY KEY (`permission_id`,`group_id`);

--
-- Indexes for table `permission_list`
--
ALTER TABLE `permission_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `user_provider`
--
ALTER TABLE `user_provider`
  ADD UNIQUE KEY `provider_uid` (`provider_uid`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permission_list`
--
ALTER TABLE `permission_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `group_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `pid` int(10) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD CONSTRAINT `group_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permission_list` (`id`);

--
-- Constraints for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD CONSTRAINT `user_permissions_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_provider`
--
ALTER TABLE `user_provider`
  ADD CONSTRAINT `user_provider_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
