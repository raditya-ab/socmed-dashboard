-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 09, 2017 at 06:00 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `narrada_auth`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) NOT NULL,
  `author` int(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `content` text NOT NULL,
  `featured_image` int(10) DEFAULT NULL COMMENT 'background img url for article title',
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_published` datetime DEFAULT NULL,
  `featured_articles` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = normal post, 1 = featured',
  `status` tinyint(1) NOT NULL COMMENT '0 = draft, 1 = published'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `author`, `title`, `subtitle`, `slug`, `content`, `featured_image`, `date_created`, `date_updated`, `date_published`, `featured_articles`, `status`) VALUES
(1, 1, 'asdaddddd', 'adadadadadadasdasd', 'asdaddddd', '<div class=\"fr-embedly \" data-original-embed=\"<a href=\'https://www.codeigniter.com/user_guide/installation/upgrade_300.html?highlight=fetch_class\' class=\'embedly-card\'></a>\"><a href=\"https://www.codeigniter.com/user_guide/installation/upgrade_300.html?highlight=fetch_class\" class=\"embedly-card\"></a></div><p>dadsadsddadaddas</p>', 7, '2017-10-09 04:13:38', '2017-10-09 04:34:21', NULL, 0, 0),
(3, 1, 'Testsdsds', 'testst', 'testsdsds', '<p>adadsadadadds</p>', 0, '2017-10-09 05:50:44', '2017-10-09 05:54:54', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `category_id` int(10) NOT NULL,
  `article_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article_category`
--

INSERT INTO `article_category` (`category_id`, `article_id`) VALUES
(1, 1),
(1, 3),
(2, 3),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `article_comment`
--

CREATE TABLE `article_comment` (
  `id` int(10) NOT NULL,
  `article_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date_posted` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `author_article`
--

CREATE TABLE `author_article` (
  `article_id` int(10) NOT NULL,
  `author_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `date_created`, `status`) VALUES
(1, 'Uncategorized', '2017-10-04 19:30:00', 1),
(2, 'General', '2017-10-04 00:00:00', 1),
(3, 'Testing', '2017-10-06 04:25:03', 1),
(4, 'Test Category', '2017-10-06 04:27:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `group_permissions`
--

CREATE TABLE `group_permissions` (
  `permission_id` int(10) NOT NULL,
  `permission_type` tinyint(1) DEFAULT NULL,
  `group_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_permissions`
--

INSERT INTO `group_permissions` (`permission_id`, `permission_type`, `group_id`) VALUES
(1, 0, 1),
(1, 0, 2),
(1, 1, 99),
(1, 0, 102),
(2, 0, 1),
(2, 0, 2),
(2, 1, 99),
(2, 0, 102),
(3, 0, 1),
(3, 0, 2),
(3, 1, 99),
(3, 1, 102),
(4, 1, 1),
(4, 1, 2),
(4, 1, 99),
(4, 1, 102),
(5, 0, 1),
(5, 1, 2),
(5, 1, 99),
(5, 1, 102),
(6, 0, 1),
(6, 0, 2),
(6, 1, 99),
(6, 1, 102),
(7, 0, 1),
(7, 1, 2),
(7, 0, 3),
(7, 1, 99),
(7, 0, 102);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) NOT NULL,
  `media_url` text NOT NULL COMMENT 'url to the uploaded files',
  `thumb_url` text NOT NULL,
  `media_name` varchar(255) DEFAULT NULL,
  `media_ext` varchar(6) NOT NULL,
  `media_size` float NOT NULL COMMENT 'media size, using kb',
  `media_type` varchar(20) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `uploader` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `media_url`, `thumb_url`, `media_name`, `media_ext`, `media_size`, `media_type`, `date_uploaded`, `uploader`) VALUES
(2, '/uploads/img/avatar2.png', '/uploads/img/thumb/thumb_avatar2.png', 'avatar2', '.png', 8.07, 'image/png', '2017-10-06 12:45:31', 1),
(3, '/uploads/img/avatar04.png', '/uploads/img/thumb/thumb_avatar04.png', 'avatar04', '.png', 13.22, 'image/png', '2017-10-08 12:12:11', 1),
(4, '/uploads/img/avatar5.png', '/uploads/img/thumb/thumb_avatar5.png', 'avatar5', '.png', 7.4, 'image/png', '2017-10-08 12:15:01', 1),
(5, '/uploads/img/avatar3.png', '/uploads/img/thumb/thumb_avatar3.png', 'avatar3', '.png', 9.02, 'image/png', '2017-10-08 17:19:53', 1),
(6, '/uploads/img/avatar.png', '/uploads/img/thumb/thumb_avatar.png', 'avatar', '.png', 7.93, 'image/png', '2017-10-08 17:23:54', 1),
(7, '/uploads/img/photo11.png', '/uploads/img/thumb/thumb_photo11.png', 'photo11', '.png', 656.26, 'image/png', '2017-10-09 04:11:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_list`
--

CREATE TABLE `permission_list` (
  `id` int(10) NOT NULL,
  `permission_name` varchar(50) NOT NULL,
  `permission_description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_list`
--

INSERT INTO `permission_list` (`id`, `permission_name`, `permission_description`) VALUES
(1, 'User Approval', ''),
(2, 'Privilege Management', ''),
(3, 'Create', ''),
(4, 'View', ''),
(5, 'Edit', ''),
(6, 'Delete', ''),
(7, 'Article Management', '');

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `group_id` int(10) NOT NULL,
  `group_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usergroups`
--

INSERT INTO `usergroups` (`group_id`, `group_name`) VALUES
(1, 'user'),
(2, 'editor'),
(3, 'Netizen'),
(99, 'admin'),
(102, 'testing');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `group_id` int(10) NOT NULL DEFAULT '1' COMMENT 'group permission, 1 = user, 2 = editor, 99 = admin',
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `screen_name` varchar(100) DEFAULT NULL,
  `profile_image_url` text,
  `location` varchar(100) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(15) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `email`, `password`, `first_name`, `last_name`, `gender`, `screen_name`, `profile_image_url`, `location`, `last_login`, `last_login_ip`, `created_at`, `updated_at`, `status`) VALUES
(1, 99, 'admin@admin.com', '60260b6351d67a91f47fe9493c959448', 'admin', 'admin', 'male', 'admin admin', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-09 04:10:13', '::1', '2017-09-29 18:52:13', NULL, 1),
(2, 2, 'editor@editor.com', 'acd61093ba7826b3cc2c9c990e1d4110', 'editor', 'editor', 'female', 'editor editor', 'assets/dist/img/female_placeholder.jpg', NULL, '2017-10-04 15:36:37', '::1', '2017-09-29 18:52:32', '2017-10-02 17:36:33', 1),
(3, 1, 'user@user.com', '5d6b3097f4156748ccda679cadb461e2', 'user', 'user', 'male', 'user user', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-05 10:31:59', '::1', '2017-10-02 09:41:55', '2017-10-02 13:57:18', 1),
(6, 1, 'radityalagi@gmail.com', '$2y$10$X/qS8TLmBq39E5/57nLBUeIHoCgR9f04HnyvxnuSaxTZ2pDTf.uPy', 'Raditya', 'Adi Baskara', 'male', 'Raditya Adi Baskara', 'assets/dist/img/male_placeholder.jpg', NULL, '2017-10-04 02:18:29', '::1', '2017-10-04 07:17:32', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_provider`
--

CREATE TABLE `user_provider` (
  `user_id` int(10) NOT NULL,
  `provider_uid` varchar(255) NOT NULL,
  `provider_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_provider`
--

INSERT INTO `user_provider` (`user_id`, `provider_uid`, `provider_name`) VALUES
(6, '111658598247505625672', 'Google');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author` (`author`),
  ADD KEY `featured_image` (`featured_image`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`category_id`,`article_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Indexes for table `article_comment`
--
ALTER TABLE `article_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `author_article`
--
ALTER TABLE `author_article`
  ADD PRIMARY KEY (`article_id`,`author_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD PRIMARY KEY (`permission_id`,`group_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploader` (`uploader`);

--
-- Indexes for table `permission_list`
--
ALTER TABLE `permission_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_provider`
--
ALTER TABLE `user_provider`
  ADD UNIQUE KEY `provider_uid` (`provider_uid`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `permission_list`
--
ALTER TABLE `permission_list`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `group_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Constraints for table `article_category`
--
ALTER TABLE `article_category`
  ADD CONSTRAINT `article_category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `article_category_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`);

--
-- Constraints for table `group_permissions`
--
ALTER TABLE `group_permissions`
  ADD CONSTRAINT `group_permissions_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permission_list` (`id`);

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`uploader`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_provider`
--
ALTER TABLE `user_provider`
  ADD CONSTRAINT `user_provider_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
